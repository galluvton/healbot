﻿namespace HealBot.GUI
{
    partial class MainScreen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainScreen));
            this.m_dataTable = new System.Windows.Forms.DataGridView();
            this.Data = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Value = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.m_battleList = new System.Windows.Forms.DataGridView();
            this.NameColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FloorColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LocationColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CritOffsetColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CidColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nup_maxXDist = new System.Windows.Forms.NumericUpDown();
            this.L_maxXDist = new System.Windows.Forms.Label();
            this.L_maxYDist = new System.Windows.Forms.Label();
            this.nup_maxYDist = new System.Windows.Forms.NumericUpDown();
            this.L_maxZDist = new System.Windows.Forms.Label();
            this.nup_maxZDist = new System.Windows.Forms.NumericUpDown();
            this.HotkeysPresets = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cb_timedAction = new System.Windows.Forms.ComboBox();
            this.nup_timedAction = new System.Windows.Forms.NumericUpDown();
            this.pb_timedAction = new System.Windows.Forms.ProgressBar();
            this.cb_afk = new System.Windows.Forms.CheckBox();
            this.nup_timedActionLeft = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.m_stat = new System.Windows.Forms.Label();
            this.m_cbOverrideShiftCtrl = new System.Windows.Forms.CheckBox();
            this.cb_safeAFK = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.nup_druidHPTarget = new System.Windows.Forms.NumericUpDown();
            this.pb_druidHP = new System.Windows.Forms.ProgressBar();
            this.tb_druidName = new System.Windows.Forms.TextBox();
            this.druid_active = new System.Windows.Forms.Label();
            this.cb_druidHK = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.check_timedAction = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.cb_maptrackActive = new System.Windows.Forms.CheckBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.l_druidHP = new System.Windows.Forms.Label();
            this.cb_druidActivate = new System.Windows.Forms.CheckBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.cb_manashieldHK = new System.Windows.Forms.ComboBox();
            this.cb_manashield = new System.Windows.Forms.CheckBox();
            this.nup_arrows = new System.Windows.Forms.NumericUpDown();
            this.cb_arrowHK = new System.Windows.Forms.ComboBox();
            this.cb_ringHK = new System.Windows.Forms.ComboBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.l_refRate = new System.Windows.Forms.Label();
            this.cb_readlog = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.m_dataTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_battleList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nup_maxXDist)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nup_maxYDist)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nup_maxZDist)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HotkeysPresets)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nup_timedAction)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nup_timedActionLeft)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nup_druidHPTarget)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nup_arrows)).BeginInit();
            this.groupBox7.SuspendLayout();
            this.SuspendLayout();
            // 
            // m_dataTable
            // 
            this.m_dataTable.AllowUserToAddRows = false;
            this.m_dataTable.AllowUserToDeleteRows = false;
            this.m_dataTable.AllowUserToResizeColumns = false;
            this.m_dataTable.AllowUserToResizeRows = false;
            this.m_dataTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.m_dataTable.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Data,
            this.Value});
            this.m_dataTable.Location = new System.Drawing.Point(129, 12);
            this.m_dataTable.Name = "m_dataTable";
            this.m_dataTable.ReadOnly = true;
            this.m_dataTable.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToDisplayedHeaders;
            this.m_dataTable.Size = new System.Drawing.Size(244, 221);
            this.m_dataTable.TabIndex = 0;
            // 
            // Data
            // 
            this.Data.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Data.HeaderText = "Data";
            this.Data.Name = "Data";
            this.Data.ReadOnly = true;
            this.Data.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Data.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Value
            // 
            this.Value.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Value.HeaderText = "Value";
            this.Value.Name = "Value";
            this.Value.ReadOnly = true;
            this.Value.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Value.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // m_battleList
            // 
            this.m_battleList.AllowUserToAddRows = false;
            this.m_battleList.AllowUserToDeleteRows = false;
            this.m_battleList.AllowUserToResizeColumns = false;
            this.m_battleList.AllowUserToResizeRows = false;
            this.m_battleList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.m_battleList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NameColumn,
            this.FloorColumn,
            this.LocationColumn,
            this.CritOffsetColumn,
            this.CidColumn});
            this.m_battleList.Location = new System.Drawing.Point(12, 239);
            this.m_battleList.Name = "m_battleList";
            this.m_battleList.ReadOnly = true;
            this.m_battleList.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToDisplayedHeaders;
            this.m_battleList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.m_battleList.Size = new System.Drawing.Size(361, 268);
            this.m_battleList.TabIndex = 1;
            // 
            // NameColumn
            // 
            this.NameColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.NameColumn.HeaderText = "Name";
            this.NameColumn.Name = "NameColumn";
            this.NameColumn.ReadOnly = true;
            this.NameColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.NameColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.NameColumn.Width = 41;
            // 
            // FloorColumn
            // 
            this.FloorColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.FloorColumn.HeaderText = "Floor (relative to user)";
            this.FloorColumn.Name = "FloorColumn";
            this.FloorColumn.ReadOnly = true;
            this.FloorColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.FloorColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // LocationColumn
            // 
            this.LocationColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.LocationColumn.HeaderText = "X/Y (relative to user)";
            this.LocationColumn.Name = "LocationColumn";
            this.LocationColumn.ReadOnly = true;
            this.LocationColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.LocationColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // CritOffsetColumn
            // 
            this.CritOffsetColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.CritOffsetColumn.HeaderText = "Offset";
            this.CritOffsetColumn.Name = "CritOffsetColumn";
            this.CritOffsetColumn.ReadOnly = true;
            this.CritOffsetColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // CidColumn
            // 
            this.CidColumn.HeaderText = "Cid";
            this.CidColumn.Name = "CidColumn";
            this.CidColumn.ReadOnly = true;
            this.CidColumn.Visible = false;
            // 
            // nup_maxXDist
            // 
            this.nup_maxXDist.Location = new System.Drawing.Point(175, 37);
            this.nup_maxXDist.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.nup_maxXDist.Name = "nup_maxXDist";
            this.nup_maxXDist.Size = new System.Drawing.Size(78, 20);
            this.nup_maxXDist.TabIndex = 1;
            // 
            // L_maxXDist
            // 
            this.L_maxXDist.AutoSize = true;
            this.L_maxXDist.Location = new System.Drawing.Point(6, 39);
            this.L_maxXDist.Name = "L_maxXDist";
            this.L_maxXDist.Size = new System.Drawing.Size(162, 13);
            this.L_maxXDist.TabIndex = 3;
            this.L_maxXDist.Text = "Max distance from user on X axis";
            // 
            // L_maxYDist
            // 
            this.L_maxYDist.AutoSize = true;
            this.L_maxYDist.Location = new System.Drawing.Point(6, 64);
            this.L_maxYDist.Name = "L_maxYDist";
            this.L_maxYDist.Size = new System.Drawing.Size(162, 13);
            this.L_maxYDist.TabIndex = 5;
            this.L_maxYDist.Text = "Max distance from user on Y axis";
            // 
            // nup_maxYDist
            // 
            this.nup_maxYDist.Location = new System.Drawing.Point(175, 62);
            this.nup_maxYDist.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.nup_maxYDist.Name = "nup_maxYDist";
            this.nup_maxYDist.Size = new System.Drawing.Size(78, 20);
            this.nup_maxYDist.TabIndex = 2;
            // 
            // L_maxZDist
            // 
            this.L_maxZDist.AutoSize = true;
            this.L_maxZDist.Location = new System.Drawing.Point(6, 88);
            this.L_maxZDist.Name = "L_maxZDist";
            this.L_maxZDist.Size = new System.Drawing.Size(162, 13);
            this.L_maxZDist.TabIndex = 8;
            this.L_maxZDist.Text = "Max distance from user on Z axis";
            // 
            // nup_maxZDist
            // 
            this.nup_maxZDist.Location = new System.Drawing.Point(175, 86);
            this.nup_maxZDist.Maximum = new decimal(new int[] {
            16,
            0,
            0,
            0});
            this.nup_maxZDist.Name = "nup_maxZDist";
            this.nup_maxZDist.Size = new System.Drawing.Size(78, 20);
            this.nup_maxZDist.TabIndex = 3;
            // 
            // HotkeysPresets
            // 
            this.HotkeysPresets.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HotkeysPresets.Location = new System.Drawing.Point(395, 12);
            this.HotkeysPresets.Name = "HotkeysPresets";
            this.HotkeysPresets.Size = new System.Drawing.Size(496, 382);
            this.HotkeysPresets.TabIndex = 0;
            this.HotkeysPresets.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.HotkeysPresets_CellContentClick);
            this.HotkeysPresets.CellLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.HotkeysPresets_CellChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 13);
            this.label1.TabIndex = 13;
            this.label1.Text = "Press";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 13);
            this.label2.TabIndex = 14;
            this.label2.Text = "every";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(105, 54);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 13);
            this.label3.TabIndex = 15;
            this.label3.Text = "seconds for";
            // 
            // cb_timedAction
            // 
            this.cb_timedAction.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_timedAction.FormattingEnabled = true;
            this.cb_timedAction.Location = new System.Drawing.Point(45, 25);
            this.cb_timedAction.Name = "cb_timedAction";
            this.cb_timedAction.Size = new System.Drawing.Size(121, 21);
            this.cb_timedAction.TabIndex = 0;
            // 
            // nup_timedAction
            // 
            this.nup_timedAction.Location = new System.Drawing.Point(45, 52);
            this.nup_timedAction.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.nup_timedAction.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nup_timedAction.Name = "nup_timedAction";
            this.nup_timedAction.Size = new System.Drawing.Size(54, 20);
            this.nup_timedAction.TabIndex = 1;
            this.nup_timedAction.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // pb_timedAction
            // 
            this.pb_timedAction.Location = new System.Drawing.Point(9, 100);
            this.pb_timedAction.Name = "pb_timedAction";
            this.pb_timedAction.Size = new System.Drawing.Size(234, 23);
            this.pb_timedAction.TabIndex = 19;
            // 
            // cb_afk
            // 
            this.cb_afk.AutoSize = true;
            this.cb_afk.Location = new System.Drawing.Point(83, 79);
            this.cb_afk.Name = "cb_afk";
            this.cb_afk.Size = new System.Drawing.Size(67, 17);
            this.cb_afk.TabIndex = 20;
            this.cb_afk.Text = "Anti-AFK";
            this.cb_afk.UseVisualStyleBackColor = true;
            this.cb_afk.CheckedChanged += new System.EventHandler(this.cb_afk_CheckedChanged);
            // 
            // nup_timedActionLeft
            // 
            this.nup_timedActionLeft.Location = new System.Drawing.Point(170, 52);
            this.nup_timedActionLeft.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.nup_timedActionLeft.Name = "nup_timedActionLeft";
            this.nup_timedActionLeft.Size = new System.Drawing.Size(42, 20);
            this.nup_timedActionLeft.TabIndex = 2;
            this.nup_timedActionLeft.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(218, 54);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(31, 13);
            this.label4.TabIndex = 22;
            this.label4.Text = "times";
            // 
            // m_stat
            // 
            this.m_stat.AutoSize = true;
            this.m_stat.Location = new System.Drawing.Point(5, 34);
            this.m_stat.Name = "m_stat";
            this.m_stat.Size = new System.Drawing.Size(24, 13);
            this.m_stat.TabIndex = 23;
            this.m_stat.Text = "stat";
            // 
            // m_cbOverrideShiftCtrl
            // 
            this.m_cbOverrideShiftCtrl.Location = new System.Drawing.Point(12, 172);
            this.m_cbOverrideShiftCtrl.Name = "m_cbOverrideShiftCtrl";
            this.m_cbOverrideShiftCtrl.Size = new System.Drawing.Size(115, 61);
            this.m_cbOverrideShiftCtrl.TabIndex = 24;
            this.m_cbOverrideShiftCtrl.Text = "Override pressed keys when using bot";
            this.m_cbOverrideShiftCtrl.UseVisualStyleBackColor = true;
            this.m_cbOverrideShiftCtrl.CheckedChanged += new System.EventHandler(this.m_cbOverrideShiftCtrl_CheckedChanged);
            // 
            // cb_safeAFK
            // 
            this.cb_safeAFK.AutoSize = true;
            this.cb_safeAFK.Location = new System.Drawing.Point(170, 79);
            this.cb_safeAFK.Name = "cb_safeAFK";
            this.cb_safeAFK.Size = new System.Drawing.Size(48, 17);
            this.cb_safeAFK.TabIndex = 25;
            this.cb_safeAFK.Text = "Safe";
            this.cb_safeAFK.UseVisualStyleBackColor = true;
            this.cb_safeAFK.CheckedChanged += new System.EventHandler(this.cb_safeAFK_CheckedChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 31);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(35, 13);
            this.label5.TabIndex = 26;
            this.label5.Text = "Name";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 53);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(22, 13);
            this.label6.TabIndex = 27;
            this.label6.Text = "HP";
            // 
            // nup_druidHPTarget
            // 
            this.nup_druidHPTarget.Location = new System.Drawing.Point(43, 51);
            this.nup_druidHPTarget.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nup_druidHPTarget.Name = "nup_druidHPTarget";
            this.nup_druidHPTarget.Size = new System.Drawing.Size(59, 20);
            this.nup_druidHPTarget.TabIndex = 1;
            this.nup_druidHPTarget.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // pb_druidHP
            // 
            this.pb_druidHP.Location = new System.Drawing.Point(43, 77);
            this.pb_druidHP.Name = "pb_druidHP";
            this.pb_druidHP.Size = new System.Drawing.Size(204, 23);
            this.pb_druidHP.TabIndex = 30;
            // 
            // tb_druidName
            // 
            this.tb_druidName.Location = new System.Drawing.Point(43, 28);
            this.tb_druidName.Name = "tb_druidName";
            this.tb_druidName.Size = new System.Drawing.Size(125, 20);
            this.tb_druidName.TabIndex = 0;
            // 
            // druid_active
            // 
            this.druid_active.AutoSize = true;
            this.druid_active.ForeColor = System.Drawing.Color.Red;
            this.druid_active.Location = new System.Drawing.Point(198, 31);
            this.druid_active.Name = "druid_active";
            this.druid_active.Size = new System.Drawing.Size(45, 13);
            this.druid_active.TabIndex = 32;
            this.druid_active.Text = "ACTIVE";
            // 
            // cb_druidHK
            // 
            this.cb_druidHK.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_druidHK.FormattingEnabled = true;
            this.cb_druidHK.Location = new System.Drawing.Point(108, 50);
            this.cb_druidHK.Name = "cb_druidHK";
            this.cb_druidHK.Size = new System.Drawing.Size(78, 21);
            this.cb_druidHK.TabIndex = 2;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.cb_timedAction);
            this.groupBox1.Controls.Add(this.nup_timedAction);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.nup_timedActionLeft);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.pb_timedAction);
            this.groupBox1.Controls.Add(this.check_timedAction);
            this.groupBox1.Controls.Add(this.cb_safeAFK);
            this.groupBox1.Controls.Add(this.cb_afk);
            this.groupBox1.Location = new System.Drawing.Point(645, 491);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(253, 135);
            this.groupBox1.TabIndex = 35;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Manasitting";
            // 
            // check_timedAction
            // 
            this.check_timedAction.Appearance = System.Windows.Forms.Appearance.Button;
            this.check_timedAction.AutoSize = true;
            this.check_timedAction.Location = new System.Drawing.Point(9, 75);
            this.check_timedAction.Name = "check_timedAction";
            this.check_timedAction.Size = new System.Drawing.Size(56, 23);
            this.check_timedAction.TabIndex = 3;
            this.check_timedAction.Text = "Activate";
            this.check_timedAction.UseVisualStyleBackColor = true;
            this.check_timedAction.CheckedChanged += new System.EventHandler(this.check_timedAction_CheckedChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.button1);
            this.groupBox2.Controls.Add(this.L_maxXDist);
            this.groupBox2.Controls.Add(this.nup_maxXDist);
            this.groupBox2.Controls.Add(this.nup_maxYDist);
            this.groupBox2.Controls.Add(this.L_maxYDist);
            this.groupBox2.Controls.Add(this.nup_maxZDist);
            this.groupBox2.Controls.Add(this.L_maxZDist);
            this.groupBox2.Controls.Add(this.cb_maptrackActive);
            this.groupBox2.Location = new System.Drawing.Point(377, 513);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.groupBox2.Size = new System.Drawing.Size(262, 113);
            this.groupBox2.TabIndex = 36;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Monster Tracking";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(351, -51);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 38;
            this.button1.Text = "Activate maptrack";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // cb_maptrackActive
            // 
            this.cb_maptrackActive.Appearance = System.Windows.Forms.Appearance.Button;
            this.cb_maptrackActive.AutoSize = true;
            this.cb_maptrackActive.Location = new System.Drawing.Point(6, 13);
            this.cb_maptrackActive.Name = "cb_maptrackActive";
            this.cb_maptrackActive.Size = new System.Drawing.Size(103, 23);
            this.cb_maptrackActive.TabIndex = 0;
            this.cb_maptrackActive.Text = "Activate maptrack";
            this.cb_maptrackActive.UseVisualStyleBackColor = true;
            this.cb_maptrackActive.CheckedChanged += new System.EventHandler(this.cb_maptrackActive_CheckedChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.l_druidHP);
            this.groupBox3.Controls.Add(this.tb_druidName);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.cb_druidHK);
            this.groupBox3.Controls.Add(this.nup_druidHPTarget);
            this.groupBox3.Controls.Add(this.druid_active);
            this.groupBox3.Controls.Add(this.cb_druidActivate);
            this.groupBox3.Controls.Add(this.pb_druidHP);
            this.groupBox3.Location = new System.Drawing.Point(379, 400);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(260, 107);
            this.groupBox3.TabIndex = 37;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Druid Healing";
            // 
            // l_druidHP
            // 
            this.l_druidHP.AutoSize = true;
            this.l_druidHP.Location = new System.Drawing.Point(6, 82);
            this.l_druidHP.Name = "l_druidHP";
            this.l_druidHP.Size = new System.Drawing.Size(29, 13);
            this.l_druidHP.TabIndex = 34;
            this.l_druidHP.Text = "NaN";
            // 
            // cb_druidActivate
            // 
            this.cb_druidActivate.Appearance = System.Windows.Forms.Appearance.Button;
            this.cb_druidActivate.AutoSize = true;
            this.cb_druidActivate.Location = new System.Drawing.Point(192, 48);
            this.cb_druidActivate.Name = "cb_druidActivate";
            this.cb_druidActivate.Size = new System.Drawing.Size(56, 23);
            this.cb_druidActivate.TabIndex = 3;
            this.cb_druidActivate.Text = "Activate";
            this.cb_druidActivate.UseVisualStyleBackColor = true;
            this.cb_druidActivate.CheckedChanged += new System.EventHandler(this.cb_druidActivate_CheckedChanged);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(684, 400);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 38;
            this.button2.Text = "save";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.save_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(782, 400);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 39;
            this.button3.Text = "load";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.load_Click);
            // 
            // cb_manashieldHK
            // 
            this.cb_manashieldHK.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_manashieldHK.FormattingEnabled = true;
            this.cb_manashieldHK.Location = new System.Drawing.Point(74, 24);
            this.cb_manashieldHK.Name = "cb_manashieldHK";
            this.cb_manashieldHK.Size = new System.Drawing.Size(78, 21);
            this.cb_manashieldHK.TabIndex = 35;
            // 
            // cb_manashield
            // 
            this.cb_manashield.Appearance = System.Windows.Forms.Appearance.Button;
            this.cb_manashield.AutoSize = true;
            this.cb_manashield.Location = new System.Drawing.Point(263, 49);
            this.cb_manashield.Name = "cb_manashield";
            this.cb_manashield.Size = new System.Drawing.Size(56, 23);
            this.cb_manashield.TabIndex = 36;
            this.cb_manashield.Text = "Activate";
            this.cb_manashield.UseVisualStyleBackColor = true;
            this.cb_manashield.CheckedChanged += new System.EventHandler(this.Manashield_Arrow_Ring_CheckedChanged);
            // 
            // nup_arrows
            // 
            this.nup_arrows.Location = new System.Drawing.Point(158, 52);
            this.nup_arrows.Name = "nup_arrows";
            this.nup_arrows.Size = new System.Drawing.Size(72, 20);
            this.nup_arrows.TabIndex = 37;
            this.nup_arrows.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // cb_arrowHK
            // 
            this.cb_arrowHK.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_arrowHK.FormattingEnabled = true;
            this.cb_arrowHK.Location = new System.Drawing.Point(74, 51);
            this.cb_arrowHK.Name = "cb_arrowHK";
            this.cb_arrowHK.Size = new System.Drawing.Size(78, 21);
            this.cb_arrowHK.TabIndex = 35;
            // 
            // cb_ringHK
            // 
            this.cb_ringHK.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_ringHK.FormattingEnabled = true;
            this.cb_ringHK.Location = new System.Drawing.Point(74, 78);
            this.cb_ringHK.Name = "cb_ringHK";
            this.cb_ringHK.Size = new System.Drawing.Size(78, 21);
            this.cb_ringHK.TabIndex = 35;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.cb_manashield);
            this.groupBox7.Controls.Add(this.cb_manashieldHK);
            this.groupBox7.Controls.Add(this.nup_arrows);
            this.groupBox7.Controls.Add(this.label9);
            this.groupBox7.Controls.Add(this.cb_arrowHK);
            this.groupBox7.Controls.Add(this.label8);
            this.groupBox7.Controls.Add(this.label7);
            this.groupBox7.Controls.Add(this.cb_ringHK);
            this.groupBox7.Location = new System.Drawing.Point(12, 513);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(325, 111);
            this.groupBox7.TabIndex = 42;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Manashielding";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(7, 54);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(39, 13);
            this.label9.TabIndex = 38;
            this.label9.Text = "Arrows";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(7, 27);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(61, 13);
            this.label8.TabIndex = 37;
            this.label8.Text = "Manashield";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(7, 81);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(34, 13);
            this.label7.TabIndex = 36;
            this.label7.Text = "Rings";
            // 
            // l_refRate
            // 
            this.l_refRate.AutoSize = true;
            this.l_refRate.Location = new System.Drawing.Point(5, 12);
            this.l_refRate.Name = "l_refRate";
            this.l_refRate.Size = new System.Drawing.Size(47, 13);
            this.l_refRate.TabIndex = 43;
            this.l_refRate.Text = "RefRate";
            // 
            // cb_readlog
            // 
            this.cb_readlog.Appearance = System.Windows.Forms.Appearance.Button;
            this.cb_readlog.AutoSize = true;
            this.cb_readlog.Location = new System.Drawing.Point(741, 452);
            this.cb_readlog.Name = "cb_readlog";
            this.cb_readlog.Size = new System.Drawing.Size(61, 23);
            this.cb_readlog.TabIndex = 44;
            this.cb_readlog.Text = "ReadLog";
            this.cb_readlog.UseVisualStyleBackColor = true;
            this.cb_readlog.CheckedChanged += new System.EventHandler(this.cb_readlog_CheckedChanged);
            // 
            // MainScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(903, 636);
            this.Controls.Add(this.cb_readlog);
            this.Controls.Add(this.l_refRate);
            this.Controls.Add(this.groupBox7);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.m_cbOverrideShiftCtrl);
            this.Controls.Add(this.m_stat);
            this.Controls.Add(this.HotkeysPresets);
            this.Controls.Add(this.m_battleList);
            this.Controls.Add(this.m_dataTable);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "MainScreen";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "HealBot";
            this.Load += new System.EventHandler(this.MainScreen_Load);
            ((System.ComponentModel.ISupportInitialize)(this.m_dataTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_battleList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nup_maxXDist)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nup_maxYDist)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nup_maxZDist)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HotkeysPresets)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nup_timedAction)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nup_timedActionLeft)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nup_druidHPTarget)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nup_arrows)).EndInit();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView m_dataTable;
        private System.Windows.Forms.DataGridView m_battleList;
        private System.Windows.Forms.NumericUpDown nup_maxXDist;
        private System.Windows.Forms.Label L_maxXDist;
        private System.Windows.Forms.Label L_maxYDist;
        private System.Windows.Forms.NumericUpDown nup_maxYDist;
        private System.Windows.Forms.Label L_maxZDist;
        private System.Windows.Forms.NumericUpDown nup_maxZDist;
        private System.Windows.Forms.DataGridView HotkeysPresets;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cb_timedAction;
        private System.Windows.Forms.NumericUpDown nup_timedAction;
        private System.Windows.Forms.ProgressBar pb_timedAction;
        private System.Windows.Forms.CheckBox cb_afk;
        private System.Windows.Forms.NumericUpDown nup_timedActionLeft;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label m_stat;
        private System.Windows.Forms.CheckBox m_cbOverrideShiftCtrl;
        private System.Windows.Forms.DataGridViewTextBoxColumn Data;
        private System.Windows.Forms.DataGridViewTextBoxColumn Value;
        private System.Windows.Forms.CheckBox cb_safeAFK;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown nup_druidHPTarget;
        private System.Windows.Forms.ProgressBar pb_druidHP;
        private System.Windows.Forms.TextBox tb_druidName;
        private System.Windows.Forms.Label druid_active;
        private System.Windows.Forms.ComboBox cb_druidHK;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label l_druidHP;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.CheckBox cb_maptrackActive;
        private System.Windows.Forms.CheckBox check_timedAction;
        private System.Windows.Forms.CheckBox cb_druidActivate;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.ComboBox cb_manashieldHK;
        private System.Windows.Forms.CheckBox cb_manashield;
        private System.Windows.Forms.ComboBox cb_arrowHK;
        private System.Windows.Forms.ComboBox cb_ringHK;
        private System.Windows.Forms.NumericUpDown nup_arrows;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label l_refRate;
        private System.Windows.Forms.DataGridViewTextBoxColumn NameColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn FloorColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn LocationColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn CritOffsetColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn CidColumn;
        private System.Windows.Forms.CheckBox cb_readlog;
    }
}