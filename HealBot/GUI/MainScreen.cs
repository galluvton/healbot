﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using HealBot.Utils;
using HealBot.Utils.Parser;
using HealBot.TableItems;
using HealBot.Threads;
using HealBot.Refreshers;
using System.Globalization;

namespace HealBot.GUI
{
    public partial class MainScreen : RefreshableForm
    {
        
        // workers
        private HotkeyWorker m_HKWorker = null;
        private BattleListWorker m_BLWorker = null;
        private DruidHealWorker m_druidHeal = null;
        private ManasitWorker m_manasit = null;
        private ManashieldWorker m_manashield = null;
        private LogReaderWorker m_logReader = null;

        private RefreshFormEventHandler m_refreshHandler = null;
        private ArrayList m_hotkeyOptions = new ArrayList();
        private ArrayList m_columnCondiotionDataSource = null;
        private Dictionary<string, HpMpCondItem> m_condMapping = null;
        private ArrayList m_columnHotkeyDataSource = null;
        private Dictionary<string, Hotkey> m_hotkeyMapping = null;
        private Object m_lock = new Object();
        private bool m_running = false;
        private bool m_killFlag = false;
        private bool m_anythingCheckedFlag = false;
        private bool m_dumpStatisticsOnClose;
        private string m_dumpPrefix = "";
        private string m_loadFile = "";

        public MainScreen(double secsRefresh, bool queue, bool dumpStat, string loadFile)
        {
            InitializeComponent();

            l_refRate.Text = "Refresh rate " + secsRefresh;
            l_refRate.Text = queue ? l_refRate.Text + " q" : l_refRate.Text;
            m_dumpStatisticsOnClose = dumpStat;
            m_loadFile = loadFile;
            if (dumpStat)
            {
                m_dumpPrefix = "******StatDump******\n";
                m_dumpPrefix += "Refresh rate is " + secsRefresh.ToString() + "\n";
                m_dumpPrefix += "Queuemode is " + queue.ToString() + "\n";
            }

            nup_maxXDist.Value = Constants.SCREEN_X_RADIUS;
            nup_maxYDist.Value = Constants.SCREEN_Y_RADIUS;
            nup_maxZDist.Value = 2;

            m_columnCondiotionDataSource = InitHpMpColumn();
            m_columnHotkeyDataSource = InitHotkeys();
            InitHotkeyDataView();
            cb_timedAction.DataSource = InitHotkeys();
            cb_timedAction.DisplayMember = "KHName";
            cb_timedAction.ValueMember = "KHValue";
            cb_druidHK.DataSource = InitHotkeys();
            cb_druidHK.DisplayMember = "KHName";
            cb_druidHK.ValueMember = "KHValue";
            druid_active.Visible = false;
            cb_manashieldHK.DataSource = InitHotkeys(true);
            cb_manashieldHK.DisplayMember = "KHName";
            cb_manashieldHK.ValueMember = "KHValue";
            cb_arrowHK.DataSource = InitHotkeys(true);
            cb_arrowHK.DisplayMember = "KHName";
            cb_arrowHK.ValueMember = "KHValue";
            cb_ringHK.DataSource = InitHotkeys(true);
            cb_ringHK.DisplayMember = "KHName";
            cb_ringHK.ValueMember = "KHValue";

            if (!Util.Update())
            {
                m_killFlag = true;
                KillIfNeeded();
            }

            m_refreshHandler = new RefreshFormEventHandler(this, secsRefresh);
            SetupWorkers(secsRefresh);
            m_HKWorker.SetActive();
        }

        private void MainScreen_Load(object sender, EventArgs e)
        {
            HotkeysPresets.Rows.Clear();
            m_refreshHandler.Start();

            if (m_loadFile != null)
            {
                Dictionary<string, IList<CompressedHotkeyTableEntry>> lines = PropertyFileParser.ReadProperties(m_loadFile);
                LoadDataFromData(lines);
            }
        }

        public DataGridViewRowCollection KHPresetsRows
        {
            get { return HotkeysPresets.Rows; }
        }

        public DataGridView BattleList
        {
            get { return m_battleList; }
        }

        public bool MaptrackCheckboxChecked
        {
            get { return cb_maptrackActive.Checked; }
        }

        public int NUPMaxX
        {
            get { return (int)nup_maxXDist.Value; }
        }

        public int NUPMaxY
        {
            get { return (int)nup_maxYDist.Value; }
        }

        public int NUPMaxZ
        {
            get { return (int)nup_maxZDist.Value; }
        }

        public void KillWithMessage(string msg)
        {
            KillWorkers();
            if (m_refreshHandler != null)
            {
                m_refreshHandler.Stop();
            }

            if (m_dumpStatisticsOnClose)
            {
                Util.debugLog(m_dumpPrefix + getStatMessage());
            }

            if (!msg.Equals(""))
            {
                MessageBox.Show(msg);
            }
            Environment.Exit(0);
        }

        private void InitHotkeyDataView()
        {
            DataGridViewComboBoxColumn columnCondition = new DataGridViewComboBoxColumn();
            columnCondition.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            columnCondition.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            columnCondition.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            columnCondition.DataPropertyName = "Prefix";
            columnCondition.Name = "Prefix";
            columnCondition.HeaderText = "Prefix";
            columnCondition.DataSource = m_columnCondiotionDataSource;
            columnCondition.DisplayMember = "CondName";
            columnCondition.ValueMember = "This";

            DataGridViewTextBoxColumn columnAmount = new DataGridViewTextBoxColumn();
            columnAmount.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            columnAmount.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            columnAmount.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            columnAmount.DataPropertyName = "Amount";
            columnAmount.Name = "Amount";
            columnAmount.HeaderText = "Amount";

            DataGridViewComboBoxColumn columnHotkey = new DataGridViewComboBoxColumn();
            columnHotkey.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            columnHotkey.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            columnHotkey.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            columnHotkey.DataPropertyName = "Hotkey";
            columnHotkey.Name = "Hotkey";
            columnHotkey.HeaderText = "Hotkey";
            columnHotkey.DataSource = m_columnHotkeyDataSource;
            columnHotkey.DisplayMember = "KHName";
            columnHotkey.ValueMember = "This";

            DataGridViewCheckBoxColumn columnCheckbox = new DataGridViewCheckBoxColumn();
            columnCheckbox.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            columnCheckbox.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            columnCheckbox.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            columnCheckbox.DataPropertyName = "Active";
            columnCheckbox.Name = "Active";
            columnCheckbox.HeaderText = "Active";
            columnCheckbox.TrueValue = true;
            columnCheckbox.FalseValue = false;

            HotkeysPresets.Columns.Insert(0, columnCondition);
            HotkeysPresets.Columns.Insert(1, columnAmount);
            HotkeysPresets.Columns.Insert(2, columnHotkey);
            HotkeysPresets.Columns.Insert(3, columnCheckbox);
        }

        private ArrayList InitHpMpColumn()
        {
            ArrayList m_Column = new ArrayList();
            m_condMapping = new Dictionary<string, HpMpCondItem>();

            m_condMapping.Add(HpMpCondItem.HPPlus, new HpMpCondItem(HpMpCondItem.HPPlusText, HpMpCondItem.HPPlus));
            m_condMapping.Add(HpMpCondItem.HPMinus, new HpMpCondItem(HpMpCondItem.HPMinusText, HpMpCondItem.HPMinus));
            m_condMapping.Add(HpMpCondItem.MPPlus, new HpMpCondItem(HpMpCondItem.MPPlusText, HpMpCondItem.MPPlus));
            m_condMapping.Add(HpMpCondItem.MPMinus, new HpMpCondItem(HpMpCondItem.MPMinusText, HpMpCondItem.MPMinus));

            foreach (KeyValuePair<string, HpMpCondItem> entry in m_condMapping)
            {
                m_Column.Add(entry.Value);
            }

            return m_Column;
        }

        private ArrayList InitHotkeys(bool includeEmpty = false)
        {
            ArrayList ret = new ArrayList();
            bool initTable = (m_hotkeyMapping == null);
            if (initTable)
            {
                m_hotkeyMapping = new Dictionary<string, Hotkey>();
            }
            if (includeEmpty)
            {
                Hotkey HK = new Hotkey("None", null);
                ret.Add(HK);
            }

            for (int i = 0; i < 36; i++)
            {
                string name;
                Constants.KeyboardControls ctrlShift;
                if (i >= 0 && i <= 11)
                {
                    name = "F" + (i + 1).ToString();
                    ctrlShift = Constants.KeyboardControls.invalid;
                }
                else if (i >= 12 && i <= 23)
                {
                    name = "Shift+F" + (i % 12 + 1).ToString();
                    ctrlShift = Constants.KeyboardControls.shift;
                }
                else
                {
                    name = "Ctrl+F" + (i % 12 + 1).ToString();
                    ctrlShift = Constants.KeyboardControls.ctrl;
                }
                KeyCombo KC = new KeyCombo(ctrlShift, (Constants.KeyboardControls)((int)Constants.KeyboardControls.F1 + (i % 12)));
                Hotkey HK = new Hotkey(name, KC);

                if (initTable)
                {
                    m_hotkeyMapping.Add(name, HK);
                }
                else
                {
                    ret.Add(HK);
                }
            }

            if (initTable)
            {
                foreach (KeyValuePair<string, Hotkey> entry in m_hotkeyMapping)
                {
                    ret.Add(entry.Value);
                }
            }

            return ret;
        }

        public override void RefreshMe()
        {
            long time = DateTime.Now.Ticks;
            
            CheckConnected();
            KillIfNeeded();
            if ( !MemoryReader.Connected() )
            {
                UncheckAll();
                return;
            }

            bool isSomeoneRunning;
            lock (m_lock)
            {
                isSomeoneRunning = m_running;
            }
            if (!isSomeoneRunning)
            {
                m_running = true;

                FillCharacterData(m_dataTable);

                m_running = false;
            }

            KillIfNeeded();
            time = DateTime.Now.Ticks - time;

            Statistics.GetInstance().Main = (time / TimeSpan.TicksPerMillisecond).ToString();
            StatRefresh();
        }

        private void SetupWorkers(double secsRefresh)
        {
            m_HKWorker = new HotkeyWorker(this, secsRefresh, ThreadPriority.Highest);
            m_BLWorker = new BattleListWorker(this, secsRefresh, ThreadPriority.Normal);
            m_druidHeal = new DruidHealWorker(this, secsRefresh, ThreadPriority.AboveNormal);
            m_manasit = new ManasitWorker(this, ThreadPriority.Normal);
            m_manashield = new ManashieldWorker(this, secsRefresh, ThreadPriority.AboveNormal);
            m_logReader = new LogReaderWorker(this, ThreadPriority.Normal);
        }

        private void KillWorkers()
        {
            List<BaseWorker> workers = new List<BaseWorker>();

            if (m_HKWorker != null) workers.Add(m_HKWorker);
            if (m_BLWorker != null) workers.Add(m_BLWorker);
            if (m_druidHeal != null) workers.Add(m_druidHeal);
            if (m_manasit != null) workers.Add(m_manasit);
            if (m_manashield != null) workers.Add(m_manashield);
            if (m_logReader != null) workers.Add(m_logReader);

            foreach (BaseWorker w in workers)
            {
                w.DeactivateWorker();
            }
        }

        /* ---------------------------- DELEGATORS START ---------------------------- */

        public void BattleListUpdateTable(MainScreen form, List<Creature> creatures)
        {
            MethodInvoker method = delegate
            {
                List<int> list = new List<int>();

                foreach (Creature c in creatures)
                {
                    DataGridViewRow ret = GetRowAlways(form.BattleList, "CidColumn", c.MyId.ToString());
                    ret.Cells["NameColumn"].Value = c.MyName;
                    ret.Cells["FloorColumn"].Value = c.MyFloor;
                    ret.Cells["LocationColumn"].Value = c.MyXy;
                    ret.Cells["CritOffsetColumn"].Value = c.MyOffset;
                    
                    list.Add(ret.Index);
                }

                int count = form.BattleList.Rows.Count-1;
                for (; count >= 0; count--)
                {
                    if (!list.Contains(count))
                    {
                        form.BattleList.Rows.RemoveAt(count);
                    }
                }
            };
            form.BeginInvoke(method);
        }

        public void TimedActionStop(MainScreen form)
        {
            MethodInvoker method = delegate
            {
                check_timedAction.Checked = false;
                check_timedAction_CheckedChanged(null, null);
                ManasitWorker.PlayNotification();
            };
            form.BeginInvoke(method);
        }

        public void TimedActionCounterDec(MainScreen form)
        {
            MethodInvoker method = delegate
            {
                nup_timedActionLeft.Value--;
            };
            form.BeginInvoke(method);
        }

        public void TimedActionProgressSet(MainScreen form, int num)
        {
            MethodInvoker method = delegate
            {
                pb_timedAction.Value = num;
            };
            form.BeginInvoke(method);
        }

        public void TimedActionProgressIncrement(MainScreen form)
        {
            MethodInvoker method = delegate
            {
                pb_timedAction.Increment(1);
            };
            form.BeginInvoke(method);
        }

        public void DruidHealTargetNotFound(MainScreen form)
        {
            MethodInvoker method = delegate
            {
                druid_active.Visible = false;
                pb_druidHP.Value = 0;
                l_druidHP.Text = "NaN";
            };
            form.BeginInvoke(method);
        }

        public void DruidHealTargetFound(MainScreen form, int hp)
        {
            MethodInvoker method = delegate
            {
                druid_active.Visible = true;
                pb_druidHP.Value = hp;
                l_druidHP.Text = hp.ToString() + "%";
            };
            form.BeginInvoke(method);
        }

        /* ----------------------------- DELEGATORS END ----------------------------- */

        private void FillCharacterData(DataGridView table)
        {
            if (!CheckConnected())
            {
                return;
            }
            CultureInfo cult = new CultureInfo("en-US");
            UpdateRow(table, "Data", "Id", "Value", MemoryReader.Cid().ToString());
            UpdateRow(table, "Data", "HP/MAX", "Value", MemoryReader.Hp().ToString("N0", cult) + "/" + MemoryReader.MaxHp().ToString("N0", cult));
            UpdateRow(table, "Data", "MP/MAX", "Value", MemoryReader.Mp().ToString("N0", cult) + "/" + MemoryReader.MaxMp().ToString("N0", cult));
            UpdateRow(table, "Data", "Level", "Value", MemoryReader.Level().ToString("N0", cult));
            UpdateRow(table, "Data", "Exp", "Value", MemoryReader.Exp().ToString("N0", cult));
            UpdateRow(table, "Data", "Exp to next level", "Value", MemoryReader.XpNextLevel().ToString("N0", cult));
            UpdateRow(table, "Data", "Cap", "Value", MemoryReader.Cap().ToString("N", cult));
            UpdateRow(table, "Data", "Soul", "Value", MemoryReader.Soul().ToString());
            UpdateRow(table, "Data", "Pos (x/y/z)", "Value", MemoryReader.X().ToString() + "/" +
                            MemoryReader.Y().ToString() + "/" + MemoryReader.Z().ToString());
        }

        private void UpdateRow(DataGridView table, string colName, string name, string colData, string data)
        {
            var duplicateRow = (from DataGridViewRow row in table.Rows
                                where (string)row.Cells[colName].Value == name
                                select row).FirstOrDefault();
            if (duplicateRow != null)
            {
                //duplicate row found, update it's columns value
                duplicateRow.Cells[colData].Value = data;
            }
            else
            {
                table.Rows.Add(name, data);
            }
        }

        private DataGridViewRow GetRowAlways(DataGridView table, string colName, string name)
        {
            var duplicateRow = (from DataGridViewRow row in table.Rows
                                where (string)row.Cells[colName].Value == name
                                select row).FirstOrDefault();
            if (duplicateRow == null)
            {
                int rowIndx = table.Rows.Add();
                duplicateRow = table.Rows[rowIndx];
                duplicateRow.Cells[colName].Value = name;
            }

            return duplicateRow;
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            base.OnFormClosing(e);

            if (e.CloseReason == CloseReason.WindowsShutDown) return;

            KillWithMessage("");
        }

        public bool CheckConnected()
        {
            if (Util.Tibia.HasExited)
            {
                m_killFlag = true;
            }

            return !m_killFlag;
        }

        private void KillIfNeeded()
        {
            if (m_killFlag)
            {
                this.Visible = false;
                KillWithMessage("No client found - Exiting");
            }
        }

        private void UncheckAll()
        {
            if (!m_anythingCheckedFlag)
            {
                return;
            }

            check_timedAction.Checked = false;
            cb_maptrackActive.Checked = false;
            cb_druidActivate.Checked = false;
            cb_manashield.Checked = false;
            cb_readlog.Checked = false;
            check_timedAction_CheckedChanged(null, null);
            cb_maptrackActive_CheckedChanged(null, null);
            cb_druidActivate_CheckedChanged(null, null);
            Manashield_Arrow_Ring_CheckedChanged(null, null);
            cb_readlog_CheckedChanged(null, null);

            foreach (DataGridViewRow row in HotkeysPresets.Rows)
            {
                DataGridViewCheckBoxCell cell = (DataGridViewCheckBoxCell)row.Cells[3];
                cell.Value = cell.FalseValue;
                cell.EditingCellFormattedValue = false;
            }

            m_anythingCheckedFlag = false;
        }

        private void HotkeysPresets_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0)
            {
                return;
            }
            m_anythingCheckedFlag = true;
            DataGridViewRow row = HotkeysPresets.Rows[e.RowIndex];
            DataGridViewCheckBoxCell isChecked = (DataGridViewCheckBoxCell)row.Cells["Active"];

            isChecked.Selected = false;

            DataGridViewComboBoxCell col1 = (DataGridViewComboBoxCell)row.Cells["Prefix"];
            DataGridViewTextBoxCell col2 = (DataGridViewTextBoxCell)row.Cells["Amount"];
            DataGridViewComboBoxCell col3 = (DataGridViewComboBoxCell)row.Cells["Hotkey"];

            int numericValue;
            if (col1.Value == null || col2.Value == null || col3.Value == null ||
                    !int.TryParse((string)col2.Value, out numericValue))
            {
                isChecked.EditingCellFormattedValue = false;
                MessageBox.Show("Some values in this line are invalid!");
            }
        }

        private void HotkeysPresets_CellChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0)
            {
                return;
            }
            DataGridViewRow row = HotkeysPresets.Rows[e.RowIndex];
            DataGridViewCheckBoxCell isChecked = (DataGridViewCheckBoxCell)row.Cells["Active"];

            isChecked.Selected = false;

            if (Util.OverrideCtrlShift && isChecked.Value == isChecked.TrueValue)
            { //value changed, uncheck box
                isChecked.Value = isChecked.FalseValue;
                isChecked.EditingCellFormattedValue = false;
            }
        }

        private void check_timedAction_CheckedChanged(object sender, EventArgs e)
        {
            if (check_timedAction.Checked)
            {
                pb_timedAction.Maximum = (int)nup_timedAction.Value;
                m_anythingCheckedFlag = true;
                m_manasit.SetActive(cb_afk.Checked, cb_safeAFK.Checked, (int)nup_timedActionLeft.Value,
                    (int)nup_timedAction.Value, (KeyCombo)cb_timedAction.SelectedValue);
            }
            else
            {
                pb_timedAction.Value = 0;
                m_manasit.SetInactive();
            }
        }

        private void cb_maptrackActive_CheckedChanged(object sender, EventArgs e)
        {
            if (!cb_maptrackActive.Checked)
            {
                BattleList.Rows.Clear();
                m_BLWorker.SetInactive();
            }
            else
            {
                m_anythingCheckedFlag = true;
                m_BLWorker.SetActive();
            }
        }

        private void m_cbOverrideShiftCtrl_CheckedChanged(object sender, EventArgs e)
        {
            Util.OverrideCtrlShift = m_cbOverrideShiftCtrl.Checked;
        }

        private void cb_druidActivate_CheckedChanged(object sender, EventArgs e)
        {
            if (cb_druidActivate.Checked && tb_druidName.Text.Length < 1)
            {
                cb_druidActivate.Checked = false;
                MessageBox.Show("Invalid name!");
            }
            if (cb_druidActivate.Checked)
            {
                m_anythingCheckedFlag = true;
                m_druidHeal.SetActive(tb_druidName.Text, (int)nup_druidHPTarget.Value, (KeyCombo)cb_druidHK.SelectedValue);
            }
            else
            {
                m_druidHeal.SetInactive();
                pb_druidHP.Value = 0;
                druid_active.Visible = false;
                l_druidHP.Text = "NaN";
            }
        }

        private void Manashield_Arrow_Ring_CheckedChanged(object sender, EventArgs e)
        {
            KeyCombo manashield = (KeyCombo)cb_manashieldHK.SelectedValue;
            KeyCombo arrow = (KeyCombo)cb_arrowHK.SelectedValue;
            KeyCombo ring = (KeyCombo)cb_ringHK.SelectedValue;
            int arrowCount = (int)nup_arrows.Value;

            bool activate = cb_manashield.Checked;
            activate &= (manashield != null) || (arrow != null) || (ring != null);

            if (!activate)
            {
                m_manashield.SetInactive();
            }
            else
            {
                m_anythingCheckedFlag = true;
                m_manashield.SetActive(manashield, ring, arrow, arrowCount);
            }

            if (cb_manashield.Checked != activate)
            {
                cb_manashield.Checked = activate;
            }
        }

        private void cb_afk_CheckedChanged(object sender, EventArgs e)
        {
            m_manasit.UpdateAFKStatus(cb_afk.Checked);
        }

        private void cb_safeAFK_CheckedChanged(object sender, EventArgs e)
        {
            m_manasit.UpdateSafeAFKStatus(cb_safeAFK.Checked);
        }

        private void cb_readlog_CheckedChanged(object sender, EventArgs e)
        {
            if (cb_readlog.Checked)
            {
                m_anythingCheckedFlag = true;
                m_logReader.SetActive();
            }
            else
            {
                m_logReader.SetInactive();
            }
        }

        private void StatRefresh()
        {
            m_stat.Text = getStatMessage();
        }

        private string getStatMessage()
        {
            Statistics stat = Statistics.GetInstance();
            string text = "Main: " + stat.Main + "\n" +
                          "Hotkey: " + stat.Hotkey + "\n" +
                          "BattleList: " + stat.BattleList + "\n" +
                          "Manasit: " + stat.Manasit + "\n" +
                          "DruidHeal: " + stat.DruidHeal + "\n" +
                          "Manashield: " + stat.Manashield + "\n" +
                          "Keystroke: " + stat.Keystroke + "\n" +
                          "Log: " + stat.Log + "\n";
            text += "\nMax: " + stat.GetMaxTime() + " ms";
            return text;
        }

        /*         SAVE/LOAD BASE INFRASTRUCTURE         */
        private void save_Click(object sender, EventArgs e)
        {
            // save
            PropertyFileParser.SaveProperties(HotkeysPresets.Rows, cb_druidHK, cb_manashieldHK, cb_arrowHK, cb_ringHK);
        }

        private void load_Click(object sender, EventArgs e)
        {
            // load
            Dictionary<string, IList<CompressedHotkeyTableEntry>> lines = PropertyFileParser.ReadProperties();
            LoadDataFromData(lines);
        }

        private void LoadDataFromData(Dictionary<string, IList<CompressedHotkeyTableEntry>> lines)
        {
            if (lines == null)
            {
                return;
            }

            HotkeysPresets.Rows.Clear();
            foreach (CompressedHotkeyTableEntry line in lines[PropertyFileParser.HOTKEY_NOTATION])
            {
                int index = HotkeysPresets.Rows.Add();
                HotkeysPresets.Rows[index].Cells["Prefix"].Value = m_condMapping[line.Prefix];
                HotkeysPresets.Rows[index].Cells["Amount"].Value = line.Amount;
                HotkeysPresets.Rows[index].Cells["Hotkey"].Value = m_hotkeyMapping[line.Hotkey];
            }

            // only load the FIRST occurence of each hotkey
            cb_druidHK.SelectedIndex = 0;
            if (lines[PropertyFileParser.DRUIDHEAL_NOTATION].Count > 0)
                cb_druidHK.SelectedIndex = findIndexOfHotkey(cb_druidHK, lines[PropertyFileParser.DRUIDHEAL_NOTATION][0].Hotkey);
            cb_manashieldHK.SelectedIndex = 0;
            if (lines[PropertyFileParser.MANASHIELD_NOTATION].Count > 0)
                cb_manashieldHK.SelectedIndex = findIndexOfHotkey(cb_manashieldHK, lines[PropertyFileParser.MANASHIELD_NOTATION][0].Hotkey);
            cb_arrowHK.SelectedIndex = 0;
            if (lines[PropertyFileParser.ARROW_NOTATION].Count > 0)
                cb_arrowHK.SelectedIndex = findIndexOfHotkey(cb_arrowHK, lines[PropertyFileParser.ARROW_NOTATION][0].Hotkey);
            cb_ringHK.SelectedIndex = 0;
            if (lines[PropertyFileParser.RING_NOTATION].Count > 0)
                cb_ringHK.SelectedIndex = findIndexOfHotkey(cb_ringHK, lines[PropertyFileParser.RING_NOTATION][0].Hotkey);
        }

        private int findIndexOfHotkey(ComboBox cb, string hkname)
        {
            ArrayList al = (ArrayList)cb.DataSource;
            for (int i = 0; i < al.Count; i++)
            {
                Hotkey hk = (Hotkey)(al[i]);
                if (hk.KHName.Equals(hkname))
                {
                    return i;
                }
            }
            return 0;
        }

    }
}
