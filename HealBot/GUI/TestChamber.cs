﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using HealBot.Utils;
using HealBot.Threads;

namespace HealBot.GUI
{
    public partial class TestChamber : Form
    {
        public TestChamber()
        {
            InitializeComponent();

            /*
            InitColors();
            InitSizes();
            InitGridView();
             */
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!Util.Update())
            {
                label1.Text = "failed";
                return;
            }
            /*
            string maybeIdText = textBox1.Text;
            int maybeId;
            maybeId = Int32.TryParse(maybeIdText, out maybeId) ? maybeId : 0;

            Objects.MapTile[] map = MemoryReader.getMap();
            int myId = (maybeId == 0) ? MemoryReader.Cid() : maybeId;
            bool found = false;
            UInt32 i = 0, j = 0;

            for (i = 0; i < MapAddresses.MaxTiles; i++)
            {
                if (map[i].objectCount > 0)
                {
                    for (j = 0; j < map[i].objectCount; j++)
                    {
                        if (map[i].items[j].id == 99 && map[i].items[j].count == myId)
                        {
                            found = true;
                            break;
                        }
                    }
                }
                if (found == true)
                {
                    break;
                }
            }

            int z = (int)i / (14 * 18);
            int y = ((int)i - z * 14 * 18) / 18;
            int x = ((int)i - z * 14 * 18 - y * 18);

            label1.Text = "My location is Tile " + i.ToString() + "(" + x.ToString() + "," + y.ToString() + "," + z.ToString() + "), item number " + j.ToString();
            UInt32 mapPointer = (UInt32)MemoryReader.ReadInt32(MapAddresses.MapStart + Util.Base);
            textBox2.Text = (i * MapAddresses.Step + mapPointer).ToString();
             */
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            base.OnFormClosing(e);

            if (e.CloseReason == CloseReason.WindowsShutDown) return;
        }





















        private void InitGridView()
        {
            DataGridViewTextBoxColumn quantityColumn = new DataGridViewTextBoxColumn();
            quantityColumn.DataPropertyName = "Quantity";
            quantityColumn.Name = "Quantity";
            quantityColumn.HeaderText = "Quantity";

            DataGridViewComboBoxColumn colorColumn = new DataGridViewComboBoxColumn();
            colorColumn.DataPropertyName = "ItemColor";
            colorColumn.Name = "ItemColor";
            colorColumn.HeaderText = "ItemColor";
            colorColumn.DataSource = colors;
            colorColumn.DisplayMember = "Name";
            colorColumn.ValueMember = "This";

            DataGridViewComboBoxColumn sizesColumn = new DataGridViewComboBoxColumn();
            sizesColumn.DataPropertyName = "ItemSize";
            sizesColumn.Name = "ItemSize";
            sizesColumn.HeaderText = "ItemSize";
            sizesColumn.DataSource = sizes;
            sizesColumn.DisplayMember = "Name";
            sizesColumn.ValueMember = "This";

            DataGridViewCheckBoxColumn columnCheckbox = new DataGridViewCheckBoxColumn();
            columnCheckbox.DataPropertyName = "Active";
            columnCheckbox.Name = "Active";
            columnCheckbox.HeaderText = "Active";
            columnCheckbox.TrueValue = true;
            columnCheckbox.FalseValue = false;

            dataGridView1.Columns.Add(quantityColumn);
            dataGridView1.Columns.Add(colorColumn);
            dataGridView1.Columns.Add(sizesColumn);
            dataGridView1.Columns.Add(columnCheckbox);
        }

        private void InitColors()
        {
            colors = new List<ItemColor>();
            colors.Add(new ItemColor(1, "Red", Color.Red));
            colors.Add(new ItemColor(2, "Green", Color.Green));
            colors.Add(new ItemColor(3, "Yellow", Color.Yellow));
        }

        private void InitSizes()
        {
            sizes = new List<ItemSize>();
            sizes.Add(new ItemSize(1, "Small", 1));
            sizes.Add(new ItemSize(2, "Medium", 5));
            sizes.Add(new ItemSize(3, "Large", 10));
        }


        private IList<ItemSize> sizes;
        private IList<ItemColor> colors;
        private IList<OrderItem> orderedItems;

        private void button2_Click(object sender, EventArgs e)
        {
            orderedItems = new List<OrderItem>();

            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                if (!row.IsNewRow)
                {
                    OrderItem itemOrdered = new OrderItem();
                    itemOrdered.Quantity = Convert.ToInt32(row.Cells["Quantity"].Value);
                    itemOrdered.ItemColor = row.Cells["ItemColor"].Value as ItemColor;
                    itemOrdered.ItemSize = row.Cells["ItemSize"].Value as ItemSize;
                    orderedItems.Add(itemOrdered);
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = null;
            dataGridView1.Rows.Clear();
            dataGridView1.Columns.Clear();

            if (dataGridView1.Columns.Count <= 0)
            {
                InitGridView();
            }
            dataGridView1.DataSource = orderedItems;
        }

        public class OrderItem {

            public int Quantity { get; set; }

            public ItemColor ItemColor { get; set; }

            public ItemSize ItemSize { get; set; }

            public OrderItem() { }

            public OrderItem(int quantity, ItemColor color, ItemSize size)
            {
                Quantity = quantity;
                ItemColor = color;
                ItemSize = size;
            }
        }
    
        public class ItemColor
        {
            public int Id { get; set; }

            public string Name { get; set; }

            public Color ColorValue { get; set; }

            public ItemColor This { get; private set; }

            public ItemColor() { }

            public ItemColor(int id, string name, Color value)
            {
                Id = id;
                Name = name;
                ColorValue = value;
                This = this;
            }
        }

        public class ItemSize {

            public int Id { get; set; }

            public string Name { get; set; }

            public int SizeValue { get; set; }

            public ItemSize This { get; private set; }

            public ItemSize() { }

            public ItemSize(int id, string name, int value)
            {
                Id = id;
                Name = name;
                SizeValue = value;
                This = this;
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (!Util.Update())
            {
                label1.Text = "failed";
                return;
            }

            bool iss = MemoryReader.IsStatusActive(PlayerFlags.FlagInProtectionZone);
            label2.Text = "Protection zone is: " + (iss ? "on" : "off");
        }

    }
}
