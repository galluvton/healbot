﻿using System;
using System.Windows.Forms;
using HealBot.GUI;
using HealBot.Utils;

namespace HealBot.Main
{

    class Program
    {

        [STAThread]
        static void Main(string[] args)
        {
            if (CommandLineArgs.IsHelpNeeded(args))
            {
                CommandLineArgs.PrintHelp();
                return;
            }

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            CommandLineArgs cmd = new CommandLineArgs(args);

            Util.setKeyPressQueue(cmd.QueueMode);
            Util.VersionIgnore = cmd.VersionIgnore;
            Util.updateAddresses(cmd.AddressesFile);

            Form baseForm;
            if (cmd.DebugMode)
            {
                // Debug mode
                baseForm = new TestChamber();
            }
            else
            {
                string file = null;
                if (!cmd.LoadFromFile.Equals(CommandLineArgs.DEFAULT_LOAD_FROM_FILE))
                {
                    file = cmd.LoadFromFile;
                }
                baseForm = new MainScreen(cmd.RefreshRate, cmd.QueueMode, cmd.DumbStat, file);
            }
            
            Application.Run(baseForm);
        }
    }
}
