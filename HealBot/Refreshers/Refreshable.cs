﻿
namespace HealBot.Refreshers
{
    public interface Refreshable
    {
        void RefreshMe();
    }
}
