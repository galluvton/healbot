﻿using System.Windows.Forms;

namespace HealBot.Refreshers
{
    public class RefreshableForm : Form, Refreshable
    {
        // placeholder, can't abstract because of C#
        public virtual void RefreshMe() { }
    }
}
