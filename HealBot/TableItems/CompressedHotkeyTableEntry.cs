﻿
namespace HealBot.TableItems
{
    public class CompressedHotkeyTableEntry
    {
        public string Amount { get; set; }

        public string Prefix { get; set; }

        public string Hotkey { get; set; }

        public CompressedHotkeyTableEntry(string amount, string cond, string hotkey)
        {
            Amount = amount.TrimEnd('\r', '\n');
            Prefix = cond.TrimEnd('\r', '\n');
            Hotkey = hotkey.TrimEnd('\r', '\n');
        }
    }
}
