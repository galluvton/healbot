﻿
namespace HealBot.TableItems
{
    public class Hotkey
    {

        public string KHName { get; set; }

        public KeyCombo KHValue { get; set; }

        public Hotkey This { get; private set; }

        public Hotkey(string name, KeyCombo value)
        {
            KHName = name;
            KHValue = value;
            This = this;
        }

    }
}
