﻿
namespace HealBot.TableItems
{
    public class HpMpCondItem
    {
        public const string HPPlusText = "HP Greater than";
        public const string HPPlus = "HP+";
        public const string HPMinusText = "HP Smaller than";
        public const string HPMinus = "HP-";
        public const string MPPlusText = "MP Greater than";
        public const string MPPlus = "MP+";
        public const string MPMinusText = "MP Smaller than";
        public const string MPMinus = "MP-";

        public string CondName { get; set; }

        public string CondValue { get; set; }

        public HpMpCondItem This { get; private set; }

        public HpMpCondItem(string name, string value)
        {
            CondName = name;
            CondValue = value;
            This = this;
        }

        public override string ToString()
        {
            return CondValue;
        }
    }
}
