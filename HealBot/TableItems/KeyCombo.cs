﻿using HealBot.Utils;

namespace HealBot.TableItems
{
    public class KeyCombo
    {

        public Constants.KeyboardControls KHValue { get; set; }

        public Constants.KeyboardControls CtrlShift { get; set; }

        public KeyCombo This { get; private set; }

        public KeyCombo(Constants.KeyboardControls ctrlShift, Constants.KeyboardControls value)
        {
            KHValue = value;
            CtrlShift = ctrlShift;
            This = this;
        }

    }
}
