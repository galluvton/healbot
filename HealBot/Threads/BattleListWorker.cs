﻿using System;
using System.Collections.Generic;
using System.Threading;
using HealBot.Utils;
using HealBot.GUI;
using HealBot.TableItems;

namespace HealBot.Threads
{
    class BattleListWorker : BaseWorker
    {
        public BattleListWorker(MainScreen form, double refreshRate, ThreadPriority priority)
            : base(form, refreshRate, priority)
        { }

        public override string GetWorkerName()
        {
            return "BattleListWorker";
        }

        public void SetActive()
        {
            base.ActivateWorker();
        }

        public void SetInactive()
        {
            base.DeactivateWorker();
        }

        public override void RunLogic()
        {
            List<Creature> creatures = new List<Creature>();

            int myX = MemoryReader.X();
            int myY = MemoryReader.Y();
            int myZ = MemoryReader.Z();
            MemoryReader.FilterArgs filt = new MemoryReader.FilterArgs((UInt32)m_form.NUPMaxX,
                                            (UInt32)m_form.NUPMaxY, (UInt32)m_form.NUPMaxZ);
            List<Objects.BList> battleListArray = MemoryReader.BlGet(false, filt);

            foreach (Objects.BList crit in battleListArray)
            {
                if (crit.Id == MemoryReader.Cid())
                {
                    continue;
                }

                int xRelative = crit.X - myX;
                int yRelative = crit.Y - myY;
                int zRelative = myZ - crit.Z;

                Creature c = new Creature();
                c.MyName = crit.Name;
                c.MyFloor = zRelative.ToString();
                c.MyXy = xRelative.ToString() + "/" + yRelative.ToString();
                c.MyOffset = crit.Offset;
                c.MyId = crit.Id;
                creatures.Add(c);
            }

            m_form.BattleListUpdateTable(m_form, creatures);
        }

        public override void UpdateStatistics(long time)
        {
            Statistics.GetInstance().BattleList = (time / TimeSpan.TicksPerMillisecond).ToString();
        }

    }
}
