﻿using System;
using System.Collections.Generic;
using System.Threading;
using HealBot.Utils;
using HealBot.GUI;
using HealBot.TableItems;

namespace HealBot.Threads
{
    public class DruidHealWorker : BaseWorker
    {
        private string m_targetName;
        private int m_targetHP;
        private KeyCombo m_selectedHK;

        public DruidHealWorker(MainScreen form, double refreshRate, ThreadPriority priority)
            : base(form, refreshRate, priority)
        { }

        public override string GetWorkerName()
        {
            return "DruidHealWorker";
        }

        public void SetActive(string name, int hp, KeyCombo kc)
        {
            m_targetName = name.ToLower();
            m_targetHP = hp;
            m_selectedHK = kc;

            base.ActivateWorker();
        }

        public void SetInactive()
        {
            base.DeactivateWorker();
        }

        public override void RunLogic()
        {
            List<Objects.BList> battleListArray = MemoryReader.BlGetPlayersOnScreen(m_targetName);
            int hp = -1;

            if (battleListArray.Count > 0)
            {
                hp = battleListArray[0].Hppc;
            }

            if (hp < 0)
            {
                // name not found, return
                m_form.DruidHealTargetNotFound(m_form);
                return;
            }

            if (hp <= m_targetHP)
            {
                Util.sendKeystroke(Util.PRIO_MID, m_selectedHK.KHValue.ToString(), m_selectedHK.CtrlShift.ToString());
            }

            (new Thread(this.updateFormGUI)).Start(hp);
        }

        public override void UpdateStatistics(long time)
        {
            Statistics.GetInstance().DruidHeal = (time / TimeSpan.TicksPerMillisecond).ToString();
        }

        private void updateFormGUI(object hpAsInt)
        {
            Thread.CurrentThread.Priority = ThreadPriority.Normal;
            m_form.DruidHealTargetFound(m_form, (int)hpAsInt);
        }

    }
}
