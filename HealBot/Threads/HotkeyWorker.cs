﻿using System;
using System.Windows.Forms;
using System.Threading;
using HealBot.Utils;
using HealBot.GUI;
using HealBot.TableItems;
using HealBot.Refreshers;

namespace HealBot.Threads
{
    class HotkeyWorker : BaseWorker
    {

        public HotkeyWorker(MainScreen form, double refreshRate, ThreadPriority priority)
            : base(form, refreshRate, priority)
        { }

        public override string GetWorkerName()
        {
            return "HotkeyWorker";
        }

        public void SetActive()
        {
            base.ActivateWorker();
        }

        public void SetInactive()
        {
            base.DeactivateWorker();
        }

        public override void RunLogic()
        {
            foreach (DataGridViewRow row in m_form.KHPresetsRows)
            {
                
                DataGridViewCheckBoxCell isChecked = (DataGridViewCheckBoxCell)row.Cells["Active"];
                if (row.IsNewRow || (bool)isChecked.EditingCellFormattedValue != true)
                {
                    continue;
                }

                DataGridViewComboBoxCell col1 = (DataGridViewComboBoxCell)row.Cells["Prefix"];
                DataGridViewTextBoxCell col2 = (DataGridViewTextBoxCell)row.Cells["Amount"];
                DataGridViewComboBoxCell col3 = (DataGridViewComboBoxCell)row.Cells["Hotkey"];

                int numericValue;
                bool sanity = int.TryParse((string)col2.Value, out numericValue);
                if (!sanity)
                {
                    m_form.KillWithMessage("Failed sanity check in row " + isChecked.RowIndex);
                }

                ActivateHotkey((HpMpCondItem)col1.Value, (Hotkey)col3.Value, numericValue);
            }
        }

        private void ActivateHotkey(HpMpCondItem cond, Hotkey hotkey, int number)
        {
            int value;
            switch (cond.CondValue)
            {
                case HpMpCondItem.HPPlus:
                    value = MemoryReader.Hp();
                    if (value > 0 && value <= number) return;
                    break;
                case HpMpCondItem.HPMinus:
                    value = MemoryReader.Hp();
                    if (value > 0 && value > number) return;
                    break;
                case HpMpCondItem.MPPlus:
                    value = MemoryReader.Mp();
                    if (value > 0 && value <= number) return;
                    break;
                case HpMpCondItem.MPMinus:
                    value = MemoryReader.Mp();
                    if (value > 0 && value > number) return;
                    break;
                default:
                    m_form.KillWithMessage("Failed sanity condition check with values " + cond +
                                            ", " + hotkey + ", " + number.ToString());
                    break;
            }

            KeyCombo KC = hotkey.KHValue;

            Util.sendKeystroke(Util.PRIO_TOP, KC.KHValue.ToString(), KC.CtrlShift.ToString());
        }

        public override void UpdateStatistics(long time)
        {
            Statistics.GetInstance().Hotkey = (time / TimeSpan.TicksPerMillisecond).ToString();
        }

    }
}
