﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using HealBot.GUI;
using HealBot.Utils;
using HealBot.Utils.TibiaLogs;

namespace HealBot.Threads
{
    public class LogReaderWorker : BaseWorker
    {
        private const int LIST_SIZE = 40;
        private SlidingMessageWindow m_headMessages;
        private MessageWrapper m_lastMessage;
        private string m_stamp;

        public LogReaderWorker(MainScreen form, ThreadPriority priority)
            : base(form, 1, priority)
        {
            m_headMessages = new SlidingMessageWindow(LIST_SIZE);
            m_lastMessage = MessageWrapper.EmptyMessage;
            m_stamp = System.DateTime.Now.ToString("dd-MM-yyyy--mm-ss");
        }

        public override string GetWorkerName()
        {
            return "LogReaderWorker";
        }

        public void SetActive()
        {
            base.ActivateWorker();
        }

        public void SetInactive()
        {
            base.DeactivateWorker();
        }

        public override void RunLogic()
        {
            List<string> tabMessages = new List<string>();
            IEnumerable<Objects.ChatStruct> chatTabs = MemoryReader.GetChatTabs();
            foreach (Objects.ChatStruct tab in chatTabs){
                // only read log messages from the server log
                if ("Server Log".Equals(tab.Name))
                {
                    tabMessages = ReadTabMessages(tab.DataPointer);
                    break;
                }
            }
            foreach (string logLine in tabMessages)
            {
                Util.lootLog(logLine, m_stamp);
            }
        }

        private List<string> ReadTabMessages(UInt32 logMessagesDataPointer)
        {
            List<string> ans = new List<string>();
            // read first message
            MessageWrapper msg = new MessageWrapper(MemoryReader.ReadUInt32(logMessagesDataPointer + LogAddresses.LogContentMessagePointer));
            if (msg.IsEmpty()) // log is empty
            {
                return ans;
            }

            // look for the head log message in the sliding window
            int found = m_headMessages.IndexOf(msg);

            if (found == -1) // log cleared
            {
                m_headMessages.Clear();
                for (int i = 0; i < LIST_SIZE && !msg.IsEmpty(); i++)
                {
                    m_headMessages.Add(msg);
                    ans.Add(msg.Message);
                    m_lastMessage = msg;
                    msg = msg.GetNextMessage();
                }
            }
            else if (found > 0) // window valid, but needs updating
            {
                MessageWrapper tmpMsg = m_headMessages.Last().GetNextMessage();
                for (int i = 0; i < found && !tmpMsg.IsEmpty(); i++) // 'found' indicates how many messages we need to read
                {
                    m_headMessages.Add(tmpMsg);
                    tmpMsg = tmpMsg.GetNextMessage();
                }
            }

            if (found >= 0) // sliding window is still valid
            {
                msg = m_lastMessage.GetNextMessage(); // keep reading from the last message
            } // else - msg already holds the last message needed (head)

            while (!msg.IsEmpty())
            {
                ans.Add(msg.Message);
                m_lastMessage = msg;
                if (m_headMessages.Count < LIST_SIZE)
                {
                    m_headMessages.Add(msg);
                }
                msg = msg.GetNextMessage();
            }

            return ans;
        }

        public override void UpdateStatistics(long time)
        {
            Statistics.GetInstance().Log = (time / TimeSpan.TicksPerMillisecond).ToString();
        }

    }
}