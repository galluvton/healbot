﻿using System;
using System.Threading;
using HealBot.Utils;
using HealBot.GUI;
using HealBot.TableItems;

namespace HealBot.Threads
{
    class ManashieldWorker : BaseWorker
    {

        private const long MANASHIELD_MIN_TIME = 180; // seconds
        private KeyCombo m_selectedManashieldHK;
        private KeyCombo m_selectedRingHK;
        private KeyCombo m_selectedArrowHK;
        private int m_arrowCount;
        private long m_refreshTime;
        private double m_acceptRate;
        private Random rand;

        public ManashieldWorker(MainScreen form, double refreshRate, ThreadPriority priority)
            : base(form, refreshRate, priority)
        {
            m_refreshTime = 0;
            m_acceptRate = 10 * refreshRate;
            rand = new Random((int)DateTime.Now.Ticks & 0x0000FFFF);
        }

        public override string GetWorkerName()
        {
            return "ManashieldWorker";
        }

        public void SetActive(KeyCombo manashieldKC, KeyCombo ringKC, KeyCombo arrowKC, int arrowCount)
        {
            m_selectedManashieldHK = manashieldKC;
            m_selectedRingHK = ringKC;
            m_selectedArrowHK = arrowKC;
            m_arrowCount = arrowCount;

            base.ActivateWorker();
        }

        public void SetInactive()
        {
            base.DeactivateWorker();
        }

        public override void RunLogic()
        {
            if (m_selectedManashieldHK != null && CheckManashieldStatus())
            {
                Util.sendKeystroke(Util.PRIO_MID, m_selectedManashieldHK.KHValue.ToString(), m_selectedManashieldHK.CtrlShift.ToString());
                m_refreshTime = (rand.Next(20) + MANASHIELD_MIN_TIME) * TimeSpan.TicksPerSecond + DateTime.Now.Ticks;
            }
            if (m_selectedArrowHK != null && (MemoryReader.GetArrowSlotId() == 0 || MemoryReader.GetArrowSlotCount() < m_arrowCount))
            {
                Util.sendKeystroke(Util.PRIO_MID, m_selectedArrowHK.KHValue.ToString(), m_selectedArrowHK.CtrlShift.ToString());
            }
            if (m_selectedRingHK != null && MemoryReader.GetRingSlotId() == 0)
            {
                Util.sendKeystroke(Util.PRIO_MID, m_selectedRingHK.KHValue.ToString(), m_selectedRingHK.CtrlShift.ToString());
            }
        }

        private bool CheckManashieldStatus()
        {
            if (!MemoryReader.IsStatusActive(PlayerFlags.FlagManashield))
            {
                return true;
            }
            return (DateTime.Now.Ticks >= m_refreshTime);
        }

        public override void UpdateStatistics(long time)
        {
            Statistics.GetInstance().Manashield = (time / TimeSpan.TicksPerMillisecond).ToString();
        }

    }
}
