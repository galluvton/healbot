﻿using System;
using System.Collections.Generic;
using System.Threading;
using HealBot.Utils;
using HealBot.GUI;
using HealBot.TableItems;

namespace HealBot.Threads
{
    class ManasitWorker : BaseWorker
    {
        public static readonly int AFK_MOVE_TIME = 10 * 60; //seconds = 10 min
        public static readonly int WIGGLE_DELEY = 300; //ms

        private int m_numOfActionsLeft;
        private int m_actionDowntime;
        private int m_actionDowntimeCounter;
        private bool m_afk;
        private bool m_safeAFK;
        private int m_afkCounter;
        private KeyCombo m_selectedHK;

        public ManasitWorker(MainScreen form, ThreadPriority priority)
            : base(form, 1, priority)
        { }

        public override string GetWorkerName()
        {
            return "ManasitWorker";
        }

        public void SetActive(bool afk, bool safe, int numOfActions, int actionDowntime, KeyCombo kc)
        {
            m_numOfActionsLeft = numOfActions;
            m_actionDowntime = actionDowntime;
            m_actionDowntimeCounter = 0;
            m_afk = afk;
            m_safeAFK = safe;
            m_afkCounter = AFK_MOVE_TIME;
            m_selectedHK = kc;

            base.ActivateWorker();
        }

        public void SetInactive()
        {
            base.DeactivateWorker();
        }

        public void UpdateAFKStatus(bool input)
        {
            m_afk = input;
        }

        public void UpdateSafeAFKStatus(bool input)
        {
            m_safeAFK = input;
        }

        public override void RunLogic()
        {
            if ((m_numOfActionsLeft == 0) && (m_actionDowntimeCounter == 0))
            {
                m_form.TimedActionStop(m_form);
            }
            if (m_afk)
            {
                HandleAFK();
            }
            if (m_safeAFK)
            {
                if (HandleSafeAFK()) return;
            }
            HandleManasitting();
        }

        public override void UpdateStatistics(long time)
        {
            Statistics.GetInstance().Manasit = (time / TimeSpan.TicksPerMillisecond).ToString();
        }

        private void HandleManasitting()
        {
            if (m_actionDowntimeCounter == 0)
            {
                m_actionDowntimeCounter = m_actionDowntime;
                m_form.TimedActionProgressSet(m_form, 0);

                if (m_numOfActionsLeft != -1 && m_numOfActionsLeft != 0)
                {
                    m_numOfActionsLeft--;
                    m_form.TimedActionCounterDec(m_form);
                }
                Util.sendKeystroke(Util.PRIO_BOT, m_selectedHK.KHValue.ToString(), m_selectedHK.CtrlShift.ToString());
            }
            else
            {
                m_actionDowntimeCounter--;
                m_form.TimedActionProgressIncrement(m_form);
            }
        }

        private void HandleAFK()
        {
            if (m_afkCounter == 0)
            {
                (new Thread(this.Wiggle)).Start();
                m_afkCounter = AFK_MOVE_TIME;
            }
            else
            {
                m_afkCounter--;
            }
        }

        private bool HandleSafeAFK()
        {
            List<Objects.BList> battleListArray = MemoryReader.BlGet(false, MemoryReader.IN_SCREEN);

            foreach (Objects.BList crit in battleListArray)
            {
                if (crit.Id == MemoryReader.Cid())
                {
                    continue;
                }
                // someone on my screen - stop the bot
                m_form.TimedActionStop(m_form);
                return true;
            }
            return false;
        }

        private void Wiggle()
        {
            Thread.CurrentThread.Priority = ThreadPriority.Lowest;
            int temp = new Random().Next(0, 2);

            for (int i = 0; i < 2; i++)
            {
                Util.sendKeystroke(Util.PRIO_BOT, ((int)Constants.KeyboardControls.left + temp * 2).ToString(), Constants.INVALID_KEY);
                Thread.Sleep(WIGGLE_DELEY);
                Util.sendKeystroke(Util.PRIO_BOT, ((int)Constants.KeyboardControls.left + (1 - temp) * 2).ToString(), Constants.INVALID_KEY);
                Thread.Sleep(WIGGLE_DELEY);
            }
        }

        public static void PlayNotification()
        {
            System.Media.SystemSounds.Beep.Play();
            System.Threading.Thread.Sleep(100);
            System.Media.SystemSounds.Beep.Play();
        }

    }
}
