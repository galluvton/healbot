﻿using System;

namespace HealBot.Utils
{
    static class TibiaVerNum
    {
        public static string ClientVersion = "10.9.9.0";
    }

    static class Addresses
    {
        public static UInt32 Cid = 0x6E2050;
        public static UInt32 Xor = 0x544FD8;
        public static UInt32 Hp = 0x6E2000; // Xor'ed
        public static UInt32 MaxHp = 0x6E2048; // Xor'ed
        public static UInt32 Mp = 0x54500C; // Xor'ed
        public static UInt32 MaxMp = 0x544FDC; // Xor'ed
        public static UInt32 Cap = 0x6E2040; // Xor'ed
        public static UInt32 Soul = 0x544FF4;
        public static UInt32 XPos = 0x6E2054;
        public static UInt32 YPos = 0x6E2058;
        public static UInt32 ZPos = 0x6E205C;
        public static UInt32 Exp = 0x544FE0;
        public static UInt32 IsConnected = 0x556ADC;
        public static UInt32 Level = 0x544FF0;
        public static UInt32 ChatLogStart = 0x5452C0;
    }

    static class LogAddresses
    { 
        public static readonly UInt32[] LogTabsMagics = { 0x24, 0x10, 0x10, 0x30, 0x24 };
        public const UInt32 LogTabsToLogNamePointer = 0x2C;
        public const UInt32 LogNameSize = 16;
        public const UInt32 LogTabsToLogContent = 0x24;
        public const UInt32 LogTabNext = 0x10;
        public const UInt32 LogContentMessagePointer = 0x10;
        public const UInt32 LogContentMessageDoublePointer = 0x4C; // tibia's amazing int******* structures :)
        public const UInt32 LogContentMessageSize = 255; //max message input is 255 characters, but some can go as high as 400. Still, only read normal messages
        public const UInt32 LogContentNext = 0x5C;
    }
    static class PlayerFlags
    {
        public static UInt32 FlagAddress = 0x544F5C;
        public const UInt32 FlagPoisened = 0x1;
        public const UInt32 FlagBurning = 0x2;
        public const UInt32 FlagEnergiezed = 0x4;
        public const UInt32 FlagDrunked = 0x8;
        public const UInt32 FlagManashield = 0x10;
        public const UInt32 FlagParalized = 0x20;
        public const UInt32 FlagHasted = 0x40;
        public const UInt32 FlagBattle = 0x80;
        public const UInt32 FlagDrowning = 0x100;
        public const UInt32 FlagFreezeing = 0x200;
        public const UInt32 FlagDazzled = 0x400;
        public const UInt32 FlagCursed = 0x800;
        public const UInt32 FlagBuffed = 0x1000;
        public const UInt32 FlagNoLogoutOrProtectionZone = 0x2000;
        public const UInt32 FlagInProtectionZone = 0x4000;
        public const UInt32 FlagBleeding = 0x8000;
    }

    static class Equipment
    {
        public static UInt32 ArrowSlotStart = 0x782FFC;
        public const UInt32 ArrowSlotAmount = 0x0;
        public const UInt32 ArrowSlotItemId = 0x4;
        public static UInt32 RingSlotStart = 0x783020;
        public const UInt32 RingSlotItemId = 0x0;
        public static UInt32 AmuletSlotStart = 0x783100;
        public const UInt32 AmuletSlotItemId = 0x0;
    }

    static class BListAdresses
    {
        public static UInt32 Start = 0x73CF70;
        public const UInt32 Step = 0xDC;
        public const int Max = 1300; // const
        // creature data (repeates in the battlelist)
        public const UInt32 IdOffset = 0x0;
        public const UInt32 TypeOffset = 0x3;
        public const UInt32 NameOffset = 0x4;
        public const UInt32 ZOffset = 0x24;
        public const UInt32 YOffset = 0x28;
        public const UInt32 XOffset = 0x2C;
        public const UInt32 DisplacementY = 0x30;
        public const UInt32 DisplacementX = 0x34;
        public const UInt32 DirectionOffset = 0x38;
        public const UInt32 TimeLastMovedOffset = 0x3C;
        public const UInt32 WalkingDirectionY = 0x44; // 0x20 if moved in Y otherwise 0x00
        public const UInt32 WalkingDirectionX = 0x48; // 0x20 if moved in X otherwise 0x00
        public const UInt32 GroundSpeed = 0x4C;
        public const UInt32 IsWalking = 0x50;
        public const UInt32 LastWalkedDirection = 0x54;
        public const UInt32 LookType = 0x60;
        public const UInt32 LookHead = 0x64;
        public const UInt32 LookBody = 0x68;
        public const UInt32 LookLegs = 0x6C;
        public const UInt32 LookFeet = 0x70;
        public const UInt32 LookAddon = 0x74;
        public const UInt32 LookMount = 0x78;
        public const UInt32 LightRadius = 0x7C;
        public const UInt32 LightColor = 0x80;
        public const UInt32 BlackSquareOffset = 0x88;
        public const UInt32 HppcOffset = 0x8C;
        public const UInt32 SpeedOffset = 0x90;
        public const UInt32 IsBlocking = 0x94;
        public const UInt32 SkullOffset = 0x98;
        public const UInt32 PartyOffset = 0x9C;
        public const UInt32 GuildWarFlag = 0xA0;
        public const UInt32 IsVisible = 0xA4;
        public const UInt32 BorderColorRed = 0xA8;
        public const UInt32 BorderColorGreen = 0xAC;
        public const UInt32 BorderColorBlue = 0xB0;
        public const UInt32 BorderVisible = 0xB4;
        public const UInt32 NumInGuildOrParty = 0xB8;
        public const UInt32 TypeB = 0xBC;
    }

    static class Cooldowns
    {
        public const UInt32 CooldownCategoryStart = 0x588A54; // ----------OLD!!
        public const UInt32 CooldownStartOffsetForEachCategory = 0x0C; // start time (time when first cast)
        public const UInt32 CooldownEndOffsetForEachCategory = 0x10; // end time (when do stop CD)
        public const int AttackCooldownPointerDepth = 1;
        public const int HealCooldownPointerDepth = 2;
        public const int SupportCooldownPointerDepth = 3;
        public const int SpecialCooldownPointerDepth = 4;
    }

    /*
    class Unclassified 10.81
    {
        tibiaModuleRegionSize=0x420000
        LoginServerStartPointer=0x98B094
        LoginServerStep=0x30
        HostnamePointerOffset=0x4
        IPAddressPointerOffset=0x20
        PortOffset=0x28
        adrMulticlient=0x5AA885
        LEVELSPY_NOP=0x567570
        LEVELSPY_ABOVE=0x56756C
        LEVELSPY_BELOW=0x567574
        LIGHT_TRICK_ADR=0x587842
        LIGHT_TRICK_CODE=BBFF000000EB11909090
        adrRSA=0x85DA30
        tibiachatlog_struct=0x933970>0x40>0x3C
        tibiachatlog_selchannel=0x933970>0x40>0x30>0x30
        tibia_popup_title=0x93395C>0x54
        MAP_POINTER_ADDR=0xB73ADC
        OFFSET_POINTER_ADDR=0xB78608
        adrConnectionKey=0x91DBA4
        LAST_BATTLELISTPOS=1299
        adrLastPacket=0x943DC2
        adrSelectedCharIndex=0xACDEF0
        adrCharListPtr=0xACDE68
        adrCharListPtrEND=0xACDE6C
        adrPointerToInternalFPSminusH5D=0xB1EDB0
        adrNumberOfAttackClick=0xAD1E38
    }
    */
}
