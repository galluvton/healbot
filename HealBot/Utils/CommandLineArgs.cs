﻿using System;

namespace HealBot.Utils
{
    public class CommandLineArgs
    {

        private const double DEFAULT_REFRESH_RATE = 0.3;
        private const bool DEFAULT_DEBUG_MODE = false;
        private const bool DEFAULT_QUEUE_MODE = false;
        private const bool DEFAULT_DUMP_STAT = false;
        public const string DEFAULT_LOAD_FROM_FILE = "";
        private const bool DEFAULT_VERSION_IGNR = false;
        private const string DEFAULT_ADDRESSES_FILE = "";
        private static readonly string ST_HELP_TOKEN = "/h";
        private static readonly string ST_REFRESH_RATE = "r";
        private static readonly string ST_DEBUG_MODE = "d";
        private static readonly string ST_QUEUE_MODE = "q";
        private static readonly string ST_DUMP_STAT = "s";
        private static readonly string ST_LOAD_FROM_FILE = "l";
        private static readonly string ST_VERSION_IGNR = "v";
        private static readonly string ST_ADDRESSES = "a";

        public double RefreshRate;
        public bool DebugMode;
        public bool QueueMode;
        public bool DumbStat;
        public string LoadFromFile;
        public bool VersionIgnore;
        public string AddressesFile;

        public CommandLineArgs(string[] args)
        {
            RefreshRate = DEFAULT_REFRESH_RATE;
            DebugMode = DEFAULT_DEBUG_MODE;
            QueueMode = DEFAULT_QUEUE_MODE;
            QueueMode = DEFAULT_QUEUE_MODE;
            LoadFromFile = DEFAULT_LOAD_FROM_FILE;
            VersionIgnore = DEFAULT_VERSION_IGNR;
            AddressesFile = DEFAULT_ADDRESSES_FILE;

            bool refreshFound = false;
            bool debugFound = false;
            bool queueFound = false;
            bool statFound = false;
            bool loadFileFound = false;
            bool versionIgnoreFound = false;
            bool addressesFound = false;

            for (int i = 0; i < args.Length; i++)
            {
                if (!refreshFound && args[i].ToLower().Equals(ST_REFRESH_RATE) && (i + 1) < args.Length)
                {
                    i++;
                    double temp;
                    if (Double.TryParse(args[i], out temp))
                    {
                        RefreshRate = temp;
                        refreshFound = true;
                    }
                }
                else if (!debugFound && args[i].ToLower().Equals(ST_DEBUG_MODE))
                {
                    DebugMode = true;
                    debugFound = true;
                }
                else if (!queueFound && args[i].ToLower().Equals(ST_QUEUE_MODE))
                {
                    QueueMode = true;
                    queueFound = true;
                }
                else if (!statFound && args[i].ToLower().Equals(ST_DUMP_STAT))
                {
                    DumbStat = true;
                    statFound = true;
                }
                else if (!loadFileFound && args[i].ToLower().Equals(ST_LOAD_FROM_FILE) && (i + 1) < args.Length)
                {
                    i++;
                    LoadFromFile = args[i];
                    loadFileFound = true;
                }
                else if (!versionIgnoreFound && args[i].ToLower().Equals(ST_VERSION_IGNR))
                {
                    VersionIgnore = true;
                    versionIgnoreFound = true;
                }
                else if (!addressesFound && args[i].ToLower().Equals(ST_ADDRESSES) && (i + 1) < args.Length)
                {
                    i++;
                    AddressesFile = args[i];
                    addressesFound = true;
                }
            }
        }

        public static bool IsHelpNeeded(string[] args)
        {
            for (int i = 0; i < args.Length; i++)
            {
                if (args[i].ToLower().Equals(ST_HELP_TOKEN))
                {
                    return true;
                }
            }
            return false;
        }

        public static void PrintHelp()
        {
            string message = "Bot options:\n\n"+
                             "/h - help menu\n"+
                             "r - refresh rate. must be followed by a number\n"+
                             "d - debug mode. DEPRECATED\n"+
                             "q - queue mode. expanded help on queue mode later\n"+
                             "s - statistics dump. on close, bot will log statistics to a file\n"+
                             "l - load file on startup. the bot will load a configuration file when starting up. "+
                             "must be followed by the file (full path)\n"+
                             "v - ignore version. will force the bot to work even if it isn't set for this client version\n"+
                             "a - addresses file. load memory addresses from a file";

            message +=       "\nExpanded explenations:\n\n" +
                             "r - refresh rate:\ndefault: 0.3\nthe sample rate for the bot.\n\n" +
                             "q - queue mode:\ndefault: off\nwhen queue mode is off, every key press the bot tries to simulate "+
                             "is sent to the keyboard as soon as it can, regardless of what component sent it.\non queue mode, "+
                             "the bot gives every component a priorty rank, and will always try to send the highest ranking "+
                             "press that is waiting for a click. note that on queue mode, the checkbox for overriding CTRL/SHIFT is ignored. "+
                             "The ranking is:\nTop: hotey\nMid: druidheal, manashield(shield, arrows, rings)\nBot: manasit\n\n" +
                             "s - statistics dump:\ndefault: off\non close, bot will log statistics to a file named"+
                             " 'botCrashReport.txt' in the same folder as the bot\n\n" +
                             "v - ignore version:\ndefault: off\n will force the bot to work even if it isn't set for this client version.\n"+
                             "current version is " + TibiaVerNum.ClientVersion + ", note that turning the bot with the wrong version "+
                             "could cause the bot to fail, and read false values (if your hp/mp are not correct, DONT USE THE BOT)";
            System.Windows.Forms.MessageBox.Show(message);
        }

    }
}
