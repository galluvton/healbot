﻿﻿using System;

namespace HealBot.Utils
{
    public class Constants
    {
        public enum KeyboardControls : uint
        {
            F1 = 0x70,
            F2 = 0x71,
            F3 = 0x72,
            F4 = 0x73,
            F5 = 0x74,
            F6 = 0x75,
            F7 = 0x76,
            F8 = 0x77,
            F9 = 0x78,
            F10 = 0x79,
            F11 = 0x7A,
            F12 = 0x7B,
            left = 0x25,
            up = 0x26,
            right = 0x27,
            down = 0x28,
            ctrl = 0x11,
            shift = 0x10,
            lbtndn = 0x0201,
            lbtnup = 0x0202,
            invalid = 0x0
        }

        public const string INVALID_KEY = "invalid";

        public const Int32 WM_CHAR = 0x0102;
        public const Int32 WM_KEYDOWN = 0x0100;
        public const Int32 WM_KEYUP = 0x0101;
        public const Int32 VK_RETURN = 0x0D;
        public const Int32 WM_SYSKEYDOWN = 0x0104;
        public const Int32 WM_SYSKEYUP = 0x0105;

        public const Int32 SCREEN_X_RADIUS = 7;
        public const Int32 SCREEN_Y_RADIUS = 5;

        public const Int32 CREATURE_TYPE_PLAYER = 0x3;

    }
}
