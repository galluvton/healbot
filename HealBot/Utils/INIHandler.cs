﻿using System;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;

namespace HealBot.Utils
{
    class INIHandler
    {
        private string Path;
        
        [DllImport("kernel32")]
        static extern long WritePrivateProfileString(string Section, string Key, string Value, string FilePath);

        [DllImport("kernel32")]
        static extern int GetPrivateProfileString(string Section, string Key, string Default, StringBuilder RetVal, int Size, string FilePath);

        public INIHandler(string IniPath)
        {
            Path = new FileInfo(IniPath).FullName.ToString();
        }

        public UInt32 ReadAddress(string Key, string Section, UInt32 Default)
        {
            var RetVal = new StringBuilder(255);
            GetPrivateProfileString(Section, Key, Default.ToString(), RetVal, 255, Path);
            string ans = RetVal.ToString();
            return UInt32.Parse(RetVal.ToString(), NumberStyles.HexNumber);
        }

        public string Read(string Key, string Section, string Default)
        {
            var RetVal = new StringBuilder(255);
            GetPrivateProfileString(Section, Key, Default, RetVal, 255, Path);
            return RetVal.ToString();
        }

        public void Write(string Key, string Value, string Section)
        {
            WritePrivateProfileString(Section, Key, Value, Path);
        }

        public void DeleteKey(string Key, string Section)
        {
            Write(Key, null, Section);
        }

        public void DeleteSection(string Section)
        {
            Write(null, null, Section);
        }

        public bool KeyExists(string Key, string Section)
        {
            return Read(Key, Section, "").Length > 0;
        }
    }
}
