﻿
namespace HealBot.Utils.KeyPresser
{
    public abstract class AKeyPresser
    {
        public abstract void sendKeystroke(int priority, string Key, string CtrlShift = Constants.INVALID_KEY);

        public void sendKeystrokeInternal(string Key, string CtrlShift = Constants.INVALID_KEY)
        {
            Util.sendKeystrokeIntern(Key, CtrlShift);
        }
    }
}
