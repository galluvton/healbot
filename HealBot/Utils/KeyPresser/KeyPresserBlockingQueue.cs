﻿using System;
using System.Threading;

namespace HealBot.Utils.KeyPresser
{

    public class KeyPresserBlockingQueue : AKeyPresser
    {
        private const int DELAY_BETWEEN_KEYS_MS = 50;
        private BlockingPriorityQueue<Pair> m_bq;

        public KeyPresserBlockingQueue()
        {
            m_bq = new BlockingPriorityQueue<Pair>();
            Thread t = new Thread(dequeue);
            t.Start();
        }

        public override void sendKeystroke(int priority, string Key, string CtrlShift = Constants.INVALID_KEY)
        {
            Pair p = new Pair();
            p.Key = Key;
            p.CtrlShift = CtrlShift;

            m_bq.Enqueue(priority, p);
        }

        private void dequeue()
        {
            Thread.CurrentThread.Priority = ThreadPriority.Highest;
            while (true)
            {
                Pair item = m_bq.Dequeue();
                sendKeystrokeInternal(item.Key, item.CtrlShift);
                System.Threading.Thread.Sleep(DELAY_BETWEEN_KEYS_MS);
            }
        }

        public class Pair
        {
            public string Key
            { get; set; }

            public string CtrlShift
            { get; set; }
        }

    }
}
