﻿﻿using System;

namespace HealBot.Utils.KeyPresser
{
    public class KeyPresserNormal : AKeyPresser
    {
        private static readonly Object m_keyLock = new Object();

        public override void sendKeystroke(int priority, string Key, string CtrlShift = Constants.INVALID_KEY)
        {
            lock (m_keyLock)
            {
                sendKeystrokeInternal(Key, CtrlShift);
            }
        }
    }
}
