﻿using System;
using System.Collections.Concurrent;
using System.Threading;
using System.Diagnostics;

namespace HealBot.Utils.KeyPresser
{
    public class KeyPresserSimpleQueue : AKeyPresser
    {
        private const int DELAY_BETWEEN_KEYS_MS = 50;
        private ConcurrentQueue<Pair> topQ;
        private ConcurrentQueue<Pair> midQ;
        private ConcurrentQueue<Pair> botQ;

        public KeyPresserSimpleQueue()
        {
            topQ = new ConcurrentQueue<Pair>();
            midQ = new ConcurrentQueue<Pair>();
            botQ = new ConcurrentQueue<Pair>();
            Thread t = new Thread(dequeue);
            t.Start();
        }

        public override void sendKeystroke(int priority, string Key, string CtrlShift = Constants.INVALID_KEY)
        {
            Pair p = new Pair();
            p.Key = Key;
            p.CtrlShift = CtrlShift;

            switch (priority)
            {
                case Util.PRIO_TOP:
                    topQ.Enqueue(p);
                    break;
                case Util.PRIO_MID:
                    midQ.Enqueue(p);
                    break;
                case Util.PRIO_BOT:
                    botQ.Enqueue(p);
                    break;
                default:
                    break;
            }
        }

        public void dequeue()
        {
            Thread.CurrentThread.Priority = ThreadPriority.Highest;
            while (true)
            {
                // debug
                long time = DateTime.Now.Ticks;

                Pair item;
                if (topQ.TryDequeue(out item))
                {

                }
                else if (midQ.TryDequeue(out item))
                {

                }
                else if (botQ.TryDequeue(out item))
                {

                }
                else
                {
                    continue;
                }
                sendKeystrokeInternal(item.Key, item.CtrlShift);
                System.Threading.Thread.Sleep(DELAY_BETWEEN_KEYS_MS);

                //debug
                time = DateTime.Now.Ticks - time;
                Console.WriteLine("keyqueue time " + (time / TimeSpan.TicksPerMillisecond).ToString());
            }
        }

        class Pair
        {
            public string Key
            {
                get;
                set;
            }

            public string CtrlShift
            {
                get;
                set;
            }
        }
    }
}
