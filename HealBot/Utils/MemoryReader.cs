﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.Diagnostics;

namespace HealBot.Utils
{
    class MemoryReader
    {
        [DllImport("kernel32.dll")]
        public static extern Int32 ReadProcessMemory(IntPtr hProcess, IntPtr lpBaseAddress,
            [In, Out] byte[] buffer, UInt32 size, out IntPtr lpNumberOfBytesRead);

        [DllImport("user32.dll", EntryPoint = "FindWindow")]
        public static extern IntPtr FindWindowEx(IntPtr parentHandle, IntPtr childAfter, string lclassName, string windowTitle);

        [DllImport("user32.dll", EntryPoint = "GetClientRect")]
        public static extern bool GetClientRect(HandleRef hwnd, out Objects.RECT lpRect);

        [DllImport("user32.dll", EntryPoint = "GetWindowRect")]
        public static extern bool GetWindowRect(HandleRef hwnd, out Objects.RECT lpRect);

        [DllImport("user32.dll", EntryPoint = "ClientToScreen")]
        public static extern bool ClientToScreen(HandleRef hwnd, out System.Drawing.Point lpRect);

        [DllImport("kernel32.dll")]
        public static extern Int32 WriteProcessMemory(IntPtr hProcess, IntPtr lpBaseAddress,
            [In, Out] byte[] buffer, UInt32 size, out IntPtr lpNumberOfBytesWritten);

        private static readonly FilterArgs _default = new FilterArgs();
        public static readonly FilterArgs IN_SCREEN = new FilterArgs(Constants.SCREEN_X_RADIUS,
                                                            Constants.SCREEN_Y_RADIUS, 0);


        public static byte[] ReadBytes(IntPtr Handle, Int64 Address, uint BytesToRead)
        {
            IntPtr ptrBytesRead;
            byte[] buffer = new byte[BytesToRead];
            ReadProcessMemory(Handle, new IntPtr(Address), buffer, BytesToRead, out ptrBytesRead);
            return buffer;
        }
        public static bool WriteBytes(IntPtr handle, Int64 address, byte[] bytes, uint length)
        {
            IntPtr bytesWritten;

            // Write to memory
            int result = WriteProcessMemory(handle, new IntPtr(address), bytes, length, out bytesWritten);

            return result != 0;
        }
        /*******************************************************************************************************/
        public static byte ReadByte(Int64 Address, IntPtr? Handle = null)
        {
            return ReadBytes(getIntPtr(Handle), Address, 1)[0];
        }
        public static bool ReadBool(Int64 Address, uint length, IntPtr? Handle = null)
        {
            return BitConverter.ToBoolean(ReadBytes(getIntPtr(Handle), Address, length), 0);
        }
        public static int ReadInt32(long Address, uint length = 4, IntPtr? Handle = null)
        {
            return BitConverter.ToInt32(ReadBytes(getIntPtr(Handle), Address, length), 0);
        }
        public static UInt32 ReadUInt32(long Address, uint length = 4, IntPtr? Handle = null)
        {
            return BitConverter.ToUInt32(ReadBytes(getIntPtr(Handle), Address, length), 0);
        }
        public static ushort ReadInt16(long Address, IntPtr? Handle = null)
        {
            return BitConverter.ToUInt16(ReadBytes(getIntPtr(Handle), Address, 2), 0);
        }
        public static double ReadDouble(long Address, IntPtr? Handle = null)
        {
            return BitConverter.ToDouble(ReadBytes(getIntPtr(Handle), Address, 8), 0);
        }
        public static string ReadString(long Address, uint length = 32, IntPtr? Handle = null)
        {
            string temp3 = ASCIIEncoding.Default.GetString(ReadBytes(getIntPtr(Handle), Address, length));
            string[] temp3str = temp3.Split('\0');
            return temp3str[0];
        }
        public static UInt32 ReadPtr(long Address, UInt32[] offsets, uint length = 32, IntPtr? Handle = null)
        {
            UInt32 Addr = (UInt32)ReadInt32(Address + Util.Base);
            UInt32 NewAddr = Addr;
            int i = 0;
            foreach (UInt32 offset in offsets)
            {
                if (i != offsets.Length - 1)
                {
                    NewAddr = (UInt32)ReadInt32(NewAddr + offset);
                }
                else
                {
                    return NewAddr + offset;
                }
                i++;
            }
            return NewAddr;
        }
        public static string ReadStringPtr(long Address, UInt32[] offsets, uint length = 32, IntPtr? Handle = null)
        {
            UInt32 Addr = (UInt32)ReadInt32(Address + Util.Base);
            UInt32 NewAddr = Addr;
            foreach (UInt32 offset in offsets)
            {
                NewAddr = (UInt32)ReadInt32(NewAddr + offset);
            }
            return ReadString(NewAddr, 255);
        }
        public static bool WriteInt32(long Address, int Value, IntPtr? Handle = null)
        {
            byte[] oldBytes = BitConverter.GetBytes(Value);
            return WriteBytes(getIntPtr(Handle), Address, oldBytes, 4);
        }

        public static IntPtr getIntPtr(IntPtr? Handle)
        {
            if (Handle == null)
            {
                return Util.Tibia.Handle;
            }
            else
            {
                return (IntPtr)Handle;
            }
        }

        // Getting client functions
        public static Process[] getClients()
        {
            Process[] procs = Process.GetProcessesByName("Tibia");
            List<Process> prcs = new List<Process>();
            foreach (Process proc in procs)
            {
                if (!(Util.VersionIgnore || TibiaVerNum.ClientVersion.Equals(Util.getFileVersion(proc.MainModule.FileName))))
                {
                    continue;
                }
                prcs.Add(proc);
            }
            return prcs.ToArray();
        }

        public static Process getFirstClient()
        {
            if (getClients().Length != 0)
            {
                return getClients()[0];
            }
            else
            {
                return null;
            }
        }

        public static Process getClientById(int id)
        {
            Process[] ProcList = getClients();
            foreach (Process p in ProcList)
            {
                if (p.Id == id)
                {
                    return p;
                }
            }
            return null;
        }

        // Reading self info
        public static int Level()
        {
            return ReadInt32(Addresses.Level + Util.Base);
        }

        public static int Hp()
        {
            return ReadInt32(Addresses.Hp + Util.Base) ^ ReadInt32(Addresses.Xor + Util.Base);
        }

        public static int MaxHp()
        {
            return ReadInt32(Addresses.MaxHp + Util.Base) ^ ReadInt32(Addresses.Xor + Util.Base);
        }

        public static int Mp()
        {
            return ReadInt32(Addresses.Mp + Util.Base) ^ ReadInt32(Addresses.Xor + Util.Base);
        }

        public static int MaxMp()
        {
            return ReadInt32(Addresses.MaxMp + Util.Base) ^ ReadInt32(Addresses.Xor + Util.Base);
        }

        public static double Cap()
        {
            int temp = ReadInt32(Addresses.Cap + Util.Base) ^ ReadInt32(Addresses.Xor + Util.Base);
            return (temp / 100.0);
        }

        public static int Soul()
        {
            return ReadInt32(Addresses.Soul + Util.Base);
        }

        public static int X()
        {
            return ReadInt32(Addresses.XPos + Util.Base);
        }

        public static int Y()
        {
            return ReadInt32(Addresses.YPos + Util.Base);
        }

        public static int Z()
        {
            return ReadInt32(Addresses.ZPos + Util.Base);
        }

        public static int Cid()
        {
            return ReadInt32(Addresses.Cid + Util.Base);
        }
        /*
        public int LastUsed()
        {
            return ReadInt32(Addresses.LastClicked + Util.Base);
        }
        */
        public static int ClientCid(UInt32 BaseAddress = 0x0, IntPtr? Handle = null)
        {
            if (BaseAddress == 0x0)
            {
                BaseAddress = Util.Base;
            }
            return ReadInt32(Addresses.Cid + BaseAddress, 4, getIntPtr(Handle));
        }

        public static int Exp()
        {
            return ReadInt32(Addresses.Exp + Util.Base);
        }

        public static int XpNextLevel()
        {
            int lvl = Level();
            int res = (50 * lvl * lvl * lvl - 150 * lvl * lvl + 400 * lvl) / 3;
            return res - Exp();
        }

        public static bool Connected()
        {
            return ReadBool(Addresses.IsConnected + Util.Base, 1);
        }

        // Reading array info
        public static List<Objects.BList> BlGet(bool returnall = true, FilterArgs filter = null)
        {
            UInt32 BaseAddress = Util.Base;

            if (filter == null)
            {
                filter = _default;
            }

            List<Objects.BList> bat = new List<Objects.BList>();

            for (int i = 0; i < BListAdresses.Max; i++)
            {
                UInt32 CreatureOffset = Convert.ToUInt32(i) * BListAdresses.Step;
                Objects.BList batt = new Objects.BList();
                batt.Addr = i;

                batt.Id = ReadInt32(BListAdresses.Start + BListAdresses.IdOffset + CreatureOffset + BaseAddress, 4, getIntPtr(null));
                if (batt.Id != 0)
                {
                    //Console.WriteLine(batt.Visible);&& batt.Visible != 0
                    UInt32 currentMem = BListAdresses.Start + CreatureOffset + BaseAddress;
                    batt.Type = ReadByte(BListAdresses.TypeOffset + currentMem, getIntPtr(null));
                    batt.Name = ReadString(BListAdresses.NameOffset + currentMem, 32, getIntPtr(null));
                    batt.Z = ReadInt32(BListAdresses.ZOffset + currentMem, 4, getIntPtr(null));
                    batt.Y = ReadInt32(BListAdresses.YOffset + currentMem, 4, getIntPtr(null));
                    batt.X = ReadInt32(BListAdresses.XOffset + currentMem, 4, getIntPtr(null));
                    batt.Hppc = ReadInt32(BListAdresses.HppcOffset + currentMem, 4, getIntPtr(null));
                    batt.Offset = CreatureOffset;
                    /*
                    batt.TimeLastMoved = ReadInt32(BListAdresses.TimeLastMovedOffset + currentMem, 4, getIntPtr(Handle));
                    batt.Walking = ReadInt32(BListAdresses.IsWalking + currentMem, 4, getIntPtr(Handle));
                    batt.Direction = ReadInt32(BListAdresses.DirectionOffset + currentMem, 4, getIntPtr(Handle));
                    batt.Previous = 0;
                    batt.Next = 0;
                    batt.Outfit = ReadInt32(BListAdresses.LookType + currentMem, 4, getIntPtr(Handle));
                    batt.MountId = ReadInt32(BListAdresses.LookMount + currentMem, 4, getIntPtr(Handle));
                    batt.BlackSquare = ReadInt32(BListAdresses.BlackSquareOffset + currentMem, 4, getIntPtr(Handle));
                    batt.Speed = ReadInt32(BListAdresses.SpeedOffset + currentMem, 4, getIntPtr(Handle));
                    batt.SkullType = ReadInt32(BListAdresses.SkullOffset + currentMem, 4, getIntPtr(Handle));
                    batt.Party = ReadInt32(BListAdresses.PartyOffset + currentMem, 4, getIntPtr(Handle));
                    batt.WarIcon = ReadInt32(BListAdresses.GuildWarFlag + currentMem, 4, getIntPtr(Handle));
                    */

                    if (filter.filterCrit(batt))
                    {
                        bat.Add(batt);
                    }
                }
            }

            return bat;
        }
        public static List<Objects.BList> BlGetPlayersOnScreen(string name = null)
        {
            UInt32 BaseAddress = Util.Base;
            List<Objects.BList> bat = new List<Objects.BList>();
            if (name != null)
            {
                name = name.ToLower(); 
            }

            for (int i = 0; i < BListAdresses.Max; i++)
            {
                UInt32 CreatureOffset = Convert.ToUInt32(i) * BListAdresses.Step;
                Objects.BList batt = new Objects.BList();
                batt.Addr = i;

                batt.Id = ReadInt32(BListAdresses.Start + BListAdresses.IdOffset + CreatureOffset + BaseAddress, 4, getIntPtr(null));
                batt.Type = ReadByte(BListAdresses.Start + BListAdresses.TypeOffset + CreatureOffset + BaseAddress,getIntPtr(null));
                batt.Visible = ReadInt32(BListAdresses.Start + BListAdresses.IsVisible + CreatureOffset + BaseAddress, 4, getIntPtr(null));
                if (batt.Id != 0 && batt.Type <= Constants.CREATURE_TYPE_PLAYER && batt.Visible != 0)
                {
                    UInt32 currentMem = BListAdresses.Start + CreatureOffset + BaseAddress;
                    batt.Name = ReadString(BListAdresses.NameOffset + currentMem, 32, getIntPtr(null));
                    batt.Hppc = ReadInt32(BListAdresses.HppcOffset + currentMem, 4, getIntPtr(null));

                    batt.Z = ReadInt32(BListAdresses.ZOffset + currentMem, 4, getIntPtr(null));
                    if (name == null)
                    {
                        batt.Y = ReadInt32(BListAdresses.YOffset + currentMem, 4, getIntPtr(null));
                        batt.X = ReadInt32(BListAdresses.XOffset + currentMem, 4, getIntPtr(null));
                        batt.Offset = CreatureOffset;
                        bat.Add(batt);
                    }
                    else if (name.Equals(batt.Name.ToLower()) && MemoryReader.Z() == batt.Z)
                    {
                        bat.Add(batt);
                        return bat;
                    }
                }
            }

            return bat;
        }

        public static int ReadCooldownCategoryStart(int depth, UInt32 startEndOffset)
        {
            UInt32 num = (UInt32)ReadInt32(Cooldowns.CooldownCategoryStart + Util.Base);
            UInt32 temp = num;
            for (int i = 0; i < depth; i++)
            {
                temp = (UInt32)ReadInt32(temp);
            }
            return ReadInt32(temp + startEndOffset);
        }

        public static IEnumerable<Objects.ChatStruct> GetChatTabs()
        {
            UInt32 tabNodeAddress = MemoryReader.ReadUInt32(Util.Base + Addresses.ChatLogStart);

            foreach (UInt32 magicNum in LogAddresses.LogTabsMagics)
            {
                tabNodeAddress = MemoryReader.ReadUInt32(tabNodeAddress + magicNum);
            }

            while (tabNodeAddress != 0x0)
            {
                //use 0x30 for longer name (possibly upto 30 bytes)
                //0x2C will use '...' for names longer than 15 chars
                Objects.ChatStruct chat;
                UInt32 tabNamePointer = MemoryReader.ReadUInt32(tabNodeAddress + LogAddresses.LogTabsToLogNamePointer);
                chat.Name = MemoryReader.ReadString(tabNamePointer, LogAddresses.LogNameSize);
                chat.DataPointer = MemoryReader.ReadUInt32(tabNodeAddress + LogAddresses.LogTabsToLogContent);
                yield return chat;
                
                //next tab node pointer is current tab node address + 0x10
                tabNodeAddress = MemoryReader.ReadUInt32(tabNodeAddress + LogAddresses.LogTabNext);
            }
            yield break;
        }

        public static int AttackCooldownStart()
        {
            return ReadCooldownCategoryStart(Cooldowns.AttackCooldownPointerDepth, Cooldowns.CooldownStartOffsetForEachCategory);
        }

        public static int AttackCooldownEnd()
        {
            return ReadCooldownCategoryStart(Cooldowns.AttackCooldownPointerDepth, Cooldowns.CooldownEndOffsetForEachCategory);
        }

        public static int HealCooldownStart()
        {
            return ReadCooldownCategoryStart(Cooldowns.HealCooldownPointerDepth, Cooldowns.CooldownStartOffsetForEachCategory);
        }

        public static int HealCooldownEnd()
        {
            return ReadCooldownCategoryStart(Cooldowns.HealCooldownPointerDepth, Cooldowns.CooldownEndOffsetForEachCategory);
        }

        public static int SupportCooldownStart()
        {
            return ReadCooldownCategoryStart(Cooldowns.SupportCooldownPointerDepth, Cooldowns.CooldownStartOffsetForEachCategory);
        }

        public static int SupportCooldownEnd()
        {
            return ReadCooldownCategoryStart(Cooldowns.SupportCooldownPointerDepth, Cooldowns.CooldownEndOffsetForEachCategory);
        }

        public static int GetArrowSlotId()
        {
            return ReadInt32(Equipment.ArrowSlotStart + Equipment.ArrowSlotItemId + Util.Base);
        }

        public static int GetArrowSlotCount()
        {
            return ReadInt32(Equipment.ArrowSlotStart + Equipment.ArrowSlotAmount + Util.Base);
        }

        public static int GetRingSlotId()
        {
            return ReadInt32(Equipment.RingSlotStart + Equipment.RingSlotItemId + Util.Base);
        }

        public static int GetAmuletSlotId()
        {
            return ReadInt32(Equipment.AmuletSlotStart + Equipment.AmuletSlotItemId + Util.Base);
        }

        public static bool IsStatusActive(UInt32 statusOffset)
        {
            int ans = ReadInt32(PlayerFlags.FlagAddress + Util.Base);
            return ((ans & statusOffset) != 0);
        }

        public class FilterArgs
        {

            private UInt32 m_x;
            private UInt32 m_y;
            private UInt32 m_z;
            private bool m_set;

            public FilterArgs(UInt32 x, UInt32 y, UInt32 z)
            {
                m_x = x;
                m_y = y;
                m_z = z;
                m_set = true;
            }

            public FilterArgs()
            {
                m_set = false;
            }

            public bool filterCrit(Objects.BList crit)
            {
                bool ans = (crit.Id != 0);

                if (m_set)
                {
                    int myX = MemoryReader.X();
                    int myY = MemoryReader.Y();
                    int myZ = MemoryReader.Z();
                    int xRelative = crit.X - myX;
                    int yRelative = crit.Y - myY;
                    int zRelative = myZ - crit.Z;

                    ans &= (Math.Abs(xRelative) <= m_x);
                    ans &= (Math.Abs(yRelative) <= m_y);
                    ans &= (Math.Abs(zRelative) <= m_z);
                }

                return ans;
            }

            public override String ToString() {
                if (m_set) {
                    String ans = "x:" + m_x.ToString();
                    ans += ";y:" + m_y.ToString();
                    ans += ";z:" + m_z.ToString();
                    return ans;
                }
                else
                {
                    return "not set";
                }
            }

        }
    }
}
