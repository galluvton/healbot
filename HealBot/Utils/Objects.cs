﻿﻿using System;
﻿using System.Diagnostics;
using System.Collections.Generic;

namespace HealBot.Utils
{
    public class Objects
    {
        public struct RECT
        {
            public int Left;        // x position of upper-left corner
            public int Top;         // y position of upper-left corner
            public int Right;       // x position of lower-right corner
            public int Bottom;      // y position of lower-right corner
        }

        public struct Error
        {
            public string Message;
            public int type; // Type can be 1-5, depending on the urgency of errors selected.
        }

        public struct MapTileItem // total size 32 bytes
        {
            public int count; // The count of the items
            public int id;
            public int[] padding; // 24 byte padding
        }

        public struct Item
        {
            public int Id;
            public int Unknown2; // This is the hangable state and returns something like 18 or 19 if the item is hanging vertical or horiz
            public int StackCount;
        }

        public class BList
        {
            public int Addr; // The current creatures offset from the start of the battle list as a number to be multiplied
            public uint Offset;

            public int Id; // Creature ID
            public int Type; // Type - 64 = creature, 2 = player?
            public string Name; // Name, terminated by \0
            public int Z; // Z position
            public int Y; // Y position
            public int X; // X position
            public int TimeLastMoved; // The time in milliseconds since this creature last moved
            public int Walking; // Is it walking? 
            public int Direction; // Which direction is it facing 
            public int Previous; // What the fuck is this for? I've gotta stop reading shit I don't know about
            public int Next; // Again, what the fuck?!
            public int Outfit; // Outfit ID, can be enumerated later on
            public int MountId; // Mount ID, again can be enumerated though probably about as much use as a condom in a nunnery.

            public int BlackSquare; // Is this even still valid? I swear to god they removed this shit.
            public int Hppc; // Health, as a percentage... That's out of 100!!!!
            public int Speed; // The creatures speed as determined by CIPs dodgy calculations

            public int SkullType; // The type of skull the player has (0 for none), can be enum'd
            public int Party; // Party type (leader, invited, member)
            public int WarIcon; // War icon (friendly, enemy, not part of your war so stay the fuck out of shit you ain't involved in).
            public int Visible; // On screen

            // 20 entries total.
        }

        public struct ChatStruct // A struct for chat tabs
        {
            public UInt32 DataPointer;
            public string Name;
        }

    }
}
