﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using HealBot.TableItems;

namespace HealBot.Utils.Parser
{
    public class PropertyFileParser
    {
        private static readonly string COMMENT = "###";
        public static readonly string HOTKEY_NOTATION = "#HK#";
        public static readonly string DRUIDHEAL_NOTATION = "#DH#";
        public static readonly string MANASHIELD_NOTATION = "#MH#";
        public static readonly string ARROW_NOTATION = "#AH#";
        public static readonly string RING_NOTATION = "#RH#";

        public static void SaveProperties(DataGridViewRowCollection KHPresetsRows, ComboBox druidHeal,
                                          ComboBox cb_manashieldHK, ComboBox cb_arrowHK, ComboBox cb_ringHK)
        {
            string outFile = Util.SaveFileDialog();
            if ("".Equals(outFile))
            {
                return;
            }

            StringBuilder outData = new StringBuilder();

            outData.Append(COMMENT).Append("Hotkeys\n");

            foreach (DataGridViewRow row in KHPresetsRows)
            {
                DataGridViewComboBoxCell col1 = (DataGridViewComboBoxCell)row.Cells["Prefix"];
                DataGridViewTextBoxCell col2 = (DataGridViewTextBoxCell)row.Cells["Amount"];
                DataGridViewComboBoxCell col3 = (DataGridViewComboBoxCell)row.Cells["Hotkey"];

                try
                {
                    string dat = HotkeyEntryParser.ParseToFile((HpMpCondItem)col1.Value, (string)col2.Value, (Hotkey)col3.Value);
                    outData.Append(HOTKEY_NOTATION).Append(dat).Append("\n");
                }
                catch (Exception)
                {
                    // do nothing, just ignore that line
                }
            }

            try
            {
                addHotkey(outData, druidHeal, DRUIDHEAL_NOTATION);
                addHotkey(outData, cb_manashieldHK, MANASHIELD_NOTATION);
                addHotkey(outData, cb_arrowHK, ARROW_NOTATION);
                addHotkey(outData, cb_ringHK, RING_NOTATION);
            }
            catch (Exception)
            {
                // do nothing, just ignore that line
            }

            System.IO.File.WriteAllText(outFile, outData.ToString());
        }

        public static Dictionary<string, IList<CompressedHotkeyTableEntry>> ReadProperties(string file = null)
        {
            string inFile;
            if (file == null)
            {
                inFile = Util.OpenFileDialog();
            }
            else
            {
                inFile = file;
            }

            string data;
            try
            {
                data = System.IO.File.ReadAllText(inFile);
            }
            catch (Exception)
            {
                return null;
            }
            
            string[] lines = data.Split('\n');
            Dictionary<string, IList<CompressedHotkeyTableEntry>> ans = new Dictionary<string, IList<CompressedHotkeyTableEntry>>();
            ans[HOTKEY_NOTATION] = new List<CompressedHotkeyTableEntry>();
            ans[DRUIDHEAL_NOTATION] = new List<CompressedHotkeyTableEntry>();
            ans[MANASHIELD_NOTATION] = new List<CompressedHotkeyTableEntry>();
            ans[ARROW_NOTATION] = new List<CompressedHotkeyTableEntry>();
            ans[RING_NOTATION] = new List<CompressedHotkeyTableEntry>();

            foreach (string line in lines)
            {
                if (line.StartsWith(HOTKEY_NOTATION))
                {
                    parseLine(line, HOTKEY_NOTATION, ans[HOTKEY_NOTATION]);
                }
                else if (line.StartsWith(DRUIDHEAL_NOTATION))
                {
                    parseLine(line, DRUIDHEAL_NOTATION, ans[DRUIDHEAL_NOTATION]);
                }
                else if (line.StartsWith(MANASHIELD_NOTATION))
                {
                    parseLine(line, MANASHIELD_NOTATION, ans[MANASHIELD_NOTATION]);
                }
                else if (line.StartsWith(ARROW_NOTATION))
                {
                    parseLine(line, ARROW_NOTATION, ans[ARROW_NOTATION]);
                }
                else if (line.StartsWith(RING_NOTATION))
                {
                    parseLine(line, RING_NOTATION, ans[RING_NOTATION]);
                }
            }

            return ans;
        }

        private static void parseLine(string str, string notation, IList<CompressedHotkeyTableEntry> lst)
        {
            try
            {
                string temp = str.Substring(notation.Length);
                CompressedHotkeyTableEntry HK = HotkeyEntryParser.ParseFromFile(temp);
                lst.Add(HK);
            }
            catch (Exception)
            {
                // do nothing, just ignore that line
            }
        }

        private static void addHotkey(StringBuilder outData, ComboBox cb, string tag)
        {
            outData.Append(COMMENT).Append("\n");
            outData.Append(COMMENT).Append("Druid healing\n");
            Hotkey tmp = new Hotkey(cb.Text, (KeyCombo)(cb.SelectedValue));
            string dat = HotkeyEntryParser.ParseToFile(null, null, tmp);
            outData.Append(tag).Append(dat).Append("\n");
        }

    }
}
