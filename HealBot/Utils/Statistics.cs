﻿using System;

namespace HealBot.Utils
{
    public class Statistics
    {
        private static Statistics m_instance = null;
        private static Object oLock = new object();
        private readonly Object m_lock1;
        private int m_HotkeyCur;
        private int m_HotkeyMax;
        private readonly Object m_lock2;
        private int m_BattleListCur;
        private int m_BattleListMax;
        private readonly Object m_lock3;
        private int m_ManasitCur;
        private int m_ManasitMax;
        private readonly Object m_lock4;
        private int m_DruidHealCur;
        private int m_DruidHealMax;
        private readonly Object m_lock5;
        private int m_ManashieldCur;
        private int m_ManashieldMax;
        private readonly Object m_lock6;
        private int m_MainCur;
        private int m_MainMax;
        private readonly Object m_lock7;
        private int m_KeystrokeCur;
        private int m_KeystrokeMax;
        private readonly Object m_lock8;
        private int m_LogCur;
        private int m_LogMax;

        private Statistics()
        {
            m_HotkeyCur = 0;
            m_HotkeyMax = 0;
            m_BattleListCur = 0;
            m_BattleListMax = 0;
            m_ManasitCur = 0;
            m_ManasitMax = 0;
            m_DruidHealCur = 0;
            m_DruidHealMax = 0;
            m_ManashieldCur = 0;
            m_ManashieldMax = 0;
            m_MainCur = 0;
            m_MainMax = 0;
            m_KeystrokeCur = 0;
            m_KeystrokeMax = 0;
            m_LogCur = 0;
            m_LogMax = 0;
            
            m_lock1 = new Object();
            m_lock2 = new Object();
            m_lock3 = new Object();
            m_lock4 = new Object();
            m_lock5 = new Object();
            m_lock6 = new Object();
            m_lock7 = new Object();
            m_lock8 = new Object();
        }

        public static Statistics GetInstance()
        {
            if (m_instance == null)
            {
                lock (oLock)
                {
                    if (m_instance == null)
                    {
                        m_instance = new Statistics();
                    }
                }
            }
            return m_instance;
        }

        public string Hotkey
        {
            get {
                lock (m_lock1) {
                    return m_HotkeyMax == 0 ? "NA" : m_HotkeyCur.ToString() + "(" + m_HotkeyMax.ToString() + ") ms";
                }
            }
            set {
                lock (m_lock1) {
                    int i_val;
                    if (Int32.TryParse(value, out i_val))
                    {
                        m_HotkeyMax = (i_val > m_HotkeyMax) ? i_val : m_HotkeyMax;
                        m_HotkeyCur = i_val;
                    }
                }
            }
        }

        public string BattleList
        {
            get {
                lock (m_lock2) {
                    return m_BattleListMax == 0 ? "NA" : m_BattleListCur.ToString() + "(" + m_BattleListMax.ToString() + ") ms";
                }
            }
            set {
                lock (m_lock2) {
                    int i_val;
                    if (Int32.TryParse(value, out i_val))
                    {
                        m_BattleListMax = (i_val > m_BattleListMax) ? i_val : m_BattleListMax;
                        m_BattleListCur = i_val;
                    }
                }
            }
        }

        public string Manasit
        {
            get {
                lock (m_lock3) {
                    return m_ManasitMax == 0 ? "NA" : m_ManasitCur.ToString() + "(" + m_ManasitMax.ToString() + ") ms";
                }
            }
            set {
                lock (m_lock3) {
                    int i_val;
                    if (Int32.TryParse(value, out i_val))
                    {
                        m_ManasitMax = (i_val > m_ManasitMax) ? i_val : m_ManasitMax;
                        m_ManasitCur = i_val;
                    }
                }
            }
        }

        public string DruidHeal
        {
            get {
                lock (m_lock4) {
                    return m_DruidHealMax == 0 ? "NA" : m_DruidHealCur.ToString() + "(" + m_DruidHealMax.ToString() + ") ms";
                }
            }
            set {
                lock (m_lock4) {
                    int i_val;
                    if (Int32.TryParse(value, out i_val))
                    {
                        m_DruidHealMax = (i_val > m_DruidHealMax) ? i_val : m_DruidHealMax;
                        m_DruidHealCur = i_val;
                    }
                }
            }
        }

        public string Manashield
        {
            get
            {
                lock (m_lock5)
                {
                    return m_ManashieldMax == 0 ? "NA" : m_ManashieldCur.ToString() + "(" + m_ManashieldMax.ToString() + ") ms";
                }
            }
            set
            {
                lock (m_lock5)
                {
                    int i_val;
                    if (Int32.TryParse(value, out i_val))
                    {
                        m_ManashieldMax = (i_val > m_ManashieldMax) ? i_val : m_ManashieldMax;
                        m_ManashieldCur = i_val;
                    }
                }
            }
        }

        public string Main
        {
            get {
                lock (m_lock6) {
                    return m_MainMax == 0 ? "NA" : m_MainCur.ToString() + "(" + m_MainMax.ToString() + ") ms";
                }
            }
            set {
                lock (m_lock6) {
                    int i_val;
                    if (Int32.TryParse(value, out i_val))
                    {
                        m_MainMax = (i_val > m_MainMax) ? i_val : m_MainMax;
                        m_MainCur = i_val;
                    }
                }
            }
        }

        public string Keystroke
        {
            get
            {
                lock (m_lock7)
                {
                    return m_KeystrokeMax == 0 ? "NA" : m_KeystrokeCur.ToString() + "(" + m_KeystrokeMax.ToString() + ") ms";
                }
            }
            set
            {
                lock (m_lock7)
                {
                    int i_val;
                    if (Int32.TryParse(value, out i_val))
                    {
                        m_KeystrokeMax = (i_val > m_KeystrokeMax) ? i_val : m_KeystrokeMax;
                        m_KeystrokeCur = i_val;
                    }
                }
            }
        }

        public string Log
        {
            get
            {
                lock (m_lock8)
                {
                    return m_LogMax == 0 ? "NA" : m_LogCur.ToString() + "(" + m_LogMax.ToString() + ") ms";
                }
            }
            set
            {
                lock (m_lock8)
                {
                    int i_val;
                    if (Int32.TryParse(value, out i_val))
                    {
                        m_LogMax = (i_val > m_LogMax) ? i_val : m_LogMax;
                        m_LogCur = i_val;
                    }
                }
            }
        }

        public string GetMaxTime()
        {
            int[] data = new int[7] { m_HotkeyCur, m_BattleListCur,
                                       m_ManasitCur, m_DruidHealCur,
                                       m_ManashieldCur, m_MainCur, m_LogCur };
            int[] maxdata = new int[7] { m_HotkeyMax, m_BattleListMax,
                                       m_ManasitMax, m_DruidHealMax,
                                       m_ManashieldMax, m_MainMax, m_LogMax };
            
            int max1 = 0;
            int max2 = 0;
            foreach (int num in data)
            {
                max1 = Math.Max(max1, num);
            }
            foreach (int num in maxdata)
            {
                max2 = Math.Max(max2, num);
            }
            
            return max1.ToString() + "(" + max2.ToString() + ")";
        }

    }
}
