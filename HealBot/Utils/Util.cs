﻿﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using HealBot.Utils.KeyPresser;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleToAttribute("AKeyPresser")]

namespace HealBot.Utils
{
    public static class Util
    {
        public const int PRIO_TOP = BlockingPriorityQueue<int>.PRIO_TOP;
        public const int PRIO_MID = BlockingPriorityQueue<int>.PRIO_MID;
        public const int PRIO_BOT = BlockingPriorityQueue<int>.PRIO_BOT;
        private static AKeyPresser keyPresser = new KeyPresserNormal();

        public static void setKeyPressQueue(bool p)
        {
            if (p)
            {
                keyPresser = new KeyPresserBlockingQueue();
            }
            else
            {
                keyPresser = new KeyPresserNormal();
            }
        }

        public static bool VersionIgnore
        {
            get;
            set;
        }

        [DllImport("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, Int32 Msg, IntPtr wParam, IntPtr lParam);
        
        [DllImport("user32.dll")]
        public static extern short GetKeyState(int nVirtKey);

        public static List<Objects.Error> Errors = new List<Objects.Error>();
        public static Objects.Error lastError;
        public static Process _Tibia = null;
        public static UInt32 _BaseAddress;
        public static IntPtr _Handle;
        public static bool _OverrideCtrlShift = false;

        public static Process Tibia
        {
            get 
            {
                return _Tibia; 
            }
            set
            {
                if (value != null)
                {
                    _Tibia = value;
                    _BaseAddress = Convert.ToUInt32(value.MainModule.BaseAddress.ToInt32());
                    _Handle = value.MainWindowHandle;
                }
            }
        }

        public static UInt32 Base
        {
            get { return (_Tibia == null)? 0 : _BaseAddress; }
            set { _BaseAddress = value; }
        }
        public static IntPtr Handle
        {
            get { return (_Tibia == null) ? IntPtr.Zero : _Handle; }
            set { _Handle = value; }
        }

        public static bool Update()
        {
            Process tmpProc = MemoryReader.getFirstClient();
            if (tmpProc == null)
            {
                return false;
            }
            if (tmpProc != Tibia)
            {
                Tibia = tmpProc;
            }
            return true;
        }

        public static bool OverrideCtrlShift
        {
            get { return _OverrideCtrlShift; }
            set { _OverrideCtrlShift = value; }
        }

        public static UInt32 CurrentTime
        {
            get { return (UInt32)((DateTime.Now.Ticks - Tibia.StartTime.Ticks) / TimeSpan.TicksPerMillisecond); }
        }

        public static string getFileVersion(string filePath)
        {
            return FileVersionInfo.GetVersionInfo(Environment.ExpandEnvironmentVariables(filePath)).FileVersion;
        }

        // Get key codes ready for sending
        public static UInt32 getKeyCode(string Key)
        {
            Constants.KeyboardControls keynum;
            if (Enum.TryParse<Constants.KeyboardControls>(Key, true, out keynum))
            {
                return (UInt32)(keynum);
            }
            else
            {
                return new UInt32();
            }
        }

        public static void sendKeystroke(int priority, string Key, string CtrlShift = Constants.INVALID_KEY)
        {
            long time = DateTime.Now.Ticks;

            keyPresser.sendKeystroke(priority, Key, CtrlShift);
                
            time = DateTime.Now.Ticks - time;
            Statistics.GetInstance().Keystroke = (time / TimeSpan.TicksPerMillisecond).ToString();
        }

        internal static void sendKeystrokeIntern(string Key, string CtrlShift = Constants.INVALID_KEY)
        {
            IntPtr wParam = new IntPtr(getKeyCode(Key));
            bool isCtrl = false;
            bool isShift = false;

            if (_OverrideCtrlShift)
            {
                isCtrl = Util.isKeyPressed(Constants.KeyboardControls.ctrl.ToString());
                isShift = Util.isKeyPressed(Constants.KeyboardControls.shift.ToString());

                handleCtrlShift(true, isShift, isCtrl);
            }

            if (CtrlShift != Constants.INVALID_KEY)
            {
                SendMessage(Util.Handle, Constants.WM_SYSKEYDOWN, (IntPtr)getKeyCode(CtrlShift), new IntPtr(0));
            }
            SendMessage(Util.Handle, Constants.WM_SYSKEYDOWN, (IntPtr)getKeyCode(Key), new IntPtr(0));
            SendMessage(Util.Handle, Constants.WM_SYSKEYUP, (IntPtr)getKeyCode(Key), new IntPtr(0));
            if (CtrlShift != Constants.INVALID_KEY)
            {
                SendMessage(Util.Handle, Constants.WM_SYSKEYUP, (IntPtr)getKeyCode(CtrlShift), new IntPtr(0));
            }

            if (_OverrideCtrlShift)
            {
                handleCtrlShift(false, isShift, isCtrl);
            }
        }

        private static bool isKeyPressed(string Key)
        {
            return ((GetKeyState((Int32)getKeyCode(Key)) & 0x8000) != 0);
        }

        private static void handleCtrlShift(bool keyup, bool shift, bool ctrl)
        {
            int action = (keyup ? Constants.WM_SYSKEYUP : Constants.WM_SYSKEYDOWN);
            if (ctrl)
            {
                SendMessage(Util.Handle, action, (IntPtr)getKeyCode(Constants.KeyboardControls.ctrl.ToString()), new IntPtr(0));
            }
            if (shift)
            {
                SendMessage(Util.Handle, action, (IntPtr)getKeyCode(Constants.KeyboardControls.shift.ToString()), new IntPtr(0));
            }
        }

        public static void debugLog(string text, string filename = "botCrashReport.txt")
        {
            string filepath = System.IO.Path.GetDirectoryName(Application.ExecutablePath);
            string fullpath = filepath + @"\" + filename;
            fullpath = fullpath.Replace(@"\\", @"\");
            string fullMsg = "Log entry, time: " + System.DateTime.Now.ToString() + "\n" + text + "\n\n\n";
            System.IO.File.AppendAllText(fullpath, fullMsg);
        }

        public static void lootLog(string text, string timestamp)
        {
            string filepath = System.IO.Path.GetDirectoryName(Application.ExecutablePath);
            string fullpath = filepath + @"\" + "botServerLog" + timestamp + ".txt";
            fullpath = fullpath.Replace(@"\\", @"\");
            string fullMsg = text + "\n";
            System.IO.File.AppendAllText(fullpath, fullMsg);
        }

        public static string SaveFileDialog()
        {
            SaveFileDialog sfd = new SaveFileDialog();

            sfd.Filter = "Healbot files (*.hbf)|*.hbf|All files (*.*)|*.*";
            sfd.FilterIndex = 1;
            sfd.RestoreDirectory = true;

            if (sfd.ShowDialog() == DialogResult.OK)
            {
                return sfd.FileName;
            }

            return "";
        }

        public static string OpenFileDialog()
        {
            OpenFileDialog ofd = new OpenFileDialog();

            ofd.Filter = "Healbot files (*.hbf)|*.hbf|All Files (*.*)|*.*";
            ofd.FilterIndex = 1;
            ofd.Multiselect = false;

            if (ofd.ShowDialog() == DialogResult.OK)
            {
                return ofd.FileName;
            }

            return "";
        }
        
        // neat tools for string or array searching
        /* 
        public static bool Contains(this string s, string text, StringComparison stringComparison)
        {
            return s.IndexOf(text, stringComparison) > -1;
        }

        public static bool isDigit(this char c)
        {
            return c >= '0' && c <= '9';
        }

        public static bool Contains(this byte[] array, int start, int length, string text)
        {
            for (int i = start; i < start + length; i++)
            {
                for (int j = 0; j < text.Length; j++)
                {
                    if (text[j] != array[i + j])
                    {
                        break;
                    }
                    if (j == text.Length - 1)
                    {
                        return true;
                    }
                }
            }

            return false;
        }
        */

        public static void updateAddresses(string filename)
        {
            if (string.Empty.Equals(filename) || !System.IO.File.Exists(filename))
            {
                return;
            }
            INIHandler ini = new INIHandler(filename);
            const string VERSION_SECTION = "Version";
            const string ADDRESS_SECTION = "Addresses";
            const string EQUIP_SECTION = "Equipment";
            const string BATTLE_LIST_SECTION = "BattleList";
            const string FLAGS_SECTION = "PlayerFlags";
            try
            {
                TibiaVerNum.ClientVersion = ini.Read("Version", VERSION_SECTION, TibiaVerNum.ClientVersion);
                Addresses.Cid = ini.ReadAddress("Cid", ADDRESS_SECTION, Addresses.Cid);
                Addresses.Xor = ini.ReadAddress("Xor", ADDRESS_SECTION, Addresses.Xor);
                Addresses.Hp = ini.ReadAddress("Hp", ADDRESS_SECTION, Addresses.Hp);
                Addresses.MaxHp = ini.ReadAddress("MaxHp", ADDRESS_SECTION, Addresses.MaxHp);
                Addresses.Mp = ini.ReadAddress("Mp", ADDRESS_SECTION, Addresses.Mp);
                Addresses.MaxMp = ini.ReadAddress("MaxMp", ADDRESS_SECTION, Addresses.MaxMp);
                Addresses.Cap = ini.ReadAddress("Cap", ADDRESS_SECTION, Addresses.Cap);
                Addresses.Soul = ini.ReadAddress("Soul", ADDRESS_SECTION, Addresses.Soul);
                Addresses.XPos = ini.ReadAddress("XPos", ADDRESS_SECTION, Addresses.XPos);
                Addresses.YPos = ini.ReadAddress("YPos", ADDRESS_SECTION, Addresses.YPos);
                Addresses.ZPos = ini.ReadAddress("ZPos", ADDRESS_SECTION, Addresses.ZPos);
                Addresses.Exp = ini.ReadAddress("Exp", ADDRESS_SECTION, Addresses.Exp);
                Addresses.IsConnected = ini.ReadAddress("IsConnected", ADDRESS_SECTION, Addresses.IsConnected);
                Addresses.Level = ini.ReadAddress("Level", ADDRESS_SECTION, Addresses.Level);
                Addresses.ChatLogStart = ini.ReadAddress("ChatLogStart", ADDRESS_SECTION, Addresses.ChatLogStart);
                PlayerFlags.FlagAddress = ini.ReadAddress("FlagAddress", FLAGS_SECTION, PlayerFlags.FlagAddress);
                Equipment.ArrowSlotStart = ini.ReadAddress("ArrowSlotStart", EQUIP_SECTION, Equipment.ArrowSlotStart);
                Equipment.RingSlotStart = ini.ReadAddress("RingSlotStart", EQUIP_SECTION, Equipment.RingSlotStart);
                Equipment.AmuletSlotStart = ini.ReadAddress("AmuletSlotStart", EQUIP_SECTION, Equipment.AmuletSlotStart);
                BListAdresses.Start = ini.ReadAddress("BListAdresses", BATTLE_LIST_SECTION, BListAdresses.Start);
            }
            catch (Exception)
            {
                // ignore reading exceptions
                return;
            }
        }
    }
}
