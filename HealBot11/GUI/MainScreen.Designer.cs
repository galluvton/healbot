﻿namespace HealBot11.GUI
{
    partial class MainScreen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainScreen));
            this.m_dataTable = new System.Windows.Forms.DataGridView();
            this.Data = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Value = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HotkeysPresets = new System.Windows.Forms.DataGridView();
            this.m_stat = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.nup_druidHPTarget = new System.Windows.Forms.NumericUpDown();
            this.pb_druidHP = new System.Windows.Forms.ProgressBar();
            this.tb_druidName = new System.Windows.Forms.TextBox();
            this.druid_active = new System.Windows.Forms.Label();
            this.cb_druidHK = new System.Windows.Forms.ComboBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.l_druidHP = new System.Windows.Forms.Label();
            this.cb_druidActivate = new System.Windows.Forms.CheckBox();
            this.l_refRate = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.m_dataTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HotkeysPresets)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nup_druidHPTarget)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // m_dataTable
            // 
            this.m_dataTable.AllowUserToAddRows = false;
            this.m_dataTable.AllowUserToDeleteRows = false;
            this.m_dataTable.AllowUserToResizeColumns = false;
            this.m_dataTable.AllowUserToResizeRows = false;
            this.m_dataTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.m_dataTable.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Data,
            this.Value});
            this.m_dataTable.Location = new System.Drawing.Point(129, 12);
            this.m_dataTable.Name = "m_dataTable";
            this.m_dataTable.ReadOnly = true;
            this.m_dataTable.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToDisplayedHeaders;
            this.m_dataTable.Size = new System.Drawing.Size(244, 221);
            this.m_dataTable.TabIndex = 0;
            // 
            // Data
            // 
            this.Data.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Data.HeaderText = "Data";
            this.Data.Name = "Data";
            this.Data.ReadOnly = true;
            this.Data.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Data.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Value
            // 
            this.Value.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Value.HeaderText = "Value";
            this.Value.Name = "Value";
            this.Value.ReadOnly = true;
            this.Value.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Value.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // HotkeysPresets
            // 
            this.HotkeysPresets.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HotkeysPresets.Location = new System.Drawing.Point(395, 12);
            this.HotkeysPresets.Name = "HotkeysPresets";
            this.HotkeysPresets.Size = new System.Drawing.Size(496, 382);
            this.HotkeysPresets.TabIndex = 0;
            this.HotkeysPresets.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.HotkeysPresets_CellContentClick);
            this.HotkeysPresets.CellLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.HotkeysPresets_CellChanged);
            // 
            // m_stat
            // 
            this.m_stat.AutoSize = true;
            this.m_stat.Location = new System.Drawing.Point(5, 34);
            this.m_stat.Name = "m_stat";
            this.m_stat.Size = new System.Drawing.Size(24, 13);
            this.m_stat.TabIndex = 23;
            this.m_stat.Text = "stat";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 31);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(35, 13);
            this.label5.TabIndex = 26;
            this.label5.Text = "Name";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 53);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(22, 13);
            this.label6.TabIndex = 27;
            this.label6.Text = "HP";
            // 
            // nup_druidHPTarget
            // 
            this.nup_druidHPTarget.Location = new System.Drawing.Point(43, 51);
            this.nup_druidHPTarget.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nup_druidHPTarget.Name = "nup_druidHPTarget";
            this.nup_druidHPTarget.Size = new System.Drawing.Size(59, 20);
            this.nup_druidHPTarget.TabIndex = 1;
            this.nup_druidHPTarget.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // pb_druidHP
            // 
            this.pb_druidHP.Location = new System.Drawing.Point(43, 77);
            this.pb_druidHP.Name = "pb_druidHP";
            this.pb_druidHP.Size = new System.Drawing.Size(204, 23);
            this.pb_druidHP.TabIndex = 30;
            // 
            // tb_druidName
            // 
            this.tb_druidName.Location = new System.Drawing.Point(43, 28);
            this.tb_druidName.Name = "tb_druidName";
            this.tb_druidName.Size = new System.Drawing.Size(125, 20);
            this.tb_druidName.TabIndex = 0;
            // 
            // druid_active
            // 
            this.druid_active.AutoSize = true;
            this.druid_active.ForeColor = System.Drawing.Color.Red;
            this.druid_active.Location = new System.Drawing.Point(198, 31);
            this.druid_active.Name = "druid_active";
            this.druid_active.Size = new System.Drawing.Size(45, 13);
            this.druid_active.TabIndex = 32;
            this.druid_active.Text = "ACTIVE";
            // 
            // cb_druidHK
            // 
            this.cb_druidHK.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_druidHK.FormattingEnabled = true;
            this.cb_druidHK.Location = new System.Drawing.Point(108, 50);
            this.cb_druidHK.Name = "cb_druidHK";
            this.cb_druidHK.Size = new System.Drawing.Size(78, 21);
            this.cb_druidHK.TabIndex = 2;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.l_druidHP);
            this.groupBox3.Controls.Add(this.tb_druidName);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.cb_druidHK);
            this.groupBox3.Controls.Add(this.nup_druidHPTarget);
            this.groupBox3.Controls.Add(this.druid_active);
            this.groupBox3.Controls.Add(this.cb_druidActivate);
            this.groupBox3.Controls.Add(this.pb_druidHP);
            this.groupBox3.Location = new System.Drawing.Point(12, 287);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(260, 107);
            this.groupBox3.TabIndex = 37;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Druid Healing";
            // 
            // l_druidHP
            // 
            this.l_druidHP.AutoSize = true;
            this.l_druidHP.Location = new System.Drawing.Point(6, 82);
            this.l_druidHP.Name = "l_druidHP";
            this.l_druidHP.Size = new System.Drawing.Size(29, 13);
            this.l_druidHP.TabIndex = 34;
            this.l_druidHP.Text = "NaN";
            // 
            // cb_druidActivate
            // 
            this.cb_druidActivate.Appearance = System.Windows.Forms.Appearance.Button;
            this.cb_druidActivate.AutoSize = true;
            this.cb_druidActivate.Location = new System.Drawing.Point(192, 48);
            this.cb_druidActivate.Name = "cb_druidActivate";
            this.cb_druidActivate.Size = new System.Drawing.Size(56, 23);
            this.cb_druidActivate.TabIndex = 3;
            this.cb_druidActivate.Text = "Activate";
            this.cb_druidActivate.UseVisualStyleBackColor = true;
            this.cb_druidActivate.CheckedChanged += new System.EventHandler(this.cb_druidActivate_CheckedChanged);
            // 
            // l_refRate
            // 
            this.l_refRate.AutoSize = true;
            this.l_refRate.Location = new System.Drawing.Point(5, 12);
            this.l_refRate.Name = "l_refRate";
            this.l_refRate.Size = new System.Drawing.Size(47, 13);
            this.l_refRate.TabIndex = 43;
            this.l_refRate.Text = "RefRate";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(298, 349);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 39;
            this.button3.Text = "load";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.load_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(298, 308);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 38;
            this.button2.Text = "save";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.save_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(21, 258);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 44;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // MainScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(903, 404);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.l_refRate);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.m_stat);
            this.Controls.Add(this.HotkeysPresets);
            this.Controls.Add(this.m_dataTable);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "MainScreen";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "HealBot";
            this.Load += new System.EventHandler(this.MainScreen_Load);
            ((System.ComponentModel.ISupportInitialize)(this.m_dataTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HotkeysPresets)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nup_druidHPTarget)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView m_dataTable;
        private System.Windows.Forms.DataGridView HotkeysPresets;
        private System.Windows.Forms.Label m_stat;
        private System.Windows.Forms.DataGridViewTextBoxColumn Data;
        private System.Windows.Forms.DataGridViewTextBoxColumn Value;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown nup_druidHPTarget;
        private System.Windows.Forms.ProgressBar pb_druidHP;
        private System.Windows.Forms.TextBox tb_druidName;
        private System.Windows.Forms.Label druid_active;
        private System.Windows.Forms.ComboBox cb_druidHK;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label l_druidHP;
        private System.Windows.Forms.CheckBox cb_druidActivate;
        private System.Windows.Forms.Label l_refRate;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
    }
}