﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using HealBot11.Utils;
using HealBot11.Utils.Parser;
using HealBot11.TableItems;
using HealBot11.Threads;
using HealBot11.Refreshers;
using System.Globalization;

namespace HealBot11.GUI
{
    public partial class MainScreen : RefreshableForm
    {
        
        // workers
        private HotkeyWorker m_HKWorker = null;
        private DruidHealWorker m_DruidHealWorker = null;
        private BLSanityWorker m_BLSanityWorker = null;

        private RefreshFormEventHandler m_refreshHandler = null;
        private ArrayList m_hotkeyOptions = new ArrayList();
        private ArrayList m_columnCondiotionDataSource = null;
        private Dictionary<string, HpMpCondItem> m_condMapping = null;
        private ArrayList m_columnHotkeyDataSource = null;
        private Dictionary<string, Hotkey> m_hotkeyMapping = null;
        private Object m_lock = new Object();
        private bool m_running = false;
        private bool m_killFlag = false;
        private bool m_dumpStatisticsOnClose;
        private string m_dumpPrefix = "";
        private string m_loadFile = "";

        public MainScreen(double secsRefresh, bool queue, bool dumpStat, string loadFile)
        {
            InitializeComponent();

            l_refRate.Text = "Refresh rate " + secsRefresh;
            l_refRate.Text = queue ? l_refRate.Text + " q" : l_refRate.Text;
            m_dumpStatisticsOnClose = dumpStat;
            m_loadFile = loadFile;
            if (dumpStat)
            {
                m_dumpPrefix = "******StatDump******\n";
                m_dumpPrefix += "Refresh rate is " + secsRefresh.ToString() + "\n";
                m_dumpPrefix += "Queuemode is " + queue.ToString() + "\n";
            }

            m_columnCondiotionDataSource = InitHpMpColumn();
            m_columnHotkeyDataSource = InitHotkeys();
            InitHotkeyDataView();
            cb_druidHK.DataSource = InitHotkeys();
            cb_druidHK.DisplayMember = "KHName";
            cb_druidHK.ValueMember = "KHValue";
            druid_active.Visible = false;
            
            if (!Util.Update())
            {
                m_killFlag = true;
                KillIfNeeded();
            }

            m_refreshHandler = new RefreshFormEventHandler(this, secsRefresh);
            SetupWorkers(secsRefresh);
            m_HKWorker.SetActive();
            m_BLSanityWorker.SetActive();
        }

        private void MainScreen_Load(object sender, EventArgs e)
        {
            HotkeysPresets.Rows.Clear();
            m_refreshHandler.Start();

            if (m_loadFile != null)
            {
                Dictionary<string, IList<CompressedHotkeyTableEntry>> lines = PropertyFileParser.ReadProperties(m_loadFile);
                LoadDataFromData(lines);
            }
        }

        public DataGridViewRowCollection KHPresetsRows
        {
            get { return HotkeysPresets.Rows; }
        }

        public void KillWithMessage(string msg)
        {
            KillWorkers();
            if (m_refreshHandler != null)
            {
                m_refreshHandler.Stop();
            }

            if (m_dumpStatisticsOnClose)
            {
                Util.debugLog(m_dumpPrefix + getStatMessage());
            }

            if (!msg.Equals(""))
            {
                MessageBox.Show(msg);
            }
            Environment.Exit(0);
        }

        private void InitHotkeyDataView()
        {
            DataGridViewComboBoxColumn columnCondition = new DataGridViewComboBoxColumn();
            columnCondition.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            columnCondition.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            columnCondition.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            columnCondition.DataPropertyName = "Prefix";
            columnCondition.Name = "Prefix";
            columnCondition.HeaderText = "Prefix";
            columnCondition.DataSource = m_columnCondiotionDataSource;
            columnCondition.DisplayMember = "CondName";
            columnCondition.ValueMember = "This";

            DataGridViewTextBoxColumn columnAmount = new DataGridViewTextBoxColumn();
            columnAmount.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            columnAmount.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            columnAmount.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            columnAmount.DataPropertyName = "Amount";
            columnAmount.Name = "Amount";
            columnAmount.HeaderText = "Amount";

            DataGridViewComboBoxColumn columnHotkey = new DataGridViewComboBoxColumn();
            columnHotkey.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            columnHotkey.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            columnHotkey.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            columnHotkey.DataPropertyName = "Hotkey";
            columnHotkey.Name = "Hotkey";
            columnHotkey.HeaderText = "Hotkey";
            columnHotkey.DataSource = m_columnHotkeyDataSource;
            columnHotkey.DisplayMember = "KHName";
            columnHotkey.ValueMember = "This";

            DataGridViewCheckBoxColumn columnCheckbox = new DataGridViewCheckBoxColumn();
            columnCheckbox.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            columnCheckbox.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            columnCheckbox.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            columnCheckbox.DataPropertyName = "Active";
            columnCheckbox.Name = "Active";
            columnCheckbox.HeaderText = "Active";
            columnCheckbox.TrueValue = true;
            columnCheckbox.FalseValue = false;

            HotkeysPresets.Columns.Insert(0, columnCondition);
            HotkeysPresets.Columns.Insert(1, columnAmount);
            HotkeysPresets.Columns.Insert(2, columnHotkey);
            HotkeysPresets.Columns.Insert(3, columnCheckbox);
        }

        private ArrayList InitHpMpColumn()
        {
            ArrayList m_Column = new ArrayList();
            m_condMapping = new Dictionary<string, HpMpCondItem>();

            m_condMapping.Add(HpMpCondItem.HPPlus, new HpMpCondItem(HpMpCondItem.HPPlusText, HpMpCondItem.HPPlus));
            m_condMapping.Add(HpMpCondItem.HPMinus, new HpMpCondItem(HpMpCondItem.HPMinusText, HpMpCondItem.HPMinus));
            m_condMapping.Add(HpMpCondItem.MPPlus, new HpMpCondItem(HpMpCondItem.MPPlusText, HpMpCondItem.MPPlus));
            m_condMapping.Add(HpMpCondItem.MPMinus, new HpMpCondItem(HpMpCondItem.MPMinusText, HpMpCondItem.MPMinus));

            foreach (KeyValuePair<string, HpMpCondItem> entry in m_condMapping)
            {
                m_Column.Add(entry.Value);
            }

            return m_Column;
        }

        private ArrayList InitHotkeys(bool includeEmpty = false)
        {
            ArrayList ret = new ArrayList();
            bool initTable = (m_hotkeyMapping == null);
            if (initTable)
            {
                m_hotkeyMapping = new Dictionary<string, Hotkey>();
            }
            if (includeEmpty)
            {
                Hotkey HK = new Hotkey("None", null);
                ret.Add(HK);
            }

            for (int i = 0; i < 36; i++)
            {
                string name;
                Constants.KeyboardControls ctrlShift;
                if (i >= 0 && i <= 11)
                {
                    name = "F" + (i + 1).ToString();
                    ctrlShift = Constants.KeyboardControls.invalid;
                }
                else if (i >= 12 && i <= 23)
                {
                    name = "Shift+F" + (i % 12 + 1).ToString();
                    ctrlShift = Constants.KeyboardControls.shift;
                }
                else
                {
                    name = "Ctrl+F" + (i % 12 + 1).ToString();
                    ctrlShift = Constants.KeyboardControls.ctrl;
                }
                KeyCombo KC = new KeyCombo(ctrlShift, (Constants.KeyboardControls)((int)Constants.KeyboardControls.F1 + (i % 12)));
                Hotkey HK = new Hotkey(name, KC);

                if (initTable)
                {
                    m_hotkeyMapping.Add(name, HK);
                }
                else
                {
                    ret.Add(HK);
                }
            }

            if (initTable)
            {
                foreach (KeyValuePair<string, Hotkey> entry in m_hotkeyMapping)
                {
                    ret.Add(entry.Value);
                }
            }

            return ret;
        }

        public override void RefreshMe()
        {
            long time = DateTime.Now.Ticks;

            CheckClientAlive();
            KillIfNeeded();
            /*
            if ( !MemoryReader.Connected() )
            {
                UncheckAll();
                return;
            }
            */
            bool isSomeoneRunning;
            lock (m_lock)
            {
                isSomeoneRunning = m_running;
            }
            if (!isSomeoneRunning)
            {
                m_running = true;

                FillCharacterData(m_dataTable);

                m_running = false;
            }

            KillIfNeeded();
            time = DateTime.Now.Ticks - time;

            Statistics.GetInstance().Main = (time / TimeSpan.TicksPerMillisecond).ToString();
            StatRefresh();
        }

        private void SetupWorkers(double secsRefresh)
        {
            m_HKWorker = new HotkeyWorker(this, secsRefresh, ThreadPriority.Highest);
            m_DruidHealWorker = new DruidHealWorker(this, secsRefresh, ThreadPriority.Highest);
            m_BLSanityWorker = new BLSanityWorker(this, secsRefresh, ThreadPriority.Highest);
        }

        private void KillWorkers()
        {
            List<BaseWorker> workers = new List<BaseWorker>();

            if (m_HKWorker != null) workers.Add(m_HKWorker);
            if (m_DruidHealWorker != null) workers.Add(m_DruidHealWorker);
            if (m_BLSanityWorker != null) workers.Add(m_BLSanityWorker);

            foreach (BaseWorker w in workers)
            {
                w.DeactivateWorker();
            }
        }

        /* ---------------------------- DELEGATORS START ---------------------------- */

        public void DruidHealTargetNotFound(MainScreen form)
        {
            MethodInvoker method = delegate
            {
                druid_active.Visible = false;
                pb_druidHP.Value = 0;
                l_druidHP.Text = "NaN";
            };
            form.BeginInvoke(method);
        }

        public void DruidHealTargetFound(MainScreen form, int hp)
        {
            MethodInvoker method = delegate
            {
                druid_active.Visible = true;
                pb_druidHP.Value = hp;
                l_druidHP.Text = hp.ToString() + "%";
            };
            form.BeginInvoke(method);
        }
        
        /* ----------------------------- DELEGATORS END ----------------------------- */

        private void FillCharacterData(DataGridView table)
        {
            if (!CheckClientAlive())
            {
                return;
            }
            CultureInfo cult = new CultureInfo("en-US");
            UpdateRow(table, "Data", "Id", "Value", MemoryReader.Cid().ToString());
            UpdateRow(table, "Data", "HP/MAX", "Value", MemoryReader.Hp().ToString("N0", cult) + "/" + MemoryReader.MaxHp().ToString("N0", cult));
            UpdateRow(table, "Data", "MP/MAX", "Value", MemoryReader.Mp().ToString("N0", cult) + "/" + MemoryReader.MaxMp().ToString("N0", cult));
            UpdateRow(table, "Data", "Level", "Value", MemoryReader.Level().ToString("N0", cult));
            UpdateRow(table, "Data", "Exp", "Value", MemoryReader.Exp().ToString("N0", cult));
            UpdateRow(table, "Data", "Exp to next level", "Value", MemoryReader.XpNextLevel().ToString("N0", cult));
            UpdateRow(table, "Data", "Cap", "Value", MemoryReader.Cap().ToString("N", cult) + "/" + MemoryReader.MaxCap().ToString("N", cult));
            UpdateRow(table, "Data", "Soul", "Value", MemoryReader.Soul().ToString());
            UpdateRow(table, "Data", "Pos (x/y/z)", "Value", MemoryReader.X().ToString() + "/" +
                            MemoryReader.Y().ToString() + "/" + MemoryReader.Z().ToString());
        }

        private void UpdateRow(DataGridView table, string colName, string name, string colData, string data)
        {
            var duplicateRow = (from DataGridViewRow row in table.Rows
                                where (string)row.Cells[colName].Value == name
                                select row).FirstOrDefault();
            if (duplicateRow != null)
            {
                //duplicate row found, update it's columns value
                duplicateRow.Cells[colData].Value = data;
            }
            else
            {
                table.Rows.Add(name, data);
            }
        }

        private DataGridViewRow GetRowAlways(DataGridView table, string colName, string name)
        {
            var duplicateRow = (from DataGridViewRow row in table.Rows
                                where (string)row.Cells[colName].Value == name
                                select row).FirstOrDefault();
            if (duplicateRow == null)
            {
                int rowIndx = table.Rows.Add();
                duplicateRow = table.Rows[rowIndx];
                duplicateRow.Cells[colName].Value = name;
            }

            return duplicateRow;
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            base.OnFormClosing(e);

            if (e.CloseReason == CloseReason.WindowsShutDown) return;

            KillWithMessage("");
        }

        public bool CheckClientAlive()
        {
            if (Util.Tibia.HasExited)
            {
                m_killFlag = true;
            }

            return !m_killFlag;
        }

        private void KillIfNeeded()
        {
            if (m_killFlag)
            {
                this.Visible = false;
                KillWithMessage("No client found - Exiting");
            }
        }

        private void HotkeysPresets_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0)
            {
                return;
            }
            DataGridViewRow row = HotkeysPresets.Rows[e.RowIndex];
            DataGridViewCheckBoxCell isChecked = (DataGridViewCheckBoxCell)row.Cells["Active"];

            isChecked.Selected = false;

            DataGridViewComboBoxCell col1 = (DataGridViewComboBoxCell)row.Cells["Prefix"];
            DataGridViewTextBoxCell col2 = (DataGridViewTextBoxCell)row.Cells["Amount"];
            DataGridViewComboBoxCell col3 = (DataGridViewComboBoxCell)row.Cells["Hotkey"];

            int numericValue;
            if (col1.Value == null || col2.Value == null || col3.Value == null ||
                    !int.TryParse((string)col2.Value, out numericValue))
            {
                isChecked.EditingCellFormattedValue = false;
                MessageBox.Show("Some values in this line are invalid!");
            }
        }

        private void HotkeysPresets_CellChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0)
            {
                return;
            }
            DataGridViewRow row = HotkeysPresets.Rows[e.RowIndex];
            DataGridViewCheckBoxCell isChecked = (DataGridViewCheckBoxCell)row.Cells["Active"];

            isChecked.Selected = false;
        }

        private void cb_druidActivate_CheckedChanged(object sender, EventArgs e)
        {
            if (cb_druidActivate.Checked && tb_druidName.Text.Length < 1)
            {
                cb_druidActivate.Checked = false;
                MessageBox.Show("Invalid name!");
            }
            if (cb_druidActivate.Checked)
            {
                m_DruidHealWorker.SetActive(tb_druidName.Text, (int)nup_druidHPTarget.Value, (KeyCombo)cb_druidHK.SelectedValue);
            }
            else
            {
                m_DruidHealWorker.SetInactive();
                pb_druidHP.Value = 0;
                druid_active.Visible = false;
                l_druidHP.Text = "NaN";
            }
        }

        private void StatRefresh()
        {
            m_stat.Text = getStatMessage();
        }

        private string getStatMessage()
        {
            Statistics stat = Statistics.GetInstance();
            string text = "Main: " + stat.Main + "\n" +
                          "DruidHeal: " + stat.DruidHeal + "\n" +
                          "Hotkey: " + stat.Hotkey + "\n" +
                          "Keystroke: " + stat.Keystroke + "\n" +
                          "BL sanity: " + stat.BL + "\n";
            text += "\nMax: " + stat.GetMaxTime() + " ms";
            return text;
        }

        /* ---------------------------- SAVE/LOAD BASE INFRASTRUCTURE START ---------------------------- */
        private void save_Click(object sender, EventArgs e)
        {
            // save
            PropertyFileParser.SaveProperties(HotkeysPresets.Rows, cb_druidHK);
        }

        private void load_Click(object sender, EventArgs e)
        {
            // load
            Dictionary<string, IList<CompressedHotkeyTableEntry>> lines = PropertyFileParser.ReadProperties();
            LoadDataFromData(lines);
        }

        private void LoadDataFromData(Dictionary<string, IList<CompressedHotkeyTableEntry>> lines)
        {
            if (lines == null)
            {
                return;
            }

            HotkeysPresets.Rows.Clear();
            foreach (CompressedHotkeyTableEntry line in lines[PropertyFileParser.HOTKEY_NOTATION])
            {
                int index = HotkeysPresets.Rows.Add();
                HotkeysPresets.Rows[index].Cells["Prefix"].Value = m_condMapping[line.Prefix];
                HotkeysPresets.Rows[index].Cells["Amount"].Value = line.Amount;
                HotkeysPresets.Rows[index].Cells["Hotkey"].Value = m_hotkeyMapping[line.Hotkey];
            }

            // only load the FIRST occurence of each hotkey
            cb_druidHK.SelectedIndex = 0;
            if (lines[PropertyFileParser.DRUIDHEAL_NOTATION].Count > 0)
                cb_druidHK.SelectedIndex = findIndexOfHotkey(cb_druidHK, lines[PropertyFileParser.DRUIDHEAL_NOTATION][0].Hotkey);
        }

        private int findIndexOfHotkey(ComboBox cb, string hkname)
        {
            ArrayList al = (ArrayList)cb.DataSource;
            for (int i = 0; i < al.Count; i++)
            {
                Hotkey hk = (Hotkey)(al[i]);
                if (hk.KHName.Equals(hkname))
                {
                    return i;
                }
            }
            return 0;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MemoryReader.GetBattleTree();
        }

        /* ---------------------------- SAVE/LOAD BASE INFRASTRUCTURE END ---------------------------- */

    }
}
