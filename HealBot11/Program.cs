﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using HealBot11.GUI;
using HealBot11.Utils;

namespace HealBot11
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            if (CommandLineArgs.IsHelpNeeded(args))
            {
                CommandLineArgs.PrintHelp();
                return;
            }

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            CommandLineArgs cmd = new CommandLineArgs(args);

            Util.setKeyPressQueue(cmd.QueueMode);

            string file = null;
            if (!cmd.LoadFromFile.Equals(CommandLineArgs.DEFAULT_LOAD_FROM_FILE))
            {
                file = cmd.LoadFromFile;
            }
            Application.Run(new MainScreen(cmd.RefreshRate, cmd.QueueMode, cmd.DumbStat, file));
        }
    }
}
