﻿using System.Timers;

namespace HealBot11.Refreshers
{
    public class RefreshEventHandler
    {
        protected System.Timers.Timer m_timer;
        private Refreshable m_form;

        public RefreshEventHandler(Refreshable form, double secsRefresh)
        {
            m_form = form;
            m_timer = new System.Timers.Timer(secsRefresh * 1000);
            RegisterSelf();
        }

        public void Start()
        {
            m_timer.Start();
        }

        public void Stop()
        {
            m_timer.Stop();
        }

        public virtual void RegisterSelf()
        {
            m_timer.Elapsed += new ElapsedEventHandler(HandleRefresh);
        }

        private void HandleRefresh(object source, ElapsedEventArgs e)
        {
            m_form.RefreshMe();
        }
        
    }
}
