﻿using System.Timers;
using System.Windows.Forms;

namespace HealBot11.Refreshers
{
    public class RefreshFormEventHandler : RefreshEventHandler
    {
        private RefreshableForm m_form;

        public RefreshFormEventHandler(RefreshableForm form, double secsRefresh)
            : base(form, secsRefresh)
        {
            m_form = form;
        }

        public override void RegisterSelf()
        {
            m_timer.Elapsed += new ElapsedEventHandler(HandleFormRefresh);
        }

        private void HandleFormRefresh(object source, ElapsedEventArgs e)
        {
            MethodInvoker method = delegate
            {
                m_form.RefreshMe();
            };
            m_form.BeginInvoke(method);
        }
    }
}
