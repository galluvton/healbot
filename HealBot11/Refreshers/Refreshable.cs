﻿
namespace HealBot11.Refreshers
{
    public interface Refreshable
    {
        void RefreshMe();
    }
}
