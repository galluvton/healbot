﻿
namespace HealBot11.TableItems
{
    public class Creature
    {
        
        public string MyName { get; set; }

        public string MyFloor { get; set; }

        public string MyXy { get; set; }

        public uint MyOffset { get; set; }

        public int MyId { get; set; }
    }
}
