﻿
namespace HealBot11.TableItems
{
    public class HotkeyTableEntry
    {
        
        public string Amount { get; set; }

        public HpMpCondItem Prefix { get; set; }

        public Hotkey Hotkey { get; set; }

        public HotkeyTableEntry(string amount, HpMpCondItem cond, Hotkey hotkey)
        {
            Amount = amount;
            Prefix = cond;
            Hotkey = hotkey;
        }

    }
}
