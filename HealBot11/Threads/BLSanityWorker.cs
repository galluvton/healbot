﻿using System;
using System.Collections.Generic;
using System.Threading;
using HealBot11.Utils;
using HealBot11.GUI;
using HealBot11.TableItems;

namespace HealBot11.Threads
{
    public class BLSanityWorker : BaseWorker
    {
        
        public BLSanityWorker(MainScreen form, double refreshRate, ThreadPriority priority)
            : base(form, refreshRate, priority)
        { }

        public override string GetWorkerName()
        {
            return "BLSanityWorker";
        }

        public void SetActive()
        {
            base.ActivateWorker();
        }

        public void SetInactive()
        {
            base.DeactivateWorker();
        }

        public override void RunLogic()
        {
            MemoryReader.GetBattleList(true);
        }

        public override void UpdateStatistics(long time)
        {
            Statistics.GetInstance().BL = (time / TimeSpan.TicksPerMillisecond).ToString();
        }

        public static void PlayNotification()
        {
            System.Media.SystemSounds.Beep.Play();
            System.Threading.Thread.Sleep(100);
            System.Media.SystemSounds.Beep.Play();
        }

    }
}
