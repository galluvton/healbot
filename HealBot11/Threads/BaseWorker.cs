﻿using System;
using System.Threading;
using HealBot11.Utils;
using HealBot11.GUI;
using HealBot11.Refreshers;

namespace HealBot11.Threads
{
    public abstract class BaseWorker : Refreshable
    {
 
        protected MainScreen m_form = null;
        private RefreshEventHandler m_refreshHandler;
        private Object m_lock;
        private bool m_running;
        private ThreadPriority m_myPriority;

        public BaseWorker(MainScreen form, double refreshRate, ThreadPriority priority)
        {
            m_lock = new Object();
            m_form = form;
            m_running = false;
            m_refreshHandler = new RefreshEventHandler(this, refreshRate);
            m_myPriority = priority;
        }

        public void ActivateWorker()
        {
            m_refreshHandler.Start();
        }

        public void DeactivateWorker()
        {
            UpdateStatistics(0);
            m_refreshHandler.Stop();
        }

        public void RefreshMe()
        {
            Thread.CurrentThread.Priority = m_myPriority;
            if (!m_form.CheckClientAlive())
            {
                DeactivateWorker();
                return;
            }

            bool isSomeoneRunning;
            lock (m_lock)
            {
                isSomeoneRunning = m_running;
            }

            if (isSomeoneRunning)
            {
                // DEBUG
                Util.debugLog("DEBUG!!! Two threads running on the same worker at the same time! In " + GetWorkerName());
                return;
            }

            m_running = true;
            long time = DateTime.Now.Ticks;

            RunLogic();
            
            time = DateTime.Now.Ticks - time;
            UpdateStatistics(time);
            m_running = false;
        }

        public abstract void RunLogic();

        public abstract void UpdateStatistics(long time);

        public abstract string GetWorkerName();

    }
}
