﻿using System;
using System.Collections.Generic;
using System.Threading;
using HealBot11.Utils;
using HealBot11.GUI;
using HealBot11.TableItems;

namespace HealBot11.Threads
{
    public class DruidHealWorker : BaseWorker
    {
        private string m_targetName;
        private int m_targetHP;
        private KeyCombo m_selectedHK;

        public DruidHealWorker(MainScreen form, double refreshRate, ThreadPriority priority)
            : base(form, refreshRate, priority)
        { }

        public override string GetWorkerName()
        {
            return "DruidHealWorker";
        }

        public void SetActive(string name, int hp, KeyCombo kc)
        {
            m_targetName = name.ToLower();
            m_targetHP = hp;
            m_selectedHK = kc;

            base.ActivateWorker();
        }

        public void SetInactive()
        {
            base.DeactivateWorker();
        }

        public override void RunLogic()
        {
            MemoryReader.PlayerData player = MemoryReader.GetPlayerFromBattlelist(m_targetName);

            if (player == null)
            {
                // name not found, return
                m_form.DruidHealTargetNotFound(m_form);
                return;
            }

            if (player.hp_perc <= m_targetHP)
            {
                Util.sendKeystroke(Util.PRIO_MID, m_selectedHK.KHValue.ToString(), m_selectedHK.CtrlShift.ToString());
            }

            (new Thread(this.updateFormGUI)).Start(player.hp_perc);
        }

        public override void UpdateStatistics(long time)
        {
            Statistics.GetInstance().DruidHeal = (time / TimeSpan.TicksPerMillisecond).ToString();
        }

        private void updateFormGUI(object hpAsInt)
        {
            Thread.CurrentThread.Priority = ThreadPriority.Normal;
            m_form.DruidHealTargetFound(m_form, (int)hpAsInt);
        }

    }
}
