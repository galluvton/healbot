﻿using System;

namespace HealBot11.Utils
{
    public static class Addresses
    {

        public static class Player
        {
            public static UInt32[] Cid = { 0x4555C8, 0x8, 0x1D8, 0xE4, 0x10 };
            public static UInt32[] Hp = { 0x4555C8, 0x8, 0x1D8, 0x60, 0x8 };
            public static UInt32[] MaxHp = { 0x4555C8, 0x8, 0x1D8, 0x60, 0xA };
            public static UInt32[] Mp = { 0x4555C8, 0x8, 0x1D8, 0x60, 0x50 };
            public static UInt32[] MaxMp = { 0x4555C8, 0x8, 0x1D8, 0x60, 0x52 };
            public static UInt32[] Cap = { 0x4555C8, 0x8, 0x1D8, 0x60, 0xC };
            public static UInt32[] MaxCap = { 0x4555C8, 0x8, 0x1D8, 0x60, 0x10 };
            public static UInt32[] Soul = { 0x4555C8, 0x8, 0x1D8, 0x60, 0x54 };
            public static UInt32[] XPos = { 0x4555C8, 0x8, 0x134, 0x24, 0x18 };
            public static UInt32[] YPos = { 0x4555C8, 0x8, 0x134, 0x24, 0x1C };
            public static UInt32[] ZPos = { 0x4555C8, 0x8, 0x134, 0x24, 0x20 };
            public static UInt32[] Exp = { 0x4555C8, 0x8, 0x1D8, 0x60, 0x18 };
            public static UInt32[] Level = { 0x4555C8, 0x8, 0x1D8, 0x60, 0x20 };
        }

        public static class BattleList
        {
            public static UInt32[] Root = { 0x4555C8, 0x8, 0x1D8, 0xE4, 0x8 };
            // relevant to root only
            public static UInt32 StarterItemOffset = 0x0;
            public static UInt32 ItemCountOffset = 0x4;
            // inside every entry
            public static UInt32 P0_Offset = 0x0;
            public static UInt32 P1_Offset = 0x4;
            public static UInt32 P2_Offset = 0x8;
            public static UInt32 TagOffset = 0xC;
            public static UInt32 CidOffset = 0x10;
            public static UInt32 InfoOffset = 0x14;
            public static UInt32 STARTER_TAG = 0x101;
            // inside the info block
            public static UInt32 NameOffset = 0x1C;
            public static UInt32 X_Offset = 0x24;
            public static UInt32 Y_Offset = 0x28;
            public static UInt32 Z_Offset = 0x2C;
            public static UInt32 HPPercentOffset = 0x7C;
        }

        public static class QString
        {
            public static UInt32 SizeOffset = 0x4;
            public static UInt32 StartOffset = 0xC;
        }

        /*
        public static class PlayerFlags
        {
            public static UInt32 FlagAddress = 0x544F5C;
            public const UInt32 FlagPoisened = 0x1;
            public const UInt32 FlagBurning = 0x2;
            public const UInt32 FlagEnergiezed = 0x4;
            public const UInt32 FlagDrunked = 0x8;
            public const UInt32 FlagManashield = 0x10;
            public const UInt32 FlagParalized = 0x20;
            public const UInt32 FlagHasted = 0x40;
            public const UInt32 FlagBattle = 0x80;
            public const UInt32 FlagDrowning = 0x100;
            public const UInt32 FlagFreezeing = 0x200;
            public const UInt32 FlagDazzled = 0x400;
            public const UInt32 FlagCursed = 0x800;
            public const UInt32 FlagBuffed = 0x1000;
            public const UInt32 FlagNoLogoutOrProtectionZone = 0x2000;
            public const UInt32 FlagInProtectionZone = 0x4000;
            public const UInt32 FlagBleeding = 0x8000;
        }

        public static class Equipment
        {
            public static UInt32 ArrowSlotStart = 0x782FFC;
            public const UInt32 ArrowSlotAmount = 0x0;
            public const UInt32 ArrowSlotItemId = 0x4;
            public static UInt32 RingSlotStart = 0x783020;
            public const UInt32 RingSlotItemId = 0x0;
            public static UInt32 AmuletSlotStart = 0x783100;
            public const UInt32 AmuletSlotItemId = 0x0;
        }
        */

    }
}
