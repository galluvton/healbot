﻿using System;

namespace HealBot11.Utils
{
    public class CommandLineArgs
    {

        private const double DEFAULT_REFRESH_RATE = 0.3;
        private const bool DEFAULT_QUEUE_MODE = false;
        private const bool DEFAULT_DUMP_STAT = false;
        public const string DEFAULT_LOAD_FROM_FILE = "";
        private static readonly string ST_HELP_TOKEN = "/h";
        private static readonly string ST_REFRESH_RATE = "r";
        private static readonly string ST_QUEUE_MODE = "q";
        private static readonly string ST_DUMP_STAT = "s";
        private static readonly string ST_LOAD_FROM_FILE = "l";

        public double RefreshRate;
        public bool QueueMode;
        public bool DumbStat;
        public string LoadFromFile;

        public CommandLineArgs(string[] args)
        {
            RefreshRate = DEFAULT_REFRESH_RATE;
            QueueMode = DEFAULT_QUEUE_MODE;
            QueueMode = DEFAULT_QUEUE_MODE;
            LoadFromFile = DEFAULT_LOAD_FROM_FILE;

            bool refreshFound = false;
            bool queueFound = false;
            bool statFound = false;
            bool loadFileFound = false;

            for (int i = 0; i < args.Length; i++)
            {
                if (!refreshFound && args[i].ToLower().Equals(ST_REFRESH_RATE) && (i + 1) < args.Length)
                {
                    i++;
                    double temp;
                    if (Double.TryParse(args[i], out temp))
                    {
                        RefreshRate = temp;
                        refreshFound = true;
                    }
                }
                else if (!queueFound && args[i].ToLower().Equals(ST_QUEUE_MODE))
                {
                    QueueMode = true;
                    queueFound = true;
                }
                else if (!statFound && args[i].ToLower().Equals(ST_DUMP_STAT))
                {
                    DumbStat = true;
                    statFound = true;
                }
                else if (!loadFileFound && args[i].ToLower().Equals(ST_LOAD_FROM_FILE) && (i + 1) < args.Length)
                {
                    i++;
                    LoadFromFile = args[i];
                    loadFileFound = true;
                }
            }
        }

        public static bool IsHelpNeeded(string[] args)
        {
            for (int i = 0; i < args.Length; i++)
            {
                if (args[i].ToLower().Equals(ST_HELP_TOKEN))
                {
                    return true;
                }
            }
            return false;
        }

        public static void PrintHelp()
        {
            string message = "Bot options:\n\n"+
                             "/h - help menu\n"+
                             "r - refresh rate. must be followed by a number\n"+
                             "q - queue mode. expanded help on queue mode later\n"+
                             "s - statistics dump. on close, bot will log statistics to a file\n"+
                             "l - load file on startup. the bot will load a configuration file when starting up. "+
                             "must be followed by the file (full path)\n";

            message +=       "\nExpanded explenations:\n\n" +
                             "r - refresh rate:\ndefault: 0.3\nthe sample rate for the bot.\n\n" +
                             "q - queue mode:\ndefault: off\nwhen queue mode is off, every key press the bot tries to simulate "+
                             "is sent to the keyboard as soon as it can, regardless of what component sent it.\non queue mode, "+
                             "the bot gives every component a priorty rank, and will always try to send the highest ranking "+
                             "press that is waiting for a click. note that on queue mode, the checkbox for overriding CTRL/SHIFT is ignored. "+
                             "The ranking is:\nTop: hotey\nMid: druidheal, manashield(shield, arrows, rings)\nBot: manasit\n\n" +
                             "s - statistics dump:\ndefault: off\non close, bot will log statistics to a file named"+
                             " 'botCrashReport.txt' in the same folder as the bot\n\n";
            System.Windows.Forms.MessageBox.Show(message);
        }

    }
}
