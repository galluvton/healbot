﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.Diagnostics;

namespace HealBot11.Utils
{
    public static class MemoryReader
    {
        [DllImport("kernel32.dll")]
        public static extern Int32 ReadProcessMemory(IntPtr hProcess, IntPtr lpBaseAddress,
            [In, Out] byte[] buffer, UInt32 size, out IntPtr lpNumberOfBytesRead);

        [DllImport("user32.dll", EntryPoint = "FindWindow")]
        public static extern IntPtr FindWindowEx(IntPtr parentHandle, IntPtr childAfter, string lclassName, string windowTitle);

        [DllImport("user32.dll", EntryPoint = "ClientToScreen")]
        public static extern bool ClientToScreen(HandleRef hwnd, out System.Drawing.Point lpRect);

        [DllImport("kernel32.dll")]
        public static extern Int32 WriteProcessMemory(IntPtr hProcess, IntPtr lpBaseAddress,
            [In, Out] byte[] buffer, UInt32 size, out IntPtr lpNumberOfBytesWritten);

        public static byte[] ReadBytes(IntPtr Handle, Int64 Address, uint BytesToRead)
        {
            IntPtr ptrBytesRead;
            byte[] buffer = new byte[BytesToRead];
            ReadProcessMemory(Handle, new IntPtr(Address), buffer, BytesToRead, out ptrBytesRead);
            return buffer;
        }
        public static bool WriteBytes(IntPtr handle, Int64 address, byte[] bytes, uint length)
        {
            IntPtr bytesWritten;

            // Write to memory
            int result = WriteProcessMemory(handle, new IntPtr(address), bytes, length, out bytesWritten);

            return result != 0;
        }
        /*******************************************************************************************************/
        public static byte ReadByte(Int64 Address, IntPtr? Handle = null)
        {
            return ReadBytes(getIntPtr(Handle), Address, 1)[0];
        }
        public static bool ReadBool(Int64 Address, uint length, IntPtr? Handle = null)
        {
            return BitConverter.ToBoolean(ReadBytes(getIntPtr(Handle), Address, length), 0);
        }
        public static int ReadInt32(long Address, uint length = 4, IntPtr? Handle = null)
        {
            return BitConverter.ToInt32(ReadBytes(getIntPtr(Handle), Address, length), 0);
        }
        public static UInt32 ReadUInt32(long Address, uint length = 4, IntPtr? Handle = null)
        {
            return BitConverter.ToUInt32(ReadBytes(getIntPtr(Handle), Address, length), 0);
        }
        public static ushort ReadUInt16(long Address, IntPtr? Handle = null)
        {
            return BitConverter.ToUInt16(ReadBytes(getIntPtr(Handle), Address, 2), 0);
        }
        public static double ReadDouble(long Address, IntPtr? Handle = null)
        {
            return BitConverter.ToDouble(ReadBytes(getIntPtr(Handle), Address, 8), 0);
        }
        public static string ReadString(long Address, uint length = 32, IntPtr? Handle = null)
        {
            string temp3 = ASCIIEncoding.Default.GetString(ReadBytes(getIntPtr(Handle), Address, length));
            string[] temp3str = temp3.Split('\0');
            return temp3str[0];
        }
        public static string ReadQString(long Address, IntPtr? Handle = null)
        {
            UInt32 length = ReadUInt32(Address + Addresses.QString.SizeOffset);
            UInt32 startOffset = ReadUInt32(Address + Addresses.QString.StartOffset);
            return Encoding.Unicode.GetString(ReadBytes(getIntPtr(Handle), Address + startOffset, length * 2));
        }
        public static UInt32 ReadPtr(long Address, UInt32[] offsets, uint length = 32, IntPtr? Handle = null)
        {
            UInt32 Addr = (UInt32)ReadInt32(Address + Util.Base);
            UInt32 NewAddr = Addr;
            int i = 0;
            foreach (UInt32 offset in offsets)
            {
                if (i != offsets.Length - 1)
                {
                    NewAddr = (UInt32)ReadInt32(NewAddr + offset);
                }
                else
                {
                    return NewAddr + offset;
                }
                i++;
            }
            return NewAddr;
        }
        public static string ReadStringPtr(long Address, UInt32[] offsets, uint length = 32, IntPtr? Handle = null)
        {
            UInt32 Addr = (UInt32)ReadInt32(Address + Util.Base);
            UInt32 NewAddr = Addr;
            foreach (UInt32 offset in offsets)
            {
                NewAddr = (UInt32)ReadInt32(NewAddr + offset);
            }
            return ReadString(NewAddr, 255);
        }
        public static bool WriteInt32(long Address, int Value, IntPtr? Handle = null)
        {
            byte[] oldBytes = BitConverter.GetBytes(Value);
            return WriteBytes(getIntPtr(Handle), Address, oldBytes, 4);
        }

        public static IntPtr getIntPtr(IntPtr? Handle)
        {
            if (Handle == null)
            {
                return Util.Tibia.Handle;
            }
            else
            {
                return (IntPtr)Handle;
            }
        }

        private static UInt16 GetUInt16Value(UInt32[] offsets, UInt32 address)
        {
            int i = 0;
            for (; i < offsets.Length - 1; i++)
            {
                address = ReadUInt32(address + offsets[i]);
            }
            return ReadUInt16(address + offsets[i]);
        }

        private static UInt32 GetUInt32Value(UInt32[] offsets, UInt32 address)
        {
            int i = 0;
            for (; i < offsets.Length - 1; i++)
            {
                address = ReadUInt32(address + offsets[i]);
            }
            return ReadUInt32(address + offsets[i]);
        }


        /*****************************************************************************************************************/
        /*****************************************************************************************************************/

        // Getting client functions
        public static Process[] getClients()
        {
            Process[] procs = Process.GetProcessesByName("Client");
            List<Process> prcs = new List<Process>();
            foreach (Process proc in procs)
            {
                prcs.Add(proc);
            }
            return prcs.ToArray();
        }

        public static Process getFirstClient()
        {
            if (getClients().Length != 0)
            {
                return getClients()[0];
            }
            else
            {
                return null;
            }
        }
        
        // Reading self info
        public static int Level()
        {
            return (int)GetUInt16Value(Addresses.Player.Level, Util.Base);
        }
         
        public static int Hp()
        {
            return (int)GetUInt16Value(Addresses.Player.Hp, Util.Base);
        }

        public static int MaxHp()
        {
            return (int)GetUInt16Value(Addresses.Player.MaxHp, Util.Base);
        }

        public static int Mp()
        {
            return (int)GetUInt16Value(Addresses.Player.Mp, Util.Base);
        }

        public static int MaxMp()
        {
            return (int)GetUInt16Value(Addresses.Player.MaxMp, Util.Base);
        }
        
        public static double Cap()
        {
            UInt32 temp = GetUInt32Value(Addresses.Player.Cap, Util.Base);
            return (temp / 100.0);
        }

        public static double MaxCap()
        {
            UInt32 temp = GetUInt32Value(Addresses.Player.MaxCap, Util.Base);
            return (temp / 100.0);
        }

        public static int Soul()
        {
            return (int)GetUInt16Value(Addresses.Player.Soul, Util.Base);
        }

        public static int X()
        {
            return (int)GetUInt32Value(Addresses.Player.XPos, Util.Base);
        }

        public static int Y()
        {
            return (int)GetUInt32Value(Addresses.Player.YPos, Util.Base);
        }

        public static int Z()
        {
            return (int)GetUInt32Value(Addresses.Player.ZPos, Util.Base);
        }

        public static int Cid()
        {
            return (int)GetUInt32Value(Addresses.Player.Cid, Util.Base);
        }
        
        public static int Exp()
        {
            return (int)GetUInt32Value(Addresses.Player.Exp, Util.Base);
        }

        public static int XpNextLevel()
        {
            int lvl = Level();
            int res = (50 * lvl * lvl * lvl - 150 * lvl * lvl + 400 * lvl) / 3;
            return res - Exp();
        }
        
        public class PlayerData {
            public UInt32 id;
            public UInt32 x;
            public UInt32 y;
            public UInt32 z;
            public string name;
            public UInt32 hp_perc;
        }

        public static PlayerData GetPlayerFromBattlelist(string playerName)
        {
            HashSet<UInt32> nodesToVisit = new HashSet<UInt32>();
            HashSet<UInt32> nodesVisited = new HashSet<UInt32>();

            UInt32 starterItemAddress = GetUInt32Value(Addresses.BattleList.Root, Util.Base) + Addresses.BattleList.StarterItemOffset;
            AddNodes(starterItemAddress, nodesToVisit, nodesVisited);
            while (nodesToVisit.Count > 0)
            {
                UInt32 curNode = nodesToVisit.First();

                nodesVisited.Add(curNode);
                nodesToVisit.Remove(curNode);
                if (ReadUInt16(curNode + Addresses.BattleList.TagOffset) == Addresses.BattleList.STARTER_TAG)
                    continue;

                UInt32 infoBlockAddress = ReadUInt32(curNode + Addresses.BattleList.InfoOffset);
                string name = ReadQString(ReadUInt32(infoBlockAddress + Addresses.BattleList.NameOffset));
                if (playerName.Contains(name.ToLower()))
                {
                    PlayerData player = new PlayerData();
                    player.id = ReadUInt32(curNode + Addresses.BattleList.CidOffset);
                    player.name = name;
                    player.x = ReadUInt32(infoBlockAddress + Addresses.BattleList.X_Offset);
                    player.y = ReadUInt32(infoBlockAddress + Addresses.BattleList.Y_Offset);
                    player.z = ReadUInt32(infoBlockAddress + Addresses.BattleList.Z_Offset);
                    player.hp_perc = ReadUInt16(infoBlockAddress + Addresses.BattleList.HPPercentOffset);

                    return player;
                }

                AddNodes(curNode, nodesToVisit, nodesVisited);
            }

            return null;
        }

        public static List<PlayerData> GetBattleList(bool validateOnly = false)
        {
            List<PlayerData> list = new List<PlayerData>();
            HashSet<UInt32> nodesToVisit = new HashSet<UInt32>();
            HashSet<UInt32> nodesVisited = new HashSet<UInt32>();
            HashSet<UInt32> CidList = new HashSet<UInt32>();

            UInt32 starterItemAddress = GetUInt32Value(Addresses.BattleList.Root, Util.Base) + Addresses.BattleList.StarterItemOffset;
            AddNodes(starterItemAddress, nodesToVisit, nodesVisited);
            while (nodesToVisit.Count > 0)
            {
                PlayerData player = new PlayerData();
                UInt32 curNode = nodesToVisit.First();

                nodesVisited.Add(curNode);
                nodesToVisit.Remove(curNode);
                if (ReadUInt16(curNode + Addresses.BattleList.TagOffset) == Addresses.BattleList.STARTER_TAG)
                    continue;

                player.id = ReadUInt32(curNode + Addresses.BattleList.CidOffset);
                CidList.Add(player.id);
                if (!validateOnly)
                {
                    UInt32 infoBlockAddress = ReadUInt32(curNode + Addresses.BattleList.InfoOffset);
                    player.name = ReadQString(ReadUInt32(infoBlockAddress + Addresses.BattleList.NameOffset));
                    player.x = ReadUInt32(infoBlockAddress + Addresses.BattleList.X_Offset);
                    player.y = ReadUInt32(infoBlockAddress + Addresses.BattleList.Y_Offset);
                    player.z = ReadUInt32(infoBlockAddress + Addresses.BattleList.Z_Offset);
                    player.hp_perc = ReadUInt16(infoBlockAddress + Addresses.BattleList.HPPercentOffset);
                }
                list.Add(player);

                AddNodes(curNode, nodesToVisit, nodesVisited);
            }

            if (list.Count != CidList.Count) 
            {
                Util.debugLog("Sanity: Improper battlelist!!!!");
                (new System.Threading.Thread(Threads.BLSanityWorker.PlayNotification)).Start();
            }

            return list;
        }

        private static void AddNodes(UInt32 starterNodeAddress, HashSet<UInt32> nodesToVisit, HashSet<UInt32> nodesVisited)
        {
            if (starterNodeAddress == 0)
                return;
            UInt32 p;
            p = ReadUInt32(starterNodeAddress + Addresses.BattleList.P0_Offset);
            if (p > 0 && !nodesVisited.Contains(p))
                nodesToVisit.Add(p);
            p = ReadUInt32(starterNodeAddress + Addresses.BattleList.P1_Offset);
            if (p > 0 && !nodesVisited.Contains(p))
                nodesToVisit.Add(p);
            p = ReadUInt32(starterNodeAddress + Addresses.BattleList.P2_Offset);
            if (p > 0 && !nodesVisited.Contains(p))
                nodesToVisit.Add(p);
        }

        private static UInt32 First(this HashSet<UInt32> list)
        {
            HashSet<UInt32>.Enumerator enumer = list.GetEnumerator();
            enumer.MoveNext();
            return enumer.Current;
        }





        /*
         * DEBUUUUUUUUUUUUUUUUUUUUUUUUUUUUUG
        public class TriNode
        {
            public UInt32 Adr;
            public UInt32 Cid;
            public UInt32 P0;
            public UInt32 P1;
            public UInt32 P2;
            public UInt32 Tag;
            public TriNode P_0;
            public TriNode P_1;
            public TriNode P_2;

            public override string ToString()
            {
                return String.Format("{0,10}[{1,10}]: {2,10}, {3,10}, {4,10}, {5,1}", Adr.ToString(), Cid.ToString(), P0.ToString(),
                    P1.ToString(), P2.ToString(), Tag.ToString());
            }
        }

        public static void GetBattleTree()
        {
            Dictionary<UInt32, TriNode> dict = new Dictionary<UInt32, TriNode>();
            List<TriNode> list = new List<TriNode>();
            HashSet<UInt32> nodesToVisit = new HashSet<UInt32>();
            HashSet<UInt32> nodesVisited = new HashSet<UInt32>();

            UInt32 starterItemAddress = GetUInt32Value(Addresses.BattleList.Root, Util.Base) + Addresses.BattleList.StarterItemOffset;
            TriNode node = new TriNode();
            TriNode root = node;
            AddNodes(starterItemAddress, node, nodesToVisit, nodesVisited);
            node.Tag = 0x101;
            node.Adr = starterItemAddress;
            node.Cid = 0;
            dict.Add(node.Adr, node);
            list.Add(node);
            while (nodesToVisit.Count > 0)
            {
                node = new TriNode();
                UInt32 curNode = nodesToVisit.First();

                nodesVisited.Add(curNode);
                nodesToVisit.Remove(curNode);
                node.Adr = curNode;
                node.Tag = ReadUInt16(curNode + Addresses.BattleList.TagOffset);
                node.Cid = ReadUInt32(curNode + Addresses.BattleList.CidOffset);
                if (node.Tag == Addresses.BattleList.STARTER_TAG)
                    continue;
                
                AddNodes(curNode, node, nodesToVisit, nodesVisited);
                list.Add(node);
                dict.Add(node.Adr, node);
            }

            foreach (TriNode nod in dict.Values)
            {
                nod.P_0 = dict.Get(nod.P0);
                nod.P_1 = dict.Get(nod.P1);
                nod.P_2 = dict.Get(nod.P2);
            }

            nodesVisited = new HashSet<UInt32>();
            Util.debugLog(list.Stringy());
            Util.debugLog(root.Stringy(0, nodesVisited));
        }

        private static void AddNodes(UInt32 starterNodeAddress, TriNode node, HashSet<UInt32> nodesToVisit, HashSet<UInt32> nodesVisited)
        {
            if (starterNodeAddress == 0)
                return;
            UInt32 p;
            p = ReadUInt32(starterNodeAddress + Addresses.BattleList.P0_Offset);
            if (p > 0 && !nodesVisited.Contains(p))
            {
                nodesToVisit.Add(p);
                if (node != null)
                    node.P0 = p;
            }
            p = ReadUInt32(starterNodeAddress + Addresses.BattleList.P1_Offset);
            if (p > 0 && !nodesVisited.Contains(p))
            {
                nodesToVisit.Add(p);
                if (node != null)
                    node.P1 = p;
            }
            p = ReadUInt32(starterNodeAddress + Addresses.BattleList.P2_Offset);
            if (p > 0 && !nodesVisited.Contains(p))
            {
                nodesToVisit.Add(p);
                if (node != null)
                    node.P2 = p;
            }
        }

        private static string Stringy(this List<TriNode> list)
        {
            string ans = "";
            foreach (TriNode node in list)
            {
                ans += node.ToString() + "\n";
            }
            return ans;
        }

        private static string Stringy(this TriNode node, int depth, HashSet<UInt32> visited)
        {
            string ans;
            ans = String.Format("{0,1}:{1," + (10 + depth * 5).ToString() + "}/{2,1}\n", depth, node.Cid, node.Tag);
            visited.Add(node.Adr);
            if (node.P_0 != null && !visited.Contains(node.P0))
                ans += "l" + node.P_0.Stringy(depth + 1, visited);
            if (node.P_1 != null && !visited.Contains(node.P1))
                ans += "m" + node.P_1.Stringy(depth + 1, visited);
            if (node.P_2 != null && !visited.Contains(node.P2))
                ans += "r" + node.P_2.Stringy(depth + 1, visited);
            return ans;
        }

        public static U Get<T, U>(this Dictionary<T, U> dict, T key)
        where U : class
        {
            U val;
            dict.TryGetValue(key, out val);
            return val;
        }
        */
    }
}
