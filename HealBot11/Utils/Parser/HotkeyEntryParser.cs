﻿using System;
using HealBot11.TableItems;

namespace HealBot11.Utils.Parser
{
    public class HotkeyEntryParser
    {
        private const char DELIM = '$';
        private static readonly HpMpCondItem default_cond = new HpMpCondItem(HpMpCondItem.HPPlusText, HpMpCondItem.HPPlus);

        public static string ParseToFile(HpMpCondItem cond, string amount, Hotkey HK)
        {
            HotkeyTableEntry ans = new HotkeyTableEntry(amount, cond, HK);
            return ToString(ans);
        }

        public static CompressedHotkeyTableEntry ParseFromFile(string dataAsString)
        {
            string[] data = dataAsString.Split(DELIM);
            if (data.Length == 3)
            {
                return new CompressedHotkeyTableEntry(data[1], data[0], data[2]);
            }
            throw new Exception();
        }

        private static string ToString(HotkeyTableEntry data)
        {
            string ans = "";
            ans += data.Prefix == null ? default_cond.ToString() : data.Prefix.ToString();
            ans += DELIM + (data.Amount == null ? "0" : data.Amount.ToString());
            ans += DELIM + data.Hotkey.KHName;

            return ans;
        }

    }
}
