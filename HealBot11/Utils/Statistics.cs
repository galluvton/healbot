﻿using System;
using System.Collections.Generic;

namespace HealBot11.Utils
{
    
    public class Statistics
    {
        private static Statistics m_instance = null;
        private static Object m_lock = new object();
        private static Dictionary<string, LockedStat> statMap;

        private Statistics()
        {
            statMap = new Dictionary<string, LockedStat>();

            statMap.Add("Hotkey", new LockedStat());
            statMap.Add("DruidHeal", new LockedStat());
            statMap.Add("Keystroke", new LockedStat());
            statMap.Add("Main", new LockedStat());
            statMap.Add("BL", new LockedStat()); // battlelist sanity
        }

        public static Statistics GetInstance()
        {
            if (m_instance == null)
            {
                lock (m_lock)
                {
                    if (m_instance == null)
                    {
                        m_instance = new Statistics();
                    }
                }
            }
            return m_instance;
        }

        public string Hotkey
        {
            get {
                return statMap["Hotkey"].Ticks;
            }
            set {
                statMap["Hotkey"].Ticks = value;
            }
        }

        public string DruidHeal
        {
            get
            {
                return statMap["DruidHeal"].Ticks;
            }
            set
            {
                statMap["DruidHeal"].Ticks = value;
            }
        }

        public string Main
        {
            get
            {
                return statMap["Main"].Ticks;
            }
            set
            {
                statMap["Main"].Ticks = value;
            }
        }

        public string Keystroke
        {
            get
            {
                return statMap["Keystroke"].Ticks;
            }
            set
            {
                statMap["Keystroke"].Ticks = value;
            }
        }

        public string BL
        {
            get
            {
                return statMap["BL"].Ticks;
            }
            set
            {
                statMap["BL"].Ticks = value;
            }
        }

        public string GetMaxTime()
        {
            int max1 = 0;
            int max2 = 0;
            foreach (LockedStat stat in statMap.Values)
            {
                max1 = Math.Max(max1, stat.Current);
                max2 = Math.Max(max2, stat.Max);
            }
            
            return max1.ToString() + "(" + max2.ToString() + ")";
        }


        private class LockedStat
        {
            private int m_current = 0;
            private int m_max = 0;
            private Object m_lock = new object();

            public int Current
            {
                get
                {
                    lock (m_lock)
                    {
                        return m_current;
                    }
                }
            }

            public int Max
            {
                get
                {
                    lock (m_lock)
                    {
                        return m_max;
                    }
                }
            }

            public string Ticks
            {
                get
                {
                    lock (m_lock)
                    {
                        return m_max == 0 ? "NA" : m_current.ToString() + "(" + m_max.ToString() + ") ms";
                    }
                }
                set
                {
                    lock (m_lock)
                    {
                        int i_val;
                        if (Int32.TryParse(value, out i_val))
                        {
                            m_max = (i_val > m_max) ? i_val : m_max;
                            m_current = i_val;
                        }
                    }
                }
            }
        }
    }
}
