﻿﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using HealBot11.Utils.KeyPresser;

[assembly: InternalsVisibleToAttribute("AKeyPresser")]

namespace HealBot11.Utils
{
    public static class Util
    {

        public const int PRIO_TOP = BlockingPriorityQueue<int>.PRIO_TOP;
        public const int PRIO_MID = BlockingPriorityQueue<int>.PRIO_MID;
        public const int PRIO_BOT = BlockingPriorityQueue<int>.PRIO_BOT;
        private static AKeyPresser keyPresser = new KeyPresserNormal();

        public static void setKeyPressQueue(bool p)
        {
            if (p)
            {
                keyPresser = new KeyPresserBlockingQueue();
            }
            else
            {
                keyPresser = new KeyPresserNormal();
            }
        }

        [DllImport("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, Int32 Msg, IntPtr wParam, IntPtr lParam);
        
        [DllImport("user32.dll")]
        public static extern short GetKeyState(int nVirtKey);

        public static Process _Tibia = null;
        public static UInt32 _BaseAddress;
        public static IntPtr _Handle;

        public static Process Tibia
        {
            get 
            {
                return _Tibia; 
            }
            set
            {
                if (value != null)
                {
                    _Tibia = value;
                    ProcessModuleCollection myProcessModuleCollection = _Tibia.Modules;
                    foreach (ProcessModule module in myProcessModuleCollection){
                        string name = module.FileName;
                        if (name.Contains("Qt5Core.dll"))
                        {
                            _BaseAddress = Convert.ToUInt32(module.BaseAddress.ToInt32());
                            break;
                        }
                    }
                    _Handle = value.MainWindowHandle;
                }
            }
        }

        public static UInt32 Base
        {
            get { return (_Tibia == null)? 0 : _BaseAddress; }
            set { _BaseAddress = value; }
        }
        public static IntPtr Handle
        {
            get { return (_Tibia == null) ? IntPtr.Zero : _Handle; }
            set { _Handle = value; }
        }

        public static bool Update()
        {
            Process tmpProc = MemoryReader.getFirstClient();
            if (tmpProc == null)
            {
                return false;
            }
            if (tmpProc != Tibia)
            {
                Tibia = tmpProc;
            }
            return true;
        }

        // Get key codes ready for sending
        public static UInt32 getKeyCode(string Key)
        {
            Constants.KeyboardControls keynum;
            if (Enum.TryParse<Constants.KeyboardControls>(Key, true, out keynum))
            {
                return (UInt32)(keynum);
            }
            else
            {
                return new UInt32();
            }
        }

        public static void sendKeystroke(int priority, string Key, string CtrlShift = Constants.INVALID_KEY)
        {
            long time = DateTime.Now.Ticks;

            keyPresser.sendKeystroke(priority, Key, CtrlShift);

            time = DateTime.Now.Ticks - time;
            //Statistics.GetInstance().Keystroke = (time / TimeSpan.TicksPerMillisecond).ToString();
        }

        internal static void sendKeystrokeIntern(string Key, string CtrlShift = Constants.INVALID_KEY)
        {
            IntPtr wParam = new IntPtr(getKeyCode(Key));
            
            if (CtrlShift != Constants.INVALID_KEY)
            {
                SendMessage(Util.Handle, Constants.WM_SYSKEYDOWN, (IntPtr)getKeyCode(CtrlShift), new IntPtr(0));
            }
            SendMessage(Util.Handle, Constants.WM_SYSKEYDOWN, (IntPtr)getKeyCode(Key), new IntPtr(0));
            SendMessage(Util.Handle, Constants.WM_SYSKEYUP, (IntPtr)getKeyCode(Key), new IntPtr(0));
            if (CtrlShift != Constants.INVALID_KEY)
            {
                SendMessage(Util.Handle, Constants.WM_SYSKEYUP, (IntPtr)getKeyCode(CtrlShift), new IntPtr(0));
            }
        }

        private static bool isKeyPressed(string Key)
        {
            return ((GetKeyState((Int32)getKeyCode(Key)) & 0x8000) != 0);
        }

        public static void debugLog(string text, string filename = "botCrashReport.txt")
        {
            string filepath = System.IO.Path.GetDirectoryName(Application.ExecutablePath);
            string fullpath = filepath + @"\" + filename;
            fullpath = fullpath.Replace(@"\\", @"\");
            string fullMsg = "Log entry, time: " + System.DateTime.Now.ToString() + "\n" + text + "\n\n\n";
            System.IO.File.AppendAllText(fullpath, fullMsg);
        }

        public static string SaveFileDialog()
        {
            SaveFileDialog sfd = new SaveFileDialog();

            sfd.Filter = "Healbot files (*.hbf)|*.hbf|All files (*.*)|*.*";
            sfd.FilterIndex = 1;
            sfd.RestoreDirectory = true;

            if (sfd.ShowDialog() == DialogResult.OK)
            {
                return sfd.FileName;
            }

            return "";
        }

        public static string OpenFileDialog()
        {
            OpenFileDialog ofd = new OpenFileDialog();

            ofd.Filter = "Healbot files (*.hbf)|*.hbf|All Files (*.*)|*.*";
            ofd.FilterIndex = 1;
            ofd.Multiselect = false;

            if (ofd.ShowDialog() == DialogResult.OK)
            {
                return ofd.FileName;
            }

            return "";
        }
        
    }
}
