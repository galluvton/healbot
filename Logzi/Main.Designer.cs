﻿namespace Logzi
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_selectProcess = new System.Windows.Forms.Button();
            this.label_main = new System.Windows.Forms.Label();
            this.cb_startStop = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button_selectProcess
            // 
            this.button_selectProcess.Location = new System.Drawing.Point(12, 12);
            this.button_selectProcess.Name = "button_selectProcess";
            this.button_selectProcess.Size = new System.Drawing.Size(121, 33);
            this.button_selectProcess.TabIndex = 0;
            this.button_selectProcess.Text = "Select tibia process";
            this.button_selectProcess.UseVisualStyleBackColor = true;
            this.button_selectProcess.Click += new System.EventHandler(this.button_selectProcess_Click);
            // 
            // label_main
            // 
            this.label_main.AutoSize = true;
            this.label_main.Location = new System.Drawing.Point(199, 22);
            this.label_main.Name = "label_main";
            this.label_main.Size = new System.Drawing.Size(150, 13);
            this.label_main.TabIndex = 1;
            this.label_main.Text = "ServerLog backup: INACTIVE";
            // 
            // cb_startStop
            // 
            this.cb_startStop.Appearance = System.Windows.Forms.Appearance.Button;
            this.cb_startStop.AutoSize = true;
            this.cb_startStop.Location = new System.Drawing.Point(127, 82);
            this.cb_startStop.Name = "cb_startStop";
            this.cb_startStop.Size = new System.Drawing.Size(103, 23);
            this.cb_startStop.TabIndex = 2;
            this.cb_startStop.Text = "Start Log Reading";
            this.cb_startStop.UseVisualStyleBackColor = true;
            this.cb_startStop.CheckedChanged += new System.EventHandler(this.cb_startStop_CheckedChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(274, 82);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 3;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(361, 117);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.cb_startStop);
            this.Controls.Add(this.label_main);
            this.Controls.Add(this.button_selectProcess);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Logzi";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Main_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button_selectProcess;
        private System.Windows.Forms.Label label_main;
        private System.Windows.Forms.CheckBox cb_startStop;
        private System.Windows.Forms.Button button1;
    }
}