﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Timers;
using System.Diagnostics;

namespace Logzi
{
    public partial class Main : Form
    {
        private int filePostfix = 0;
        private int counter = 0;
        private System.Timers.Timer m_timer;
        private const int LIST_SIZE = 40;
        private Dictionary<string, SlidingMessageWindow> m_headMessages;
        private MessageWrapper m_lastMessage;
        private Dictionary<int, List<MessageWrapper>> m_tabMessages;

        public Main()
        {
            InitializeComponent();

            m_timer = new System.Timers.Timer(1 * 1000); // 1 second
            m_timer.Elapsed += new ElapsedEventHandler(ReadLog);

            lootLog("*******************Logzi opened at " + System.DateTime.Now.ToString() + "*******************", "messageDump" + filePostfix.ToString() + ".txt");

            m_headMessages = new Dictionary<string, SlidingMessageWindow>();
            m_lastMessage = MessageWrapper.EmptyMessage;
            m_tabMessages = new Dictionary<int, List<MessageWrapper>>();
        }

        public void ReadLog(object source, ElapsedEventArgs e)
        {
            
            IEnumerable<MemoryReader.ChatStruct> chatTabs = MemoryReader.GetChatTabs();
            foreach (MemoryReader.ChatStruct tab in chatTabs)
            {
                if (!m_headMessages.ContainsKey(tab.Name))
                {
                    m_headMessages[tab.Name] = new SlidingMessageWindow(LIST_SIZE);
                }
                ReadTabMessages(tab.Name, tab.DataPointer, m_tabMessages);
            }
        }

        private void ReadTabMessages(string logName, UInt32 logMessagesDataPointer, Dictionary<int, List<MessageWrapper>> tabMessages)
        {
            // read first message
            MessageWrapper msg = new MessageWrapper(MemoryReader.ReadUInt32(logMessagesDataPointer + MemoryReader.LogAddresses.LogContentMessagePointer));
            if (msg.IsEmpty()) // log is empty
            {
                return;
            }

            // look for the head log message in the sliding window
            int found = m_headMessages[logName].IndexOf(msg);

            if (found == -1) // log cleared
            {
                m_headMessages[logName].Clear();
                for (int i = 0; i < LIST_SIZE && !msg.IsEmpty(); i++)
                {
                    m_headMessages[logName].Add(msg);
                    if (!tabMessages.ContainsKey(msg.GetId()))
                    {
                        tabMessages[msg.GetId()] = new List<MessageWrapper>();
                    }
                    tabMessages[msg.GetId()].Add(msg);
                    counter++;
                    m_lastMessage = msg;
                    msg = msg.GetNextMessage();
                }
            }
            else if (found > 0) // window valid, but needs updating
            {
                MessageWrapper tmpMsg = m_headMessages[logName].Last().GetNextMessage();
                for (int i = 0; i < found && !tmpMsg.IsEmpty(); i++) // 'found' indicates how many messages we need to read
                {
                    m_headMessages[logName].Add(tmpMsg);
                    tmpMsg = tmpMsg.GetNextMessage();
                }
            }

            if (found >= 0) // sliding window is still valid
            {
                msg = m_lastMessage.GetNextMessage(); // keep reading from the last message
            } // else - msg already holds the last message needed (head)

            while (!msg.IsEmpty())
            {
                if (!tabMessages.ContainsKey(msg.GetId()))
                {
                    tabMessages[msg.GetId()] = new List<MessageWrapper>();
                }
                tabMessages[msg.GetId()].Add(msg);
                counter++;
                m_lastMessage = msg;
                msg = msg.GetNextMessage();
            }

            if (counter > 1000)
            {
                DumpMemory("messageDump" + filePostfix.ToString() + ".txt");
                filePostfix++;
                counter = 0;
            }
        }

        public static void lootLog(string text, string filename = "ServerLogBackup.txt")
        {
            string filepath = System.IO.Path.GetDirectoryName(Application.ExecutablePath);
            string fullpath = filepath + @"\" + filename;
            fullpath = fullpath.Replace(@"\\", @"\");
            string fullMsg = text + "\n";
            System.IO.File.AppendAllText(fullpath, fullMsg);
        }

        private void cb_startStop_CheckedChanged(object sender, EventArgs e)
        {
            if (MemoryReader.Tibia == null)
            {
                button_selectProcess_Click(null, null);
            }
            if (cb_startStop.Checked)
            {
                ReadLog(null, null);
                m_timer.Start();
                label_main.Text = "ServerLog backup: ACTIVE";
                label_main.ForeColor = Color.Red;
                cb_startStop.Text = "Stop Log Reading";
            }
            else
            {
                m_timer.Stop();
                label_main.Text = "ServerLog backup: INACTIVE";
                label_main.ForeColor = Color.Black;
                cb_startStop.Text = "Start Log Reading";
            }
        }

        private void button_selectProcess_Click(object sender, EventArgs e)
        {
            SelectProcessForm form = new SelectProcessForm();
            form.StartPosition = FormStartPosition.Manual;

            form.SetDesktopLocation(this.DesktopLocation.X + (this.Width - form.Width) / 2, this.DesktopLocation.Y + (this.Height - form.Height) / 2);
            form.ShowDialog();
        }

        private void Main_FormClosing(object sender, FormClosingEventArgs e)
        {
            string filename = "messageDump" + filePostfix.ToString() + ".txt";
            DumpMemory(filename);
            lootLog("*******************Logzi closed at " + System.DateTime.Now.ToString() + "*******************", filename);
        }

        private void DumpMemory(string filename)
        {
            List<int> list = m_tabMessages.Keys.ToList();
            list.Sort();
            foreach (int key in list)
            {
                lootLog("Type of: " + key.ToString(), filename);
                foreach (MessageWrapper msg in m_tabMessages[key])
                {
                    lootLog("   " + msg.Message, filename);
                }
            }
            m_tabMessages.Clear();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ReadLog(null, null);
        }

    }
}
