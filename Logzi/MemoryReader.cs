﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

namespace Logzi
{
    public static class MemoryReader
    {
        public static Process _Tibia = null;
        public static UInt32 _BaseAddress;
        public static IntPtr _Handle;

        [DllImport("kernel32.dll")]
        public static extern Int32 ReadProcessMemory(IntPtr hProcess, IntPtr lpBaseAddress,
            [In, Out] byte[] buffer, UInt32 size, out IntPtr lpNumberOfBytesRead);

        public static byte[] ReadBytes(IntPtr Handle, Int64 Address, uint BytesToRead)
        {
            IntPtr ptrBytesRead;
            byte[] buffer = new byte[BytesToRead];
            ReadProcessMemory(Handle, new IntPtr(Address), buffer, BytesToRead, out ptrBytesRead);
            return buffer;
        }

        public static Process Tibia
        {
            get
            {
                return _Tibia;
            }
            set
            {
                if (value != null)
                {
                    _Tibia = value;
                    _BaseAddress = Convert.ToUInt32(value.MainModule.BaseAddress.ToInt32());
                    _Handle = value.MainWindowHandle;
                }
            }
        }

        public static UInt32 Base
        {
            get { return (_Tibia == null) ? 0 : _BaseAddress; }
            set { _BaseAddress = value; }
        }
        public static IntPtr Handle
        {
            get { return (_Tibia == null) ? IntPtr.Zero : _Handle; }
            set { _Handle = value; }
        }

        public static IntPtr getIntPtr(IntPtr? Handle)
        {
            if (Handle == null)
            {
                return Tibia.Handle;
            }
            else
            {
                return (IntPtr)Handle;
            }
        }
        /*******************************************************************************************************/
        public static byte ReadByte(Int64 Address, IntPtr? Handle = null)
        {
            return ReadBytes(getIntPtr(Handle), Address, 1)[0];
        }
        public static bool ReadBool(Int64 Address, uint length, IntPtr? Handle = null)
        {
            return BitConverter.ToBoolean(ReadBytes(getIntPtr(Handle), Address, length), 0);
        }
        public static int ReadInt32(long Address, uint length = 4, IntPtr? Handle = null)
        {
            return BitConverter.ToInt32(ReadBytes(getIntPtr(Handle), Address, length), 0);
        }
        public static UInt32 ReadUInt32(long Address, uint length = 4, IntPtr? Handle = null)
        {
            return BitConverter.ToUInt32(ReadBytes(getIntPtr(Handle), Address, length), 0);
        }
        public static ushort ReadInt16(long Address, IntPtr? Handle = null)
        {
            return BitConverter.ToUInt16(ReadBytes(getIntPtr(Handle), Address, 2), 0);
        }
        public static double ReadDouble(long Address, IntPtr? Handle = null)
        {
            return BitConverter.ToDouble(ReadBytes(getIntPtr(Handle), Address, 8), 0);
        }
        public static string ReadString(long Address, uint length = 32, IntPtr? Handle = null)
        {
            string temp3 = ASCIIEncoding.Default.GetString(ReadBytes(getIntPtr(Handle), Address, length));
            string[] temp3str = temp3.Split('\0');
            return temp3str[0];
        }
        public static UInt32 ReadPtr(long Address, UInt32[] offsets, uint length = 32, IntPtr? Handle = null)
        {
            UInt32 Addr = (UInt32)ReadInt32(Address + Base);
            UInt32 NewAddr = Addr;
            int i = 0;
            foreach (UInt32 offset in offsets)
            {
                if (i != offsets.Length - 1)
                {
                    NewAddr = (UInt32)ReadInt32(NewAddr + offset);
                }
                else
                {
                    return NewAddr + offset;
                }
                i++;
            }
            return NewAddr;
        }
        public static string ReadStringPtr(long Address, UInt32[] offsets, uint length = 32, IntPtr? Handle = null)
        {
            UInt32 Addr = (UInt32)ReadInt32(Address + Base);
            UInt32 NewAddr = Addr;
            int i = 0;
            foreach (UInt32 offset in offsets)
            {
                if (i != offsets.Length)
                {
                    NewAddr = (UInt32)ReadInt32(NewAddr + offset);
                }
                i++;
            }
            return ReadString(NewAddr, 255);
        }

        public static IEnumerable<ChatStruct> GetChatTabs()
        {
            UInt32 tabNodeAddress = MemoryReader.ReadUInt32(Base + LogAddresses.ChatLogStart);

            foreach (UInt32 magicNum in LogAddresses.LogTabsMagics)
            {
                tabNodeAddress = MemoryReader.ReadUInt32(tabNodeAddress + magicNum);
            }

            while (tabNodeAddress != 0x0)
            {
                //use 0x30 for longer name (possibly upto 30 bytes)
                //0x2C will use '...' for names longer than 15 chars
                ChatStruct chat;
                UInt32 tabNamePointer = MemoryReader.ReadUInt32(tabNodeAddress + LogAddresses.LogTabsToLogNamePointer);
                chat.Name = MemoryReader.ReadString(tabNamePointer, LogAddresses.LogNameSize);
                chat.DataPointer = MemoryReader.ReadUInt32(tabNodeAddress + LogAddresses.LogTabsToLogContent);
                yield return chat;

                //next tab node pointer is current tab node address + 0x10
                tabNodeAddress = MemoryReader.ReadUInt32(tabNodeAddress + LogAddresses.LogTabNext);
            }
            yield break;
        }

        public static IEnumerable<string> ReadTabMessages(UInt32 logMessagesDataPointer)
        {
            UInt32 tabMessageNodeAddress = MemoryReader.ReadUInt32(logMessagesDataPointer + LogAddresses.LogContentMessagePointer);

            int messageCount = 0;
            while (tabMessageNodeAddress != 0x0)
            {
                messageCount++;
                UInt32 tabMessageAddress = MemoryReader.ReadUInt32(tabMessageNodeAddress + LogAddresses.LogContentMessageDoublePointer);
                //max message input is 255 characters, but the Advertising channel has 400+ character initial message
                string tabMessage = MemoryReader.ReadString(tabMessageAddress, LogAddresses.LogContentMessageSize);
                yield return tabMessage;

                //next tab messages node pointer is current tab messages node address + 0x5C
                tabMessageNodeAddress = MemoryReader.ReadUInt32(tabMessageNodeAddress + 0x5C);
            }
            yield break;
        }

        public struct ChatStruct // A struct for chat tabs
        {
            public UInt32 DataPointer;
            public string Name;
        }

        public struct LogAddresses
        {
            public const UInt32 ChatLogStart = 0x53596C;
            public static readonly UInt32[] LogTabsMagics = { 0x24, 0x10, 0x10, 0x30, 0x24 };
            public const UInt32 LogTabsToLogNamePointer = 0x2C;
            public const UInt32 LogNameSize = 16;
            public const UInt32 LogTabsToLogContent = 0x24;
            public const UInt32 LogTabNext = 0x10;
            public const UInt32 LogContentMessagePointer = 0x10;
            public const UInt32 LogContentMessageDoublePointer = 0x4C; // tibia's amazing *******int structures :)
            public const UInt32 LogContentMessageSize = 255;
            public const UInt32 LogContentNext = 0x5C;
        }

        public static bool Contains(this string s, string text, StringComparison stringComparison)
        {
            return s.IndexOf(text, stringComparison) > -1;
        }
    }
}
