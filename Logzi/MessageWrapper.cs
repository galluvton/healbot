﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logzi
{
    public class MessageWrapper
    {
        public static readonly MessageWrapper EmptyMessage = new MessageWrapper(0x0);
        private readonly int m_id;
        private readonly string m_message;
        private readonly UInt32 m_pMyAddress;

        public MessageWrapper(UInt32 myAddress)
        {
            if (myAddress == 0x0)
            {
                m_id = -1;
                m_message = string.Empty;
                m_pMyAddress = 0x0;
            }
            else
            {
                m_pMyAddress = myAddress;
                m_id = (int)MemoryReader.ReadUInt32(m_pMyAddress + 4);
                UInt32 tabMessageAddress = MemoryReader.ReadUInt32(myAddress + MemoryReader.LogAddresses.LogContentMessageDoublePointer);
                m_message = MemoryReader.ReadString(tabMessageAddress, MemoryReader.LogAddresses.LogContentMessageSize);
            }
        }

        public string Message
        {
            get { return m_message; }
        }

        public MessageWrapper GetNextMessage()
        {
            UInt32 nextMessage = MemoryReader.ReadUInt32(m_pMyAddress + MemoryReader.LogAddresses.LogContentNext);
            return new MessageWrapper(nextMessage);
        }

        public bool IsEmpty()
        {
            return (string.Empty.Equals(m_message));
        }

        public int GetId()
        {
            return m_id;
        }

        public override bool Equals(Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            MessageWrapper p = obj as MessageWrapper;
            if ((System.Object)p == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (Message == p.Message) && (m_pMyAddress == p.m_pMyAddress);
        }

        public static bool operator ==(MessageWrapper a, MessageWrapper b)
        {
            // If both are null, or both are same instance, return true.
            if (System.Object.ReferenceEquals(a, b))
            {
                return true;
            }

            // If one is null, but not both, return false.
            if (((object)a == null) || ((object)b == null))
            {
                return false;
            }

            // Return true if the fields match:
            return (a.Message == b.Message) && (a.m_pMyAddress == b.m_pMyAddress);
        }

        public override int GetHashCode()
        {
            int hash = 13;
            hash = (hash * 7) + m_message.GetHashCode();
            hash = (hash * 7) + m_pMyAddress.GetHashCode();
            return hash;
        }

        public static bool operator !=(MessageWrapper a, MessageWrapper b)
        {
            return !(a == b);
        }

        public override string ToString()
        {
            return Message + "(" + m_pMyAddress + ")";
        }

    }

}
