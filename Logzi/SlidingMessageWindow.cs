﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Logzi
{
    public class SlidingMessageWindow : List<MessageWrapper>
    {
        private int m_myMax;

        public SlidingMessageWindow(int size)
            : base(size + 3) // extra 3 just in case
        {
            m_myMax = size;
        }

        public new void Add(MessageWrapper message)
        {
            if (base.Count >= m_myMax)
            {
                base.RemoveAt(0);
            }
            base.Add(message);
        }

        public new MessageWrapper this[int index]
        {
            get
            {
                return base[index];
            }
            set { } // do not allow set-by-index
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder("SlidingGenericList with " + base.Count + " elements of:\n");
            foreach (MessageWrapper element in this)
            {
                sb.Append(element.ToString()).Append("\n");
            }
            return sb.ToString();
        }

    }
}
